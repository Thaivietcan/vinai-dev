<?php
get_header();
$_currentTax = get_queried_object();
$_taxonomy = $_currentTax->taxonomy;
$_slug = $_currentTax->slug;
$_id = $_currentTax->term_id;
$_name = $_currentTax->name;
$_thumbnail = get_field('careers_image_category', 'term_'.$_id) ? get_field('careers_image_category', 'term_'.$_id) : NOT_IMAGE; 
$_content = get_field('content_team', 'term_'.$_id) ? get_field('content_team', 'term_'.$_id) : '<p>'.__('Updating', 'corex').'...</p>';
$terms_teams = get_terms([
    'taxonomy'   => $_taxonomy,
    'hide_empty' => false,
    'orderby'       => 'id', 
    'order'         => 'ASC',
]);

$taxonomy_jobs = 'jobs-category';
$terms_jobs_category = get_terms([
    'taxonomy'   => $taxonomy_jobs,
    'hide_empty' => false,
    'orderby'       => 'id', 
    'order'         => 'DESC',
]);
?>
<div class="careers_detail news_detail teams_detail">
    <div class="tw-container">
        <div class="tw-content">
            <div class="_left">
                <div class="thumbnail">
                    <img src="<?php echo $_thumbnail; ?>" alt="">
                </div>
                <h1><?php echo $_name; ?></h1>
                <div class="the_content">
                    <?php echo $_content; ?>
                </div>
                <a href="<?php echo(get_bloginfo('url').'/'.__('careers', 'corex')); ?>" class="tw-button-common">
                    <span><?php _e('Explore More', 'corex') ?></span>
                    <img src="<?php echo THEME_ASSETS .'/images/common/right.png'; ?>" alt="">
                </a>
                <div class="_tabs" id="js_tabs_type_job">
                    <?php
                        if(!empty($terms_jobs_category)){
                            $i = 0;
                            foreach ($terms_jobs_category as $key => $job) {
                                $i++;
                                $_classActive = $i == 1 ? 'is--active' : null;
                                echo '<a href="'.$job->slug.'" class="'.$_classActive.'" data-job="type-'.$i.'">'.$job->name.'</a>';
                            };
                        }
                    ?>
                </div>
                <div class="_jobs" id="position_hiring">
                    <?php
                        if(!empty($terms_jobs_category)){
                            $i = 0;
                            foreach ($terms_jobs_category as $key => $job) {
                                $i++;
                                $_classActive = $i == 1 ? 'is--active' : null;
                                $_careers = new WP_Query([
                                    'post_type'      => 'careers',
                                    'posts_per_page' => -1,
                                    'teams-category'  => $_slug,
                                    'jobs-category'  => $job->slug,
                                    'post_status'    => 'publish',
                                ]);
                                ?>
                                    <ul class="<?php echo $_classActive; ?>" id="type-<?php echo $i; ?>">
                                        <?php
                                            if($_careers->have_posts(  )){
                                                while ($_careers->have_posts(  )) {
                                                    $_careers->the_post(  );
                                                    $_careerID = get_the_ID(  );
                                                    $_title = get_the_title($_careerID);
                                                    $_link = get_the_permalink($_careerID);
                                                    $_locations  = get_the_terms( $_careerID, "location-category" );
                                                ?>
                                                    <li>
                                                        <a href="<?php echo $_link; ?>" class="--name"><?php echo $_title; ?></a>
                                                        <p class="--location">
                                                            <?php
                                                                if($_locations){
                                                                    $j = 0;
                                                                    foreach ($_locations as $location) {
                                                                        $j++;
                                                                        $termID = $location->term_id;
                                                                        $shortened_name = get_field('location_shortened_name', 'term_'.$termID);
                                                                        if( $j == count($_locations)){
                                                                            echo $shortened_name;
                                                                        }else{
                                                                            echo $shortened_name . ', ';
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                        </p>
                                                    </li>
                                                <?php
                                                }
                                            }else{
                                                echo '<p>'.__('We have not opened new position yet, please check back later!', 'corex').'</p>';
                                            }
                                        ?>
                                    </ul>
                                <?php
                            }
                        }
                    ?>
                </div>
            </div>

            <div class="_right">
                <h3><?php _e('related posts', 'corex'); ?></h3>

                <div class="_related_list">
                    <?php
                        // var_dump($terms_teams);
                        if($terms_teams){
                            foreach ($terms_teams as $team) {
                                $_termId = $team->term_id;
                                $_link_term = get_term_link($_termId);
                                $_name_term = $team->name;
                                $_description_term = $team->description;
                                $_thumbnail_term = get_field('careers_image_category', 'term_'.$_termId) ? get_field('careers_image_category', 'term_'.$_termId) : NOT_IMAGE;
                                if($_termId !== $_id){
                                    echo '<div class="__item">
                                        <a href="'.$_link_term.'" class="--img" style="background-image: url('.$_thumbnail_term.')">
                                            <h3>'.$_name_term.'</h3>
                                        </a>
                                        <div class="--txt">
                                            <p>'.$_description_term.'</p>
                                            <a href="'.$_link_term.'" class="tw-button-common">
                                                <span>'.__('Explore More', 'corex').'</span>
                                                <img src="'. THEME_ASSETS . '/images/common/right_a.png'.'" alt="">
                                            </a>
                                        </div>
                                    </div>';
                                }
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('[data-job]').on('click', function(e){
        e.preventDefault();
        const typeValue = $(this).attr('data-job');
        const listJobEl = $('#'+typeValue+'');
        $(this).siblings('a').removeClass('is--active');
        $(this).addClass('is--active');
        listJobEl.siblings('ul').removeClass('is--active');
        listJobEl.addClass('is--active');
    });
});
</script>
<?php get_footer();