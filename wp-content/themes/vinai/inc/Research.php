<?php

    // load navigation blog
    add_action('wp_ajax_ajax_navigation_load_blogs', 'ajax_navigation_load_blogs_func');
    add_action('wp_ajax_nopriv_ajax_navigation_load_blogs', 'ajax_navigation_load_blogs_func');
    function ajax_navigation_load_blogs_func(){
        // if(!wp_verify_nonce( $_POST['nonce'], "ajax_loading_blogs")){
        //     wp_send_json_error('None?');
        // };
        $paged = isset($_POST['paged']) ? intval($_POST['paged']) : '';
        if($paged <= 0 || !$paged || !is_numeric($paged)) wp_send_json_error('Paged?');
        $slug = isset($_POST['slug']) ? $_POST['slug'] : '';
        if(!$slug) wp_send_json_error('Slug?');

        $searchtxt = isset($_POST['searchtxt']) ? $_POST['searchtxt'] : '';
        $researchArea = isset($_POST['researchArea']) ? $_POST['researchArea'] : null;
        $year = isset($_POST['year']) ? intval($_POST['year']) : null;

        if($searchtxt != ''){
            $blogs = new WP_Query([
                'post_type'      => 'post',
                'category_name'  => $slug,
                'research-area'  => $researchArea,
                'posts_per_page' => 12,
                'paged'          => $paged,
                'post_status'    => 'publish',
                'date_query' => [
                    [
                        'year' => $year,
                    ],
                    'compare' =>  '=',
                ],
                's' => $searchtxt,
                'sentence' => true,
            ]);
        }else{
            $blogs = new WP_Query([
                'post_type'      => 'post',
                'category_name'  => $slug,
                'research-area'  => $researchArea,
                'posts_per_page' => 12,
                'paged'          => $paged,
                'post_status'    => 'publish',
                'date_query' => [
                    [
                        'year' => $year,
                    ],
                    'compare' =>  '=',
                ],
            ]);
        }
        ob_start();
        ?>
        <div class="_list_news d-flex list-publications-global d-flex flex-wrap">
        <?php
        if($blogs->have_posts(  )){ 
            while ($blogs->have_posts(  )){ 
                $blogs->the_post(  ); 
                $blogID = get_the_ID(  );
                $_thumbnail = get_the_post_thumbnail_url($blogID) ? get_the_post_thumbnail_url($blogID) : NOT_IMAGE;
                $_title = get_the_title($blogID);
				$_theExcerpt = get_the_excerpt($blogID);
				$_category = get_the_terms( $blogID, 'research-area' );
                $_time = get_the_time( 'd/m/Y', $blogID );
                $_permalink = get_the_permalink($blogID);
                ?>
					<div class="__item">
						<a href="<?php echo $_permalink ?>" class="thumbnail">
							<div class="--bg"
								style="background-image: url(<?php echo $_thumbnail ?>)">
							</div>
						</a>
						<div class="text">
							<span class="sub-heading">
								<?php 
									foreach ( $_category as $cat){
									   echo $cat->name;
									}
								?>
							</span>
							<a href="<?php echo $_permalink ?>" class="--title">
								<h4><?php echo $_title; ?></h4>
							</a>
							<p>
								<?php 
									if (strlen($_theExcerpt) > 100) {
										echo substr($_theExcerpt, 0, 100) . '(...)';
									} else {
										echo $_theExcerpt;
									}
								?>
							</p>
							
							<div class="--meta d-flex align-items-center justify-content-between">
								<a href="<?php echo $_permalink ?>" class="btn btn-view-detail btn-view-detail-home">
									View details
									<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt="">
								</a>
								<p class="--time"><?php echo $_time; ?></p>
							</div>
						</div>
					</div>
				<?php
            } wp_reset_postdata(  ); 
        }
        ?>
        </div>
        <div class="pagination-common js_panigator">
            <?php wp_navigation_paged_ajax($blogs, $paged); ?>
        </div>
        <?php
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }



    //ajax_search_load_blogs
    add_action('wp_ajax_ajax_search_load_blogs', 'ajax_search_load_blogs_func');
    add_action('wp_ajax_nopriv_ajax_search_load_blogs', 'ajax_search_load_blogs_func');
    function ajax_search_load_blogs_func(){
        // if(!isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], "ajax_loading_blogs")){
        //     wp_send_json_error('None?');
        // };
        $slug = isset($_POST['slug']) ? $_POST['slug'] : '';
        if(!$slug) wp_send_json_error('Slug?');
        $searchtxt = isset($_POST['searchtxt']) ? $_POST['searchtxt'] : '';
        $researchArea = isset($_POST['researchArea']) ? $_POST['researchArea'] : null;
        $year = isset($_POST['year']) ? intval($_POST['year']) : null;
        // $slug = $researchArea ? $researchArea : $slug;
        if($searchtxt != ''){
            $blogs = new WP_Query([
                'post_type'      => 'post',
                'category_name'  => $slug,
                'research-area'  => $researchArea,
                'posts_per_page' => 12,
                'post_status'    => 'publish',
                'date_query' => [
                    [
                        'year' => $year,
                    ],
                    'compare' =>  '=',
                ],
                's' => $searchtxt,
                // 'sentence' => true,
            ]);
        }else{
            $blogs = new WP_Query([
                'post_type'      => 'post',
                'category_name'  => $slug,
                'research-area'  => $researchArea,
                'posts_per_page' => 12,
                'post_status'    => 'publish',
                'date_query' => [
                    [
                        'year' => $year,
                    ],
                    'compare' =>  '=',
                ],
            ]);
        }
        ob_start();
       
        if($blogs->have_posts(  )){
            echo '<div class="_list_news list-publications-global d-flex flex-wrap">';
            while ($blogs->have_posts(  )) {
                $blogs->the_post(  ); 
                $blogID = get_the_ID(  );
                $_thumbnail = get_the_post_thumbnail_url($blogID) ? get_the_post_thumbnail_url($blogID) : NOT_IMAGE;
                $_title = get_the_title($blogID);
				$_theExcerpt = get_the_excerpt($blogID);
				$_category = get_the_terms( $blogID, 'research-area' );
                $_time = get_the_time( 'd/m/Y', $blogID );
                $_permalink = get_the_permalink($blogID);
                ?>
					<div class="__item">
						<a href="<?php echo $_permalink ?>" class="thumbnail">
							<div class="--bg"
								style="background-image: url(<?php echo $_thumbnail ?>)">
							</div>
						</a>
						<div class="text">
							<span class="sub-heading">
								<?php 
									foreach ( $_category as $cat){
									   echo $cat->name;
									}
								?>
							</span>
							<a href="<?php echo $_permalink ?>" class="--title">
								<h4><?php echo $_title; ?></h4>
							</a>
							<p>
								<?php 
									if (strlen($_theExcerpt) > 100) {
										echo substr($_theExcerpt, 0, 100) . '(...)';
									} else {
										echo $_theExcerpt;
									}
								?>
							</p>
							
							<div class="--meta d-flex align-items-center justify-content-between">
								<a href="<?php echo $_permalink ?>" class="btn btn-view-detail btn-view-detail-home">
									View details
									<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt="">
								</a>
								<p class="--time"><?php echo $_time; ?></p>
							</div>
						</div>
					</div>
				<?php
            } wp_reset_postdata(  );
            echo '</div>';
        }else{
            wp_send_json_error('<p>'.__('There are no relevant search results', 'corex').'</p>');
        }
        ?>
        
        <div class="pagination-common js_panigator">
            <?php wp_navigation_paged_ajax($blogs); ?>
        </div>
        <?php
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }


    //ajax_search_load_publications
    add_action('wp_ajax_ajax_search_load_publications', 'ajax_search_load_publications_func');
    add_action('wp_ajax_nopriv_ajax_search_load_publications', 'ajax_search_load_publications_func');
    function ajax_search_load_publications_func(){
        // if(!isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], "ajax_loading_publication")){
        //     wp_send_json_error('None?');
        // };
        $searchtxt = isset($_POST['searchtxt']) ? $_POST['searchtxt'] : '';
        $researchArea = isset($_POST['researchArea']) ? $_POST['researchArea'] : null;
        $year = isset($_POST['year']) ? intval($_POST['year']) : null;
        if($searchtxt != ''){

            $publications_1 = new WP_Query([
                'post_type'      => 'publications',
                'research-area'  => $researchArea,
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'date_query' => [
                    [
                        'year' => $year,
                    ],
                    'compare' =>  '=',
                ],
                'meta_query'      => array(
                    'relation' => 'OR',
                    array(
                        'key'     => 'authors',
                        'value'   => $searchtxt,
                        'compare' => 'LIKE'
                    ),
                    array(
                        'key'     => 'conference_name',
                        'value'   => $searchtxt,
                        'compare' => 'LIKE'
                    )
                ),
            ]);

            $publications_2 = new WP_Query([
                'post_type'      => 'publications',
                'research-area'  => $researchArea,
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'date_query' => [
                    [
                        'year' => $year,
                    ],
                    'compare' =>  '=',
                ],
                's' => $searchtxt,
                'sentence' => true, 
            ]);

            $publications = new WP_Query();
            $publications->posts = array_unique( array_merge( $publications_1->posts, $publications_2->posts ), SORT_REGULAR );
            $publications->post_count = count( $publications->posts );
        }else{
            $publications = new WP_Query([
                'post_type'      => 'publications',
                'research-area'  => $researchArea,
                'posts_per_page' => 18,
                'post_status'    => 'publish',
                'date_query' => [
                    [
                        'year' => $year,
                    ],
                    'compare' =>  '=',
                ],
            ]);
        }
        ob_start();
    
        if($publications->have_posts(  )){
            echo '<div class="_list_news list-publications-global d-flex flex-wrap">';
            while ($publications->have_posts(  )) {
                $publications->the_post(  );
                $post_id = get_the_ID();
                $title = get_the_title($post_id);
				$category = get_the_terms( $post_id, 'research-area' );
                $author = get_field('authors', $post_id);
                $conference_name = get_field('conference_name', $post_id);
                $pdf = get_field('pdf', $post_id);
                $permalink = get_the_permalink($post_id);
                $is_hover_download = $pdf ? 'class="is_hover"' : null;
                ?>
					<div class="__item">
						<div class="--description">
							<div class="--title_doc">
								<span class="sub-heading">
									<?php 
										foreach ( $category as $cat){
										   echo $cat->name;
										}
									?>
								</span>
								<a href="<?php echo $permalink; ?>">
									<h4>
										<?php echo $title; ?>
									</h4>
								</a>
							</div>
							<div class="--member">
								<img src="<?php echo THEME_ASSETS . '/images/user-icon.svg' ?>" />
								<?php 
									if ($author) {
										echo $author;
									}
								?>
								<span style="color: #000">(<?php echo $conference_name; ?>)</span>
							</div>
						</div>
						<div class="--control d-flex justify-content-between align-items-center">
							<a href="<?php echo $permalink; ?>" class="btn btn-view-detail d-flex align-items-center">
								<span><?php _e('View detail', 'corex') ?></span>
								<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>">
							</a>
							<a class="--control_download" href="<?php echo $pdf; ?>" <?php echo $is_hover_download; ?> target="_blank"><i class="fas fa-arrow-down"></i></a>
						</div>
					</div>
				<?php
            } wp_reset_postdata(  );
            echo '</div>';
        }else{
            wp_send_json_error('<p>'.__('There are no relevant search results', 'corex').'</p>');
        }
        ?>
        
        <div class="pagination-common js_panigator">
            <?php wp_navigation_paged_ajax($publications); ?>
        </div>
        <?php
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }

    // load navigation pulication
    add_action('wp_ajax_ajax_navigation_load_publications', 'ajax_navigation_load_publications_func');
    add_action('wp_ajax_nopriv_ajax_navigation_load_publications', 'ajax_navigation_load_publications_func');
    function ajax_navigation_load_publications_func(){
        // if(!wp_verify_nonce( $_POST['nonce'], "ajax_loading_publication")){
        //     wp_send_json_error('None?');
        // };
        $paged = isset($_POST['paged']) ? intval($_POST['paged']) : '';
        if($paged <= 0 || !$paged || !is_numeric($paged)) wp_send_json_error('Paged?');
        $searchtxt = isset($_POST['searchtxt']) ? $_POST['searchtxt'] : '';
        $researchArea = isset($_POST['researchArea']) ? $_POST['researchArea'] : null;
        $year = isset($_POST['year']) ? intval($_POST['year']) : null;

        if($searchtxt != ''){
            $publications = new WP_Query([
                'post_type'      => 'publications',
                'research-area'  => $researchArea,
                'posts_per_page' => 18,
                'paged'          => $paged,
                'post_status'    => 'publish',
                'date_query' => [
                    [
                        'year' => $year,
                    ],
                    'compare' =>  '=',
                ],
                's' => $searchtxt,
            ]);
        }else{
            $publications = new WP_Query([
                'post_type'      => 'publications',
                'research-area'  => $researchArea,
                'posts_per_page' => 18,
                'paged'          => $paged,
                'post_status'    => 'publish',
                'date_query' => [
                    [
                        'year' => $year,
                    ],
                    'compare' =>  '=',
                ],
            ]);
        }
        ob_start();
        ?>
        <div class="_list_news list-publications-global d-flex flex-wrap">
        <?php
        if($publications->have_posts(  )){ 
            while ($publications->have_posts(  )){ 
                $publications->the_post(  );
                $post_id = get_the_ID();
                $title = get_the_title($post_id);
				$category = get_the_terms( $post_id, 'research-area' );
                $author = get_field('authors', $post_id);
                $conference_name = get_field('conference_name', $post_id);
                $pdf = get_field('pdf', $post_id);
                $permalink = get_the_permalink($post_id);
                $is_hover_download = $pdf ? 'class="is_hover"' : null;
                ?>
					<div class="__item">
						<div class="--description">
							<div class="--title_doc">
								<span class="sub-heading">
									<?php 
										foreach ( $category as $cat){
										   echo $cat->name;
										}
									?>
								</span>
								<a href="<?php echo $permalink; ?>">
									<h4>
										<?php echo $title; ?>
									</h4>
								</a>
							</div>
							<div class="--member">
								<img src="<?php echo THEME_ASSETS . '/images/user-icon.svg' ?>" />
								<?php 
									if ($author) {
										echo $author;
									}
								?>
								<span style="color: #000">(<?php echo $conference_name; ?>)</span>
							</div>
						</div>
						<div class="--control d-flex justify-content-between align-items-center">
							<a href="<?php echo $permalink; ?>" class="btn btn-view-detail d-flex align-items-center">
								<span><?php _e('View detail', 'corex') ?></span>
								<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>">
							</a>
							<a class="--control_download" href="<?php echo $pdf; ?>" <?php echo $is_hover_download; ?> target="_blank"><i class="fas fa-arrow-down"></i></a>
						</div>
					</div>
				<?php
            } wp_reset_postdata(  ); 
        }
        ?>
        </div>
        <div class="pagination-common js_panigator">
            <?php wp_navigation_paged_ajax($publications, $paged); ?>
        </div>
        <?php
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }
	
	//ajax_search_load_seminar
    add_action('wp_ajax_ajax_search_load_seminar', 'ajax_search_load_seminar_func');
    add_action('wp_ajax_nopriv_ajax_search_load_seminar', 'ajax_search_load_seminar_func');
    function ajax_search_load_seminar_func(){
        // if(!isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], "ajax_loading_seminar")){
        //     wp_send_json_error('None?');
        // };
		$paged = isset($_POST['paged']) ? intval($_POST['paged']) : 1;
        $researchSeminar = isset($_POST['researchSeminar']) ? $_POST['researchSeminar'] : null;
		$upcoming_speaker_id = isset($_POST['upcoming_speaker_id']) ? $_POST['upcoming_speaker_id'] : [];
		$seminar = new WP_Query([
			'post_type'      => 'seminar',
			'seminar-series'  => $researchSeminar,
			'posts_per_page' => 8,
			'paged'          => $paged,
			'post_status'    => 'publish',
		]);
        ob_start();
    
        if($seminar->have_posts(  )){
            echo '<div class="grid-speakers">';
            while ($seminar->have_posts(  )) {
                $seminar->the_post(  );
                $post_id = get_the_ID();
                $post_avatar = get_field('avatar', $post_id) ?: NOT_AVATAR;
				$post_name = get_field('name_speakers', $post_id);
				$post_work_place = get_field('work_place', $post_id);
				$post_title = get_the_title($post_id);
				$dateObject = new DateTime(get_field('time_takes_place', $post_id));
				$timeStamp = $dateObject->getTimestamp();
				$post_timing = get_field('time_takes_place', $post_id) ? date('l, M d Y - h:i a', $timeStamp).' (GMT + 7)' : null;
				$post_permalink = get_the_permalink($post_id);
                ?>
					<div class="speaker-item">
							<div class="avatar">
								<a href="<?php echo $post_permalink; ?>"><img src="<?php echo $post_avatar; ?>" alt="<?php echo $post_title; ?>"></a>
							</div>
							<div class="info">
								<a href="<?php echo $post_permalink; ?>"><h3 class="name"><?php echo $post_name; ?></h3></a>
								<p class="work_place">
									<?php 
										if (strlen($post_work_place) > 50) {
											echo substr($post_work_place, 0, 50) . '...';
										} else {
											echo $post_work_place;
										} 
									?>
								</p>
								<p class="title-post"><i class="fas fa-microphone"></i>
									<?php 
										if (strlen($post_title) > 90) {
											echo substr($post_title, 0, 90) . '...';
										} else {
											echo $post_title;
										} 
									?>
								</p>
								<p class="time_takes_place"><i class="fas fa-clock"></i> <?php echo $post_timing; ?></p>
							</div>
						</div>
				<?php
            } wp_reset_postdata(  );
            echo '</div>';
        }else{
            wp_send_json_error('<p>'.__('There are no relevant search results', 'corex').'</p>');
        }
        ?>
        
        <div class="pagination-common js_panigator text-align-center">
            <?php wp_navigation_paged_ajax($seminar); ?>
        </div>
        <?php
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }


    // load navigation seminar
    add_action('wp_ajax_ajax_navigation_load_seminar', 'ajax_navigation_load_seminar_func');
    add_action('wp_ajax_nopriv_ajax_navigation_load_seminar', 'ajax_navigation_load_seminar_func');
    function ajax_navigation_load_seminar_func(){
        // if(!wp_verify_nonce( $_POST['nonce'], "ajax_loading_seminar")){
        //     wp_send_json_error('None?');
        // };
        $paged = isset($_POST['paged']) ? intval($_POST['paged']) : '';
        if($paged <= 0 || !$paged || !is_numeric($paged)) wp_send_json_error('Paged?');
        $researchSeminar = isset($_POST['researchSeminar']) ? $_POST['researchSeminar'] : null;

        $seminar = new WP_Query([
			'post_type'      => 'seminar',
			'seminar-series'  => $researchSeminar,
			'posts_per_page' => 8,
			'paged'          => $paged,
			'post_status'    => 'publish',
		]);
        ob_start();
        ?>
        <div class="grid-speakers">
        <?php
        if($seminar->have_posts(  )){ 
            while ($seminar->have_posts(  )){ 
                $seminar->the_post(  );
                $post_id = get_the_ID();
                $post_avatar = get_field('avatar', $post_id) ?: NOT_AVATAR;
				$post_name = get_field('name_speakers', $post_id);
				$post_work_place = get_field('work_place', $post_id);
				$post_title = get_the_title($post_id);
				$dateObject = new DateTime(get_field('time_takes_place', $post_id));
				$timeStamp = $dateObject->getTimestamp();
				$post_timing = get_field('time_takes_place', $post_id) ? date('l, M d Y - h:i a', $timeStamp).' (GMT + 7)' : null;
				$post_permalink = get_the_permalink($post_id);
                ?>
					<div class="speaker-item">
						<div class="avatar">
							<a href="<?php echo $post_permalink; ?>"><img src="<?php echo $post_avatar; ?>" alt="<?php echo $post_title; ?>"></a>
						</div>
						<div class="info">
							<a href="<?php echo $post_permalink; ?>"><h3 class="name"><?php echo $post_name; ?></h3></a>
							<p class="work_place">
								<?php 
									if (strlen($post_work_place) > 50) {
										echo substr($post_work_place, 0, 50) . '...';
									} else {
										echo $post_work_place;
									} 
								?>
							</p>
							<p class="title-post"><i class="fas fa-microphone"></i>
								<?php 
									if (strlen($post_title) > 90) {
										echo substr($post_title, 0, 90) . '...';
									} else {
										echo $post_title;
									} 
								?>
							</p>
							<p class="time_takes_place"><i class="fas fa-clock"></i> <?php echo $post_timing; ?></p>
						</div>
					</div>
				<?php
            } wp_reset_postdata(  ); 
        }
        ?>
        </div>
        <div class="pagination-common js_panigator text-align-center">
            <?php wp_navigation_paged_ajax($seminar, $paged); ?>
        </div>
        <?php
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }

    //load navigation pubs ------------------------------------------- option page
    add_action('wp_ajax_ajax_load_pubs', 'ajax_load_pubs_func');
    add_action('wp_ajax_nopriv_ajax_load_pubs', 'ajax_load_pubs_func');
    function ajax_load_pubs_func(){
        // if(!wp_verify_nonce( $_POST['nonce'], "ajax_loading_publication")){
        //     wp_send_json_error('None?');
        // };
        $current = isset($_POST['current']) ? intval($_POST['current']) : '';
        if( $current <= 0 || !$current || !is_numeric($current) ) wp_send_json_error('Current?');
        $paged = isset($_POST['paged']) ? intval($_POST['paged']) : '';
        if($paged <= 0 || !$paged || !is_numeric($paged)) wp_send_json_error('Paged?');
        $publications = get_field('publications', 'option');
        $searchtxt = isset($_POST['searchtxt']) ? $_POST['searchtxt'] : null;
        $researchArea = isset($_POST['researchArea']) ? intval($_POST['researchArea']) : 0;
        $years = isset($_POST['years']) ? intval($_POST['years']) : 0;
        if($publications){
            if(!$searchtxt && $researchArea == 0 && $years == 0){
                $total = count($publications);
                $pageNumber = ceil($total/$current);
                $limit = $current * $paged;
                $start = $limit - $current;
                ob_start();
                renderPubsItem($publications, $start, $limit, $pageNumber, $paged);
            }else{
                ob_start();
                $resultPubs = array();
                foreach ($publications as $publication) {
                    $_index = stripos($publication['title'],$searchtxt);
                    $_researchArea = intval($publication['research_area']);
                    $_years = intval($publication['publication_date']);
    
                    if($researchArea == 0){
                        if($years == 0){
                            if(!$searchtxt){
                                array_push($resultPubs, $publication);
                            }else{
                                if(is_numeric($_index)){
                                    array_push($resultPubs, $publication);
                                }
                            }
                        }else{
                            if($years == $_years){
                                if(!$searchtxt){
                                    array_push($resultPubs, $publication);
                                }else{
                                    if(is_numeric($_index)){
                                        array_push($resultPubs, $publication);
                                    }
                                }
                            }
                        }
                    }else{
                        if($researchArea == $_researchArea){
                            if($years == 0){
                                if(!$searchtxt){
                                    array_push($resultPubs, $publication);
                                }else{
                                    if(is_numeric($_index)){
                                        array_push($resultPubs, $publication);
                                    }
                                }
                            }else{
                                if($years == $_years){
                                    if(!$searchtxt){
                                        array_push($resultPubs, $publication);
                                    }else{
                                        if(is_numeric($_index)){
                                            array_push($resultPubs, $publication);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $total = count($resultPubs);
                $pageNumber = ceil($total/$current);
                $limit = $current * $paged;
                $start = $limit - $current;
                renderPubsItem($resultPubs, $start, $limit, $pageNumber, $paged);
            }
        }else{
            wp_send_json_error('Publications?');
        }
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }

    function renderPubsItem($arrayPubs, $start, $limit, $pageNumber, $paged){
        echo '<div class="_list_news ">';
        $i = 0;
        foreach ($arrayPubs as $value) {
            $i++;
            if($i > $start){
                $year_conference = $value['year_conference'] ? '('.$value['year_conference'].')' : null;
                $addfile = $value['document'];
                $hyperlink = $value['hyperlink'];
                $document = $addfile ? $addfile : $hyperlink;
                echo '<div class="__item">
                        <div class="--description">
                            <div class="--title_doc">
                                <p>'.$value['title'].'</p>
                                <img src="'. THEME_ASSETS . '/images/common/doc.png'.'" alt="">
                            </div>
                            <div class="--member">
                                '.$value['author'].' <span>'.$year_conference.'</span>
                            </div>
                        </div>
                        <div class="--control">
                            <a href="'.$document.'" download><img src="'. THEME_ASSETS . '/images/common/download.png'.'" alt=""><span>'.__('Download', 'corex').'</span></a>
                            <a href="'.$value['copy_bibtex'].'" target="_blank" copy-bibtex="'.$value['copy_bibtex'].'" class="copy-bibtex"><img src="'. THEME_ASSETS . '/images/common/copy.png'.'" alt=""><span>'.__('Copy Bibtex', 'corex').'</span></a>
                        </div>
                    </div>';
            }
            if($i == $limit){
                break;
            }
        }
        echo '</div>';
        ?>
        <div class="pagination-common js_panigator">
            <div class="paginate_links">
                <?php
                    if($pageNumber > 1){
                        if($paged !== 1){
                            echo '<a class="next page-numbers" href="'.get_bloginfo('url').'/'.__('value').'/page/'. ($paged - 1) .'/"><</a>';
                        }
                        for ($i = 1; $i <= $pageNumber; $i++) {
                            if($i == $paged){
                                echo '<span class="page-numbers current">'.$i.'</span>';
                            }else{
                                echo '<a class="page-numbers" href="'.get_bloginfo('url').'/'.__('publication').'/page/'.$i.'/">'.$i.'</a>';
                            }
                        }
                        if($paged !== intval($pageNumber)){
                            echo '<a class="next page-numbers" href="'.get_bloginfo('url').'/'.__('publication').'/page/'. ($paged + 1) .'/">></a>';
                        }
                    }
                ?>
            </div>
        </div>
        <?php
    }


    // Search publication
    add_action('wp_ajax_ajax_search_pubs', 'ajax_search_pubs_func');
    add_action('wp_ajax_nopriv_ajax_search_pubs', 'ajax_search_pubs_func');
    function ajax_search_pubs_func(){
        // if(!wp_verify_nonce( $_POST['nonce'], "ajax_loading_publication")){
        //     wp_send_json_error('None?');
        // };
        $current = isset($_POST['current']) ? intval($_POST['current']) : null;
        if( $current <= 0 || !$current || !is_numeric($current) ) wp_send_json_error('Current?');
        $publications = get_field('publications', 'option');
        $searchtxt = isset($_POST['searchtxt']) ? $_POST['searchtxt'] : null;
        $researchArea = isset($_POST['researchArea']) ? intval($_POST['researchArea']) : 0;
        $years = isset($_POST['years']) ? intval($_POST['years']) : 0;
        $resultPubs = array();
        if($publications){
            foreach ($publications as $publication) {
                $_index = stripos($publication['title'],$searchtxt);
                $_researchArea = intval($publication['research_area']);
                $_years = intval($publication['publication_date']);

                if($researchArea == 0){
                    if($years == 0){
                        if(!$searchtxt){
                            array_push($resultPubs, $publication);
                        }else{
                            if(is_numeric($_index)){
                                array_push($resultPubs, $publication);
                            }
                        }
                    }else{
                        if($years == $_years){
                            if(!$searchtxt){
                                array_push($resultPubs, $publication);
                            }else{
                                if(is_numeric($_index)){
                                    array_push($resultPubs, $publication);
                                }
                            }
                        }
                    }
                }else{
                    if($researchArea == $_researchArea){
                        if($years == 0){
                            if(!$searchtxt){
                                array_push($resultPubs, $publication);
                            }else{
                                if(is_numeric($_index)){
                                    array_push($resultPubs, $publication);
                                }
                            }
                        }else{
                            if($years == $_years){
                                if(!$searchtxt){
                                    array_push($resultPubs, $publication);
                                }else{
                                    if(is_numeric($_index)){
                                        array_push($resultPubs, $publication);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }else{
            wp_send_json_error('Publications?');
        }
        ob_start();
        if($resultPubs){
            $total = count($resultPubs);
            $pageNumber = ceil($total/$current);
            $i = 0;
            echo '<div class="_list_news">';
            foreach ($resultPubs as $Pubs) {
                $i++;
                $year_conference = $Pubs['year_conference'] ? '('.$Pubs['year_conference'].')' : null;
                $addfile = $Pubs['document'];
                $hyperlink = $Pubs['hyperlink'];
                $document = $addfile ? $addfile : $hyperlink;
                echo '<div class="__item">
                        <div class="--description">
                            <div class="--title_doc">
                                <p>'.$Pubs['title'].'</p>
                                <img src="'. THEME_ASSETS . '/images/common/doc.png'.'" alt="">
                            </div>
                            <div class="--member">
                                '.$Pubs['author'].' <span>'.$year_conference.'</span>
                            </div>
                        </div>
                        <div class="--control">
                            <a href="'.$document .'" download><img src="'. THEME_ASSETS . '/images/common/download.png'.'" alt=""><span>'.__('Download', 'corex').'</span></a>
                            <a href="'.$Pubs['copy_bibtex'].'" target="_blank" copy-bibtex="'.$Pubs['copy_bibtex'].'" class="copy-bibtex"><img src="'. THEME_ASSETS . '/images/common/copy.png'.'" alt=""><span>'.__('Copy Bibtex', 'corex').'</span></a>
                        </div>
                    </div>';
                if($i == $current){
                break;
                }
            }
            echo '</div>';
        }else{
            wp_send_json_error('<p>'.__('There are no relevant search results', 'corex').'</p>');
        }
        ?>
        <div class="pagination-common js_panigator">
            <div class="paginate_links">
                <?php
                    if($pageNumber > 1){
                        for ($i = 1; $i <= $pageNumber; $i++) { 
                            if($i === 1){
                                echo '<span class="page-numbers current">'.$i.'</span>';
                            }else{
                                echo '<a class="page-numbers" href="'.get_bloginfo('url').'/'.__('publication').'/publicationage/'.$i.'/">'.$i.'</a>';
                            }
                        }
                        echo '<a class="next page-numbers" href="'.get_bloginfo('url').'/'.__('publication').'/page/2/">&gt;</a>';
                    }
                ?>
            </div>
        </div>
        <?php
        $content=ob_get_clean();
        wp_send_json_success($content);
        die();
    }
?>