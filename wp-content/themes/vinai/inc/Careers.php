<?php
    add_action('wp_ajax_ajax_jobs_type', 'ajax_jobs_type_func');
    add_action('wp_ajax_nopriv_ajax_jobs_type', 'ajax_jobs_type_func');

    function ajax_jobs_type_func(){
        // if(!wp_verify_nonce( $_POST['nonce'], "ajax_loading_jobs_careers")){
        //     wp_send_json_error('None?');
        // };
        $jobType = isset($_POST['jobType']) ? $_POST['jobType'] : null;
        if(!$jobType) wp_send_json_error('jobType?');
        $_careers = new WP_Query([
            'post_type'   => 'careers',
            'posts_per_page' => 5, //16
            'jobs-category' => $jobType,
            'post_status' => 'publish',
        ]);
        ob_start();
        if($_careers->have_posts(  )){
            contentJobsAjax($_careers);
        }else{
            wp_send_json_error('<p>'.__('We have not opened new position yet, please check back later!', 'corex').'</p>');
        }
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }

    add_action('wp_ajax_ajax_navigation_load_jobs', 'ajax_navigation_load_jobs_func');
    add_action('wp_ajax_nopriv_ajax_navigation_load_jobs', 'ajax_navigation_load_jobs_func');
    function ajax_navigation_load_jobs_func(){
        // if(!wp_verify_nonce( $_POST['nonce'], "ajax_loading_jobs_careers")){
        //     wp_send_json_error('None?');
        // };

        $_type = isset($_POST['type']) ? $_POST['type'] : null;
        if(!$_type) wp_send_json_error('jobType?');
        $paged = isset($_POST['paged']) ? intval($_POST['paged']) : '';
        if($paged <= 0 || !$paged || !is_numeric($paged)) wp_send_json_error('Paged?');
        $_team = isset($_POST['team']) ? $_POST['team'] : null;
        $_location = isset($_POST['location']) ? $_POST['location'] : null;
        $_careers = new WP_Query([
            'post_type'   => 'careers',
            'posts_per_page' => 5, //16
            'paged'  => $paged,
            'jobs-category' => $_type,
            'location-category' => $_location,
            'teams-category' => $_team,
            'post_status' => 'publish',
        ]);
        ob_start();
        if($_careers->have_posts(  )){
            contentJobsAjax($_careers, $paged);
        }else{
            wp_send_json_error('<p>'.__('We have not opened new position yet, please check back later!', 'corex').'</p>');
        }
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }

    function contentJobsAjax($_careers, $paged = 1){
        echo '<ul>';
            while ($_careers->have_posts(  )) {
                $_careers->the_post(  );
                $_careerID = get_the_ID(  );
                $_title = get_the_title($_careerID);
                $_link = get_the_permalink($_careerID);
                $_locations  = get_the_terms( $_careerID, "location-category" );
                ?>
                    <li>
                        <a href="<?php echo $_link; ?>" class="--name"><?php echo $_title; ?></a>
                        <p class="--location">
							<i class="fas fa-map-marker-alt"></i>
                            <?php
                                if($_locations){
                                    $i = 0;
                                    foreach ($_locations as $location) {
                                        $i++;
                                        $termID = $location->term_id;
                                        $shortened_name = get_field('location_shortened_name', 'term_'.$termID);
                                        if( $i == count($_locations)){
                                            echo $shortened_name;
                                        }else{
                                            echo $shortened_name . ', ';
                                        }
                                    }
                                }
                            ?>
                        </p>
                    </li>
                <?php
            }
            echo '</ul>';
        ?>
            <div class="pagination-common js_panigator">
                <?php echo wp_navigation_paged_ajax($_careers, $paged); ?>
            </div>
        <?php
    }
