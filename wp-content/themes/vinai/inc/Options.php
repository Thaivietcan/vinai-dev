<?php
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Other',
        'menu_title'	=> 'Other Settings',
        'parent_slug'	=> 'theme-general-settings',
    ));
}

//home
function register_acf_options_pages_home() {

    // Check function exists.
    if( !function_exists('acf_add_options_page'))
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Homes page'),
        'menu_title'    => __('Homes page'),
        'menu_slug'     => 'homes_page',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'position' => '4.1',
        'icon_url' => 'dashicons-admin-home',
    ));
}
// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages_home');

//about us
function register_acf_options_pages_about_us() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('About us'),
        'menu_title'    => __('About us'),
        'menu_slug'     => 'about_page',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'position' => '5.1',
        'icon_url' => 'dashicons-info',
    ));
}
// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages_about_us');

// careers
function register_acf_options_pages_careers() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Careers content'),
        'menu_title'    => __('Careers content'),
        'menu_slug'     => 'careers_content',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'parent'     => 'edit.php?post_type=careers',
    ));
}
// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages_careers');

// Seminar
function register_acf_options_pages_seminar() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Seminar content'),
        'menu_title'    => __('Seminar content'),
        'menu_slug'     => 'seminar_content',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'parent'     => 'edit.php?post_type=seminar',
    ));
}
// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages_seminar');


// Out teams
function register_acf_options_pages_our_team() {
    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Our team'),
        'menu_title'    => __('Our team'),
        'menu_slug'     => 'our-team',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'position' => '5.2',
        'icon_url' => 'dashicons-groups',
    ));
}
add_action('acf/init', 'register_acf_options_pages_our_team');

// Menu Product
function register_acf_options_pages_menu_product() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Menu Products'),
        'menu_title'    => __('Menu products'),
        'menu_slug'     => 'menu-products',
        'redirect'      => true,
        'position' => '10',
    ));
}
add_action('acf/init', 'register_acf_options_pages_menu_product');

// Product
function register_acf_options_pages_product() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Products'),
        'menu_title'    => __('Products'),
        'menu_slug'     => 'products',
        'redirect'      => true,
        'position' => '5.5',
        'icon_url' => 'dashicons-products',
    ));
}
add_action('acf/init', 'register_acf_options_pages_product');

// Product Driver occupants
function register_acf_options_pages_product_driver() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Products Driver'),
        'menu_title'    => __('Products Driver'),
        'menu_slug'     => 'products-driver',
        'redirect'      => true,
        'position' => '5.6',
        'icon_url' => 'dashicons-products',
    ));
}
add_action('acf/init', 'register_acf_options_pages_product_driver');

// Product Surround view
function register_acf_options_pages_product_surround() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Products Surround'),
        'menu_title'    => __('Products Surround'),
        'menu_slug'     => 'products-surround',
        'redirect'      => true,
        'position' => '5.7',
        'icon_url' => 'dashicons-products',
    ));
}
add_action('acf/init', 'register_acf_options_pages_product_surround');

// Product Guard Pro
function register_acf_options_pages_product_guard() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Products Guard'),
        'menu_title'    => __('Products Guard'),
        'menu_slug'     => 'products-guard',
        'redirect'      => true,
        'position' => '5.8',
        'icon_url' => 'dashicons-products',
    ));
}
add_action('acf/init', 'register_acf_options_pages_product_guard');

// Product Platform
function register_acf_options_pages_product_platform() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Products Platform'),
        'menu_title'    => __('Products Platform'),
        'menu_slug'     => 'products-platform',
        'redirect'      => true,
        'position' => '5.9',
        'icon_url' => 'dashicons-products',
    ));
}
add_action('acf/init', 'register_acf_options_pages_product_platform');

// Programs
function register_acf_options_pages_programs() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Programs'),
        'menu_title'    => __('Programs'),
        'menu_slug'     => 'programs',
        'redirect'      => true,
        'position' => '5.4',
        'icon_url' => 'dashicons-welcome-view-site',
    ));
}
add_action('acf/init', 'register_acf_options_pages_programs');

//AI Residency
function register_acf_options_pages_ai_residency() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('AI Residency'),
        'menu_title'    => __('AI Residency'),
        'menu_slug'     => 'ai-residency',
        'parent_slug'   => 'programs',
    ));
}
// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages_ai_residency');

//Entrepreneurship
function register_acf_options_pages_entrepreneurship() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Entrepreneurship'),
        'menu_title'    => __('Entrepreneurship'),
        'menu_slug'     => 'entrepreneurship',
        'parent_slug'   => 'programs',
    ));
}
// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages_entrepreneurship');


// Research Publication
/*
function register_acf_options_pages_publication() {

    if( !function_exists('acf_add_options_page') )
        return;

    $option_page = acf_add_options_page(array(
        'page_title'    => __('Publications old'),
        'menu_title'    => __('Publications old'),
        'menu_slug'     => 'publication',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'position' => '5.5',
        'icon_url' => 'dashicons-media-document',
    ));
}
add_action('acf/init', 'register_acf_options_pages_publication');


// Applied AI
function register_acf_options_pages_applied_ai() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Applied AI'),
        'menu_title'    => __('Applied AI'),
        'menu_slug'     => 'applied-ai',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'position' => '5.6',
        'icon_url' => 'dashicons-image-filter',
    ));
}
// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages_applied_ai');
*/


// AI Technology
function register_acf_options_pages_ai_technology() {

    // Check function exists.
    if( !function_exists('acf_add_options_page'))
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('AI Technology'),
        'menu_title'    => __('AI Technology'),
        'menu_slug'     => 'ai-technology',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'position' => '5.7',
        'icon_url' => 'dashicons-awards',
    ));
}
// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages_ai_technology');