<?php
function vn_to_str ($str){
 
    $unicode = array(
        'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
        'd'=>'đ',
        'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
        'i'=>'í|ì|ỉ|ĩ|ị',
        'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
        'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
        'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
        'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        'D'=>'Đ',
        'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
        'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
        'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
        'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
        'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
    );
    foreach($unicode as $nonUnicode=>$uni){
        $str = preg_replace("/($uni)/i", $nonUnicode, $str);
    }
    $str = str_replace(' ','_',$str);
    return $str;
}

/**
 * Get terms
 * @method fn_get_terms()
 * @param string $term_name, $orderby, $order, $parent_id
 * @return object $terms
 */
function fn_get_terms($term_name, $parent_id = 0, $orderby = 'date', $order = 'DESC') {
    $taxonomies = array( 
        $term_name,
    );
    $args = array(
        'orderby'                => $orderby,
        'order'                  => $order,
        'hide_empty'             => 0,
        'hierarchical'           => true,
        'child_of'               => 0,
        'parent'                 => $parent_id,
    );
    $terms = get_terms($taxonomies, $args);
    return $terms;
}

/**
 * Get posts
 * @method fn_get_posts()
 * @param string $post_type
 * @param int $page, $post_per_page
 * @param array $custom_args
 * @return object $posts
 */
function fn_get_posts($post_type = 'post', $page = 1, $post_per_page = -1, $custom_args = array()) {
    $args = array(
        'post_type' => $post_type,
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
    );
    if(count($custom_args) > 0){
        $args = array_merge($args, $custom_args);
    }

    $posts = new WP_Query($args);
    return $posts;
}