<?php
    add_action( 'wp_ajax_ajax_load_member', 'ajax_load_member_func');
    add_action( 'wp_ajax_nopriv_ajax_load_member', 'ajax_load_member_func');

    function ajax_load_member_func(){
        // if(!wp_verify_nonce( $_POST['nonce'], "loading_out_teams_post")){
        //     wp_send_json_error('None?');
        // };
        $name = isset($_POST['name']) ? $_POST['name'] : null;
        $team = isset($_POST['team']) ? intval($_POST['team']) : 0;
        $current = isset($_POST['current']) ? intval($_POST['current']) : '';
        if( $current <= 0 || !$current || !is_numeric($current) ) wp_send_json_error('Current?');
        $paged = isset($_POST['paged']) ? intval($_POST['paged']) : '';
        if($paged <= 0 || !$paged || !is_numeric($paged)) wp_send_json_error('Paged?');
        $members = get_field('employee_list', 'option');
        foreach ($members as $key => $member) {
            if(!empty($member['employee_hide'])){
                unset($members[$key]);
            }
        }
        $resultMembers = array();
        ob_start();
        if($members){
            if(!$name && $team == 0){
                $total = count($members);
                $pageNumber = ceil($total/$current);
                $limit = $current * $paged;
                $start = $limit - $current;
                renderMemberItem($members, $start, $limit, $pageNumber, $paged);
            }else{
                foreach ($members as $member) {
                    $index = stripos($member['employee_name'],$name);
                    $_team = $member['employee_teams'];
                    if($team == 0){
                        if(!$name){
                            array_push($resultMembers, $member);
                        }else{
                            if(is_numeric($index)){
                                array_push($resultMembers, $member);
                            }
                        }
                    }else{
                        if(!$name){
                            if($team == $_team){
                                array_push($resultMembers, $member);
                            }
                        }else{
                            if($team == $_team && is_numeric($index)){
                                array_push($resultMembers, $member);
                            }
                        }
                    }
                }

                if($resultMembers){
                    $total = count($resultMembers);
                    $pageNumber = ceil($total/$current);
                    $limit = $current * $paged;
                    $start = $limit - $current;
                    renderMemberItem($resultMembers, $start, $limit, $pageNumber, $paged);
                }
            }
        }else{
            wp_send_json_error('Members?');
        }
    ?>
    <?php
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }
?>
<?php
function renderMemberItem($arrayMember, $start, $limit, $pageNumber, $paged){
    echo '<div class="member_content">';
    $i = 0;
    foreach ($arrayMember as $value) {
        $i++;
        if($i > $start){
            $avatar = $value['employee_avatar'] ? $value['employee_avatar'] : NOT_AVATAR;
            echo '<div class="_item">
                <div class="_avatar_member">
                    <img src="'.$avatar.'" alt="">
                </div>
                <p class="_name">'.$value['employee_name'].'</p>
                <p class="_location">'.$value['employee_location'].'</p>
                <p class="_excrept">'.$value['employee_maxim'].'</p>
            </div>';
        }
        if($i == $limit){
            break;
        }
    }
    echo '</div>';
    ?>
    <div class="pagination-common js_panigator">
        <div class="paginate_links">
            <?php
                if($pageNumber > 1){
                    if($paged !== 1){
                        echo '<a class="next page-numbers" href="'.get_bloginfo('url').'/'.__('our-teams').'/page/'. ($paged - 1) .'/"><</a>';
                    }
                    for ($i = 1; $i <= $pageNumber; $i++) {
                        if($i == $paged){
                            echo '<span class="page-numbers current">'.$i.'</span>';
                        }else{
                            echo '<a class="page-numbers" href="'.get_bloginfo('url').'/'.__('our-teams').'/page/'.$i.'/">'.$i.'</a>';
                        }
                    }
                    if($paged !== intval($pageNumber)){
                        echo '<a class="next page-numbers" href="'.get_bloginfo('url').'/'.__('our-teams').'/page/'. ($paged + 1) .'/">></a>';
                    }
                }
            ?>
        </div>
    </div>
    <?php
}
?>
<?php
    add_action( 'wp_ajax_ajax_search_member', 'ajax_search_member_func');
    add_action( 'wp_ajax_nopriv_ajax_search_member', 'ajax_search_member_func');
    function ajax_search_member_func() {
        // if(!wp_verify_nonce( $_POST['nonce'], "loading_out_teams_post")){
        //     wp_send_json_error('None?');
        // };
        $current = isset($_POST['current']) ? intval($_POST['current']) : null;
        if( $current <= 0 || !$current || !is_numeric($current) ) wp_send_json_error('Current?');
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $team = isset($_POST['team']) ? intval($_POST['team']) : 0;
        $members = get_field('employee_list', 'option');
        foreach ($members as $key => $member) {
            if(!empty($member['employee_hide'])){
                unset($members[$key]);
            }
        }
        $resultMembers = array();
        if($members){
            foreach ($members as $member) {
                $index = stripos($member['employee_name'],$name);
                $_team = $member['employee_teams'];
                if($team == 0){
                    if(!$name){
                        array_push($resultMembers, $member);
                    }else{
                        if(is_numeric($index)){
                            array_push($resultMembers, $member);
                        }
                    }
                }else{
                    if(!$name){
                        if($team == $_team){
                            array_push($resultMembers, $member);
                        }
                    }else{
                        if($team == $_team && is_numeric($index)){
                            array_push($resultMembers, $member);
                        }
                    }
                }
            }


        }
        else{
            wp_send_json_error('Members?');
        }
        if($resultMembers){
            $total = count($resultMembers);
            $pageNumber = ceil($total/$current);
            $i = 0;
            ob_start();
            echo '<div class="member_content">';
            foreach ($resultMembers as $key => $value) {
                $i++;
                    $avatar = $value['employee_avatar'] ? $value['employee_avatar'] : NOT_AVATAR;
                    echo '<div class="_item">
                        <div class="_avatar_member">
                            <img src="'.$avatar.'" alt="">
                        </div>
                        <p class="_name">'.$value['employee_name'].'</p>
                        <p class="_location">'.$value['employee_location'].'</p>
                        <p class="_excrept">'.$value['employee_maxim'].'</p>
                    </div>';
                if($i == $current){
                    break;
                }
            }
            echo '</div>';
        }else{
            wp_send_json_error('<p>'.__('There are no relevant search results', 'corex').'</p>');
        }
    ?>
        <div class="pagination-common js_panigator">
            <div class="paginate_links">
                <?php
                    if($pageNumber > 1){
                        for ($i = 1; $i <= $pageNumber; $i++) { 
                            if($i === 1){
                                echo '<span class="page-numbers current">'.$i.'</span>';
                            }else{
                                echo '<a class="page-numbers" href="'.get_bloginfo('url').'/'.__('our-teams').'/page/'.$i.'/">'.$i.'</a>';
                            }
                        }
                        echo '<a class="next page-numbers" href="'.get_bloginfo('url').'/'.__('our-teams').'/page/2/">&gt;</a>';
                    }
                ?>
            </div>
        </div>
    <?php
        $content = ob_get_clean();
        wp_send_json_success($content);
        die();
    }
?>