<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_careers', 0);
function tu_reg_post_type_careers() {
    //Change this when creating post type
    $post_type_name = __('Careers', 'corex');
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_menu_position = 5;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('All', 'corex').' '.$post_type_name_lower,
        'add_new' => __('Add new', 'corex'),
        'add_new_item' => __('Add new', 'corex').' '.$post_type_name_lower,
        'edit_item' => __('Edit', 'corex').' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('View post', 'corex'),
        'search_items' => __('Search', 'corex'),
        'not_found' => __('Not found', 'corex'),
        'not_found_in_trash' => __('Not found in trash', 'corex'),
        'view' => __('View', 'corex').' '.$post_type_name_lower,

    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_in_menu' => true,
        'show_ui' => true,
        'hierarchical' => false,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-businesswoman',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => __('recruitment-posts','corex'),
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false,
		'show_in_admin_bar'     => true,
		'can_export'            => true,
    );

    register_post_type('careers', $args);
    
    /** Register Taxonomies */

    register_taxonomy('teams-category', array('careers'), array(
        "hierarchical" => true,
        "label" => __('Categories teams', 'corex'),
        "singular_label" => __('Categories teams', 'corex'),
        "rewrite" => array('slug' => __('teams', 'corex'), 'hierarchical' => true),
        "show_admin_column" => true,
        "show_ui" => true,
        "public" => true,
        "show_in_nav_menus" => true,
        "show_tagcloud" => true,
        "hierarchical" => true,
    ));

    register_taxonomy('location-category', array('careers'), array(
        "hierarchical" => true,
        "label" => __('Categories location', 'corex'),
        "singular_label" => __('Categories location', 'corex'),
        "rewrite" => array('slug' => __('location-post', 'corex'), 'hierarchical' => true),
        "show_admin_column" => true,
        "show_ui" => true,
        "public" => true,
        "show_in_nav_menus" => true,
        "show_tagcloud" => true,
        "hierarchical" => true,
    ));
    register_taxonomy('jobs-category', array('careers'), array(
        "hierarchical" => true,
        "label" => __('Categories jobs', 'corex'),
        "singular_label" => __('Categories jobs', 'corex'),
        "rewrite" => array('slug' => __('jobs-post', 'corex'), 'hierarchical' => true),
        "show_admin_column" => true,
        "show_ui" => true,
        "public" => true,
        "show_in_nav_menus" => true,
        "show_tagcloud" => true,
        "hierarchical" => true,
    ));
}


// seminar post type
add_action('init', 'tu_reg_post_type_seminar', 0);
function tu_reg_post_type_seminar() {
    //Change this when creating post type
    $post_type_name = __('Seminar', 'corex');
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_menu_position = 6;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('All', 'corex').' '.$post_type_name_lower,
        'add_new' => __('Add new', 'corex'),
        'add_new_item' => __('Add new', 'corex').' '.$post_type_name_lower,
        'edit_item' => __('Edit', 'corex').' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('View post', 'corex'),
        'search_items' => __('Search', 'corex'),
        'not_found' => __('Not found', 'corex'),
        'not_found_in_trash' => __('Not found in trash', 'corex'),
        'view' => __('View', 'corex').' '.$post_type_name_lower,

    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_in_menu' => true,
        'show_ui' => true,
        'hierarchical' => false,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-admin-site-alt2',
        'supports' => array('title', 'editor', 'thumbnail'),
        'rewrite' => array(
            'slug' => __('seminar-posts','corex'),
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false,
		'show_in_admin_bar'     => true,
		'can_export'            => true,
    );

    register_post_type('seminar', $args);
    
    /** Register Taxonomies */

    register_taxonomy('seminar-series', array('seminar'), array(
        "hierarchical" => true,
        "label" => __('Seminar series', 'corex'),
        "singular_label" => __('Seminar series', 'corex'),
        "rewrite" => array('slug' => __('seminar-series', 'corex'), 'hierarchical' => true),
        "show_admin_column" => true,
        "show_ui" => true,
        "public" => true,
        "show_in_nav_menus" => true,
        "show_tagcloud" => true,
        "hierarchical" => true,
    ));
}

// workshops post type
add_action('init', 'tu_reg_post_type_workshop', 0);
function tu_reg_post_type_workshop() {
    //Change this when creating post type
    $post_type_name = __('Workshops', 'corex');
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_menu_position = 8;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('All', 'corex').' '.$post_type_name_lower,
        'add_new' => __('Add new', 'corex'),
        'add_new_item' => __('Add new', 'corex').' '.$post_type_name_lower,
        'edit_item' => __('Edit', 'corex').' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('View post', 'corex'),
        'search_items' => __('Search', 'corex'),
        'not_found' => __('Not found', 'corex'),
        'not_found_in_trash' => __('Not found in trash', 'corex'),
        'view' => __('View', 'corex').' '.$post_type_name_lower,

    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_in_menu' => true,
        'show_ui' => true,
        'hierarchical' => false,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-admin-multisite',
        'supports' => array('title', 'thumbnail'),
        

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false,
		'show_in_admin_bar'     => true,
		'can_export'            => true,
    );

    register_post_type('workshop', $args);
}

// publications post type
add_action('init', 'tu_reg_post_type_publications', 0);
function tu_reg_post_type_publications() {
    //Change this when creating post type
    $post_type_name = __('Publications', 'corex');
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_menu_position = 7;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('All', 'corex').' '.$post_type_name_lower,
        'add_new' => __('Add new', 'corex'),
        'add_new_item' => __('Add new', 'corex').' '.$post_type_name_lower,
        'edit_item' => __('Edit', 'corex').' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('View post', 'corex'),
        'search_items' => __('Search', 'corex'),
        'not_found' => __('Not found', 'corex'),
        'not_found_in_trash' => __('Not found in trash', 'corex'),
        'view' => __('View', 'corex').' '.$post_type_name_lower,

    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_in_menu' => true,
        'show_ui' => true,
        'hierarchical' => false,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-media-interactive',
        'supports' => array('title', 'thumbnail'),
        'rewrite' => array(
            'slug' => __('publication-posts','corex'),
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false,
		'show_in_admin_bar'     => true,
		'can_export'            => true,
    );

    register_post_type('publications', $args);
}
