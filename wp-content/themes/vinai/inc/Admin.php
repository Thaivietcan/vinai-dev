<?php
/**
  * edit logo login
*/
function banv_custom_login(){ ?>
    <style type="text/css">
        #login h1 a{
            background-image: url('<?php echo THEME_ASSETS.'/images/logo-admin.png' ?>');
            width: 310px;
            height: 80px;
            background-size: contain;
        }
    </style>
<?php }
add_action('login_enqueue_scripts', 'banv_custom_login');

/**
 * edit href logo login wp.org -> home_url('/');
 */
add_filter( 'login_headerurl', 'login_logo_url');
function login_logo_url() {
    return HOME_URL;
}

/**
 *add logo admin container
 */
// function banv_admin_logo(){
//     echo '<img src="'.THEME_ASSETS.'/images/logo-admin.png'.'" width="300" style="display: block; margin-top: 20px;">';
// }
// add_action('admin_notices', 'banv_admin_logo');

/**
 * footer
 */

function banv_admin_footer($text){
    $text = '<p id="footer-left" class="alignleft">
               <p>
                    Email: contact@vinai.io | job@vinai.io
               <p>
               <p>
                    Location: Hanoi & HCMC | Vietnam
               </p>
           </p>';
    return $text;
}
add_filter('admin_footer_text','banv_admin_footer');

/**
  * edit favicon admin
*/
function favicon_admin() {
    echo '<link rel="Shortcut Icon" type="image/x-icon" href="' . get_template_directory_uri() . '/assets/images/favicon.png" />';
}
add_action( 'admin_head', 'favicon_admin' );

/**
  * remove wp bar login
*/
function example_admin_bar_remove_logo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'wp-logo' );
}
add_action( 'wp_before_admin_bar_render', 'example_admin_bar_remove_logo', 0 );