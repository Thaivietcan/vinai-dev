<?php

class ContactForm {
    private static $_instance = null;
    private $fields;

    public function __construct($fields = array()) {
        $this->fields = $fields;
        add_action('wp_ajax_applyFormAjax', array($this, 'applyFormAjax'));
        add_action('wp_ajax_nopriv_applyFormAjax', array($this, 'applyFormAjax'));
        add_action('init', array($this, 'initContactPostType'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue'));
        add_action( 'manage_posts_extra_tablenav', array($this, 'admin_order_list_top_bar_button'), 20, 1 );
        add_action('admin_init', array($this, 'sub_menu_pages_export_contact_handle'));
    }

    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function security() {
        wp_nonce_field( 'apply_form_submit', 'apply_nonce' );
    }

    public function enqueue() {
        wp_enqueue_script('contact-form-js', THEME_URI . '/inc/contact-form/ContactForm.js', array('jquery'));
        $wp_script_data = array(
            'AJAX_URL' => ADMIN_AJAX_URL,
            'HOME_URL' => HOME_URL
        );
        wp_localize_script('contact-form-js', 'obj', $wp_script_data);
    }

    /**
     * Post Type: Apply Form.
     */
    public function initContactPostType() {
        $labels = [
            "name" => __("Apply Form", "corex"),
            "singular_name" => __("Apply Form", "corex"),
        ];

        $args = [
            "label" => __("Apply Form", "corex"),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "show_in_rest" => false,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => false,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "delete_with_user" => false,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => ["slug" => "contact", "with_front" => true],
            "query_var" => true,
            "menu_icon" => "dashicons-buddicons-pm",
            "supports" => ["title", "custom-fields"],
            'capabilities' => array(
                'create_posts' => false
            )
        ];

        register_post_type("apply", $args);
    }

    public function applyFormAjax() {
        $errors = array();
        // if (!isset($_POST['apply_nonce']) || !wp_verify_nonce($_POST['apply_nonce'], 'apply_form_submit')) {
        //     wp_send_json_error(__('Could not confirm security key', 'corex'));
        //     exit;
        // }

        $fields = array();

        foreach ($_POST as $key => $data) {
            if (in_array($key, $this->fields)) {
                if (empty($data)) {
                    array_push($errors, array($key));
                } else {
                    $fields[$key] = sanitize_text_field($data);
                }
            }
        }

        if (!empty($errors)) {
            wp_send_json_error(__('Please enter full information: ', 'corex'));
        }

        // validator phone
        if (!empty($fields['phone'])) {
            if (!preg_match('/^08([0-9]{8})|09([0-9]{8})|03([0-9]{8})|05([0-9]{8})|07([0-9]{8})$/', $fields['phone'])) {
                wp_send_json_error(__('invalid phone number!', 'corex'));
            }
        }

        // validator email
        if(!empty($fields['email'])) {
            if (!preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',$fields['email'])){
                wp_send_json_error(__('Invalid email!', 'corex'));
            }
        }

        $data = array();
        $data['resume'] = (isset($_POST['resume'])) ? sanitize_text_field($_POST['resume']) : null;
        /*file*/
        if ( isset( $_FILES['resume'] ) ) {
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/media.php');

            if ( UPLOAD_ERR_OK == $data['resume']['error']  ) {

                $attach_id = media_handle_upload( 'resume', 0 );

                if ( !is_wp_error($attach_id) ) {
                    $fields['resume'] = wp_get_attachment_url($attach_id);
                }else {
                    wp_send_json_error(__('File upload error. Please try again!', 'corex'));
                }
            } else {
                wp_send_json_error(__('File format must be .pdf, .docx, .dox, .xlsx, .xls. Please check the format and re-send.', 'corex'));
            }
        }else{
            wp_send_json_error(__('Upload file must not be blank!', 'corex'));
        }

        $post_arr = array(
            'post_title' => $fields['name'].' - '.$fields['phone'].' - '.$fields['job'],
            'post_content' => '',
            'post_status' => 'pending',
            'post_type' => 'apply',
            'meta_input' => $fields
        );

        $resp = wp_insert_post($post_arr);
        if ($resp) {
            wp_send_json_success(__('Submitted successfully', 'corex'));
        } else {
            wp_send_json_error(__('Error, could not create new contact', 'corex'));
        }
    }

    public function admin_order_list_top_bar_button( $which ) {
        global $typenow;

        if ( 'apply' === $typenow && 'top' === $which ) {
            ?>
                <div class="alignleft actions custom">
                    <input type="submit" name="submit_extract" id="submit-extract" class="button button-primary" value="<?php _e('Extract all', 'corex'); ?>">
                </div>
            <?php
        }
    }

    //sub_menu_pages_export_contact_handle
    public function sub_menu_pages_export_contact_handle() {
        require_once(THEME_DIR.'/inc/plugins/PHPExcel/PHPExcel/IOFactory.php');
        if(isset($_GET['submit_extract'])){
            $apply = new WP_Query(array(
                'post_type'       => 'apply',
                'posts_per_page'  => -1,
                'post_status'     => 'any',
            ));
            $file_name = 'VinAIApplyAll-'.date('d-m-Y');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->setTitle($file_name);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'STT')
                ->setCellValue('B1', 'Full name')
                ->setCellValue('C1', 'Email')
                ->setCellValue('D1', 'Phone number')
                ->setCellValue('E1', 'Linkedin profile')
                ->setCellValue('F1', 'Resume')
                ->setCellValue('G1', 'Job')
                ->setCellValue('H1', 'Feedback status')
                ->setCellValue('I1', 'Registration Date');
            $stt = 0;
            $row = 1;
            if($apply->have_posts(  )){
                while ($apply->have_posts(  )) {
                    $apply->the_post(  );
                    $stt++;
                    $row++;
                    $_postID = get_the_ID(  );
                    $_applyName = get_field('name', $_postID);
                    $_applyPhone = get_field('phone', $_postID);
                    $_applyEmail = get_field('email', $_postID);
                    $_applyLinkedin = get_field('linkedin', $_postID);
                    $_applyResume = get_field('resume', $_postID);
                    $_applyJob = get_field('job', $_postID);
                    $_applyStatus = get_post_status($_postID);
                    $_applyDate = get_the_date('d/m/Y', $_postID);
    
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row, $stt)
                    ->setCellValue('B' . $row, $_applyName)
                    ->setCellValue('C' . $row, $_applyEmail)
                    ->setCellValue('D' . $row, $_applyPhone)
                    ->setCellValue('E' . $row, $_applyLinkedin)
                    ->setCellValue('F' . $row, $_applyResume)
                    ->setCellValue('G' . $row, $_applyJob)
                    ->setCellValue('H' . $row, $_applyStatus)
                    ->setCellValue('I' . $row, $_applyDate);
                }
            }
            ob_end_clean();
            ob_start();
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
            header('Cache-Control: max-age=0');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    
            $objWriter->save('php://output'); 
            
            exit();
        }
    }
}

//Adding a custom bulk action in the dropdown
add_filter('bulk_actions-edit-apply', function($bulk_actions) {
	$bulk_actions['extract-apply'] = __('Extract', 'corex');
	return $bulk_actions;
});

//Making our custom bulk action do something
add_filter('handle_bulk_actions-edit-apply', function($redirect_url, $action, $post_ids) {
	if ($action == 'extract-apply') {
        require_once(THEME_DIR.'/inc/plugins/PHPExcel/PHPExcel/IOFactory.php');
        $file_name = 'VinAIApply-'.date('d-m-Y');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle($file_name);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'STT')
            ->setCellValue('B1', 'Full name')
            ->setCellValue('C1', 'Email')
            ->setCellValue('D1', 'Phone number')
            ->setCellValue('E1', 'Linkedin profile')
            ->setCellValue('F1', 'Resume')
            ->setCellValue('G1', 'Job')
            ->setCellValue('H1', 'Feedback status')
            ->setCellValue('I1', 'Registration Date');
        $stt = 0;
        $row = 1;
        if($post_ids){
            foreach ($post_ids as $post_id) {
                $stt++;
                $row++;
                $_applyName = get_field('name', $post_id);
                $_applyPhone = get_field('phone', $post_id);
                $_applyEmail = get_field('email', $post_id);
                $_applyLinkedin = get_field('linkedin', $post_id);
                $_applyResume = get_field('resume', $post_id);
                $_applyJob = get_field('job', $post_id);
                $_applyStatus = get_post_status($post_id);
                $_applyDate = get_the_date('d/m/Y', $post_id);

                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $row, $stt)
                ->setCellValue('B' . $row, $_applyName)
                ->setCellValue('C' . $row, $_applyEmail)
                ->setCellValue('D' . $row, $_applyPhone)
                ->setCellValue('E' . $row, $_applyLinkedin)
                ->setCellValue('F' . $row, $_applyResume)
                ->setCellValue('G' . $row, $_applyJob)
                ->setCellValue('H' . $row, $_applyStatus)
                ->setCellValue('I' . $row, $_applyDate);
            }
            ob_end_clean();
            ob_start();
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;
        }
	}
}, 10, 3);