<?php
add_action('init', 'posts_taxonomy_func', 0);
function posts_taxonomy_func() {
 
    /* Biến $label chứa các tham số thiết lập tên hiển thị của Taxonomy
     */
    $labels = array(
            'name' => 'Research Area',
            'singular' => 'Research Area',
            'menu_name' => 'Research Area'
    );

    /* Biến $args khai báo các tham số trong custom taxonomy cần tạo
     */
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_rest' => true, // add support for Gutenberg editor
        'publicly_queryable' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => true,
        'show_admin_column' => true,
    );

    /* Hàm register_taxonomy để khởi tạo taxonomy
     */
    register_taxonomy('research-area', array('post', 'publications'), $args);
}

// load navigation blog
add_action('wp_ajax_ajax_navigation_load_post_category', 'ajax_navigation_load_post_category_func');
add_action('wp_ajax_nopriv_ajax_navigation_load_post_category', 'ajax_navigation_load_post_category_func');
function ajax_navigation_load_post_category_func(){
    // if(!wp_verify_nonce( $_POST['nonce'], "ajax_loading_post")){
    //     wp_send_json_error('None?');
    // };
    $paged = isset($_POST['paged']) ? intval($_POST['paged']) : '';
    if($paged <= 0 || !$paged || !is_numeric($paged)) wp_send_json_error('Paged?');
    $perPage = isset($_POST['perPage']) ? intval($_POST['perPage']) : 9;
    $slug = isset($_POST['slug']) ? $_POST['slug'] : null;
    if(!$slug) wp_send_json_error('Slug?');

    $posts = new WP_Query([
        'post_type'      => 'post',
        'category_name'  => $slug,
        'paged'          => $paged,
        'posts_per_page' => $perPage,
        'post_status'    => 'publish',
    ]);
    ob_start();
    ?>
    <div class="_list_news list-publications-global d-flex flex-wrap">
    <?php
    if($posts->have_posts()){
        while ($posts->have_posts()) {
            $posts->the_post();
            $post_id = get_the_ID();
            $permalink = get_the_permalink($post_id);
            $thumbnail = get_the_post_thumbnail_url($post_id, 'full') ?: NOT_IMAGE;
            $title = get_the_title($post_id);
			$category = get_the_category( $post_id);
			$theExcerpt = get_the_excerpt($post_id);
            $day = get_the_time('d/m/Y', $post_id);
            ?>
				<div class="__item">
					<a href="<?php echo $permalink ?>" class="thumbnail">
						<div class="--bg"
							style="background-image: url(<?php echo $thumbnail ?>)">
						</div>
					</a>
					<div class="text">
						<span class="sub-heading">
							<?php 
								foreach ( $category as $cat){
								   echo $cat->name;
								}
							?>
						</span>
						<a href="<?php echo $permalink ?>" class="--title">
							<h4><?php if (strlen($title) > 40) {
									echo substr($title, 0, 40) . '...';
								} else {
									echo $title;
								} ?></h4>
						</a>
						<p>
							<?php 
								if (strlen($theExcerpt) > 100) {
									echo substr($theExcerpt, 0, 100) . '(...)';
								} else {
									echo $theExcerpt;
								}
							?>
						</p>
						
						<div class="--meta d-flex align-items-center justify-content-between">
							<a href="<?php echo $permalink ?>" class="btn btn-view-detail">
								<span><?php _e('View details', 'corex'); ?></span>
								<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt="">
							</a>
							<p class="--time"><?php echo $time; ?></p>
						</div>
					</div>
				</div>
			<?php
        } wp_reset_postdata();
    }
    ?>
        </div>
        <div class="pagination-common js_panigator text-align-center">
            <?php wp_navigation_paged_ajax($posts, $paged); ?>
        </div>
    <?php
    $content = ob_get_clean();
    wp_send_json_success($content);
    die();
}

// load navigation Achievement
add_action('wp_ajax_ajax_navigation_load_post_achievement', 'ajax_navigation_load_post_achievement_func');
add_action('wp_ajax_nopriv_ajax_navigation_load_post_achievement', 'ajax_navigation_load_post_achievement_func');
function ajax_navigation_load_post_achievement_func(){
    // if(!wp_verify_nonce( $_POST['nonce'], "ajax_loading_post")){
    //     wp_send_json_error('None?');
    // };
    $paged = isset($_POST['paged']) ? intval($_POST['paged']) : '';
    if($paged <= 0 || !$paged || !is_numeric($paged)) wp_send_json_error('Paged?');
    $perPage = isset($_POST['perPage']) ? intval($_POST['perPage']) : 9;
    $slug = isset($_POST['slug']) ? $_POST['slug'] : null;
    if(!$slug) wp_send_json_error('Slug?');

    $posts_achi = new WP_Query([
        'post_type'      => 'post',
        'category_name'  => $slug,
        'paged'          => $paged,
        'posts_per_page' => $perPage,
        'post_status'    => 'publish',
    ]);
    ob_start();
    ?>
    <div class="_list_news list-publications-global d-flex flex-wrap">
    <?php
    if($posts_achi->have_posts()){
        while ($posts_achi->have_posts()) {
            $posts_achi->the_post();
            $post_id = get_the_ID();
            $permalink = get_the_permalink($post_id);
            $thumbnail = get_the_post_thumbnail_url($post_id, 'full') ?: NOT_IMAGE;
            $title = get_the_title($post_id);
			$category = get_the_category( $post_id);
			$theExcerpt = get_the_excerpt($post_id);
            $day = get_the_time('d/m/Y', $post_id);
            ?>
				<div class="__item">
					<a href="<?php echo $permalink ?>" class="thumbnail">
						<div class="--bg"
							style="background-image: url(<?php echo $thumbnail ?>)">
						</div>
					</a>
					<div class="text">
						<span class="sub-heading">
							<?php 
								foreach ( $category as $cat){
								   echo $cat->name;
								}
							?>
						</span>
						<a href="<?php echo $permalink ?>" class="--title">
							<h4><?php if (strlen($title) > 40) {
									echo substr($title, 0, 40) . '...';
								} else {
									echo $title;
								} ?></h4>
						</a>
						<p>
							<?php 
								if (strlen($theExcerpt) > 100) {
									echo substr($theExcerpt, 0, 100) . '(...)';
								} else {
									echo $theExcerpt;
								}
							?>
						</p>
						
						<div class="--meta d-flex align-items-center justify-content-between">
							<a href="<?php echo $permalink ?>" class="btn btn-view-detail">
								<span><?php _e('View details', 'corex'); ?></span>
								<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt="">
							</a>
							<p class="--time"><?php echo $time; ?></p>
						</div>
					</div>
				</div>
			<?php
        } wp_reset_postdata();
    }
    ?>
        </div>
        <div class="pagination-common js_panigator text-align-center">
            <?php wp_navigation_paged_ajax($posts_achi, $paged); ?>
        </div>
    <?php
    $content = ob_get_clean();
    wp_send_json_success($content);
    die();
}