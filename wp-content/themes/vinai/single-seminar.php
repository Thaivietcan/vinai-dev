<?php
get_header();
if (have_posts()) {
    while (have_posts()) {
        the_post();
        $post_id = get_the_ID();
        $thumbnail = get_the_post_thumbnail_url($post_id, 'full') ?: null;
        $featured_video = get_field('featured_video', $post_id) ?: null;
        $title = get_the_title($post_id);
        $work_place = get_field('work_place', $post_id);
        $dateObject = new DateTime(get_field('time_takes_place', $post_id));
        $timeStamp = $dateObject->getTimestamp();
        $timing = get_field('time_takes_place', $post_id) ? date('l, M d Y - h:i a', $timeStamp) . ' (GMT + 7)' : null;
        $video_recording = get_field('video_recording', $post_id) ?: null;
        $content = get_the_content($post_id) ?: null;
        $name_speakers = get_field('name_speakers', $post_id);
		$avatar = get_field('avatar', $post_id) ?: NOT_AVATAR;
		$about_speaker = get_field('about_speaker', $post_id);
		$abstract_speaker = get_field('abstract_speaker', $post_id);
?>
        <div class="seminar-page-single">
			<div class="section-1">
				<div class="tw-container">
					<div class="tw-content">
						<p class="location">
							<span><i class="fas fa-clock"></i>
							<?php echo $timing; ?></span>
						</p>
						<h2 class="tw-title"><?php echo $title; ?></h2>
					</div>
				</div>
				<div class="shape-one"></div>
				<div class="shape-two"></div>
			</div>
			
			<div class="section-2">
				<div class="tw-container">
					<div class="tw-content">
						<div class="about-speaker d-flex justify-content-between flex-direction-colum">
							<div class="about-img w-100">
								<img src="<?php echo $avatar; ?>" />
							</div>
							<div class="about-content w-100">
								<?php if(!empty($about_speaker)) : ?>
									<span class="sub-heading"><?php _e('About the speaker', 'corex'); ?></span>
									<p><?php echo $about_speaker; ?></p>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<?php if(!empty($abstract_speaker)) : ?>
				<div class="section-3">
					<div class="tw-container">
						<div class="tw-content">
							<div class="abstract-speaker">
								<div class="abstract-content">
									<span class="title-about"><i class="fas fa-file-alt"></i><?php _e('Abstract', 'corex'); ?></span>
									<p><?php echo $abstract_speaker; ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			
			<?php if(!empty($video_recording)) : ?>
				<div class="section-4">
					<div class="tw-container">
						<div class="tw-content">
							<div class="video-speaker">
								<p class="feature_image feature_video">
									<iframe src="<?php echo $featured_video; ?>?rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</p>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			
			<div class="section-5">
				<div class="tw-container">
					<div class="tw-content">
						<div class="upcoming_speakers">
							<h2 class="tw-title">Upcoming Speakers</h2>
							<div class="grid-speakers">
								<?php
								$upcoming_speaker_posts = fn_get_posts('seminar', 1, -1, [
									'post__not_in'    => [$post_id],
									'meta_query'      => array(
										array(
											'key'     => 'upcoming_speakers',
											'value'   => 'yes',
											'compare' => 'LIKE'
										)
									)
								]);
								if ($upcoming_speaker_posts->have_posts()) {
									while ($upcoming_speaker_posts->have_posts()) {
										$upcoming_speaker_posts->the_post();
										$upcoming_speaker_post_id = get_the_ID();
										$upcoming_speaker_post_avatar = get_field('avatar', $upcoming_speaker_post_id) ?: NOT_AVATAR;
										$upcoming_speaker_post_name = get_field('name_speakers', $upcoming_speaker_post_id);
										$upcoming_speaker_post_work_place = get_field('work_place', $upcoming_speaker_post_id);
										$upcoming_speaker_post_title = get_the_title($upcoming_speaker_post_id);
										$dateObject = new DateTime(get_field('time_takes_place', $upcoming_speaker_post_id));
										$timeStamp = $dateObject->getTimestamp();
										$upcoming_speaker_post_timing = get_field('time_takes_place', $upcoming_speaker_post_id) ? date('l, M d Y - h:i a', $timeStamp) . ' (GMT + 7)' : null;
										$upcoming_speaker_post_permalink = get_the_permalink($upcoming_speaker_post_id);
										?>
											<div class="speaker-item">
												<div class="avatar">
													<a href="<?php echo $upcoming_speaker_post_permalink; ?>"><img src="<?php echo $upcoming_speaker_post_avatar; ?>" alt="<?php echo $post_title; ?>"></a>
												</div>
												<div class="info">
													<a href="<?php echo $upcoming_speaker_post_permalink; ?>"><h3 class="name"><?php echo $upcoming_speaker_post_name; ?></h3></a>
													<p class="work_place">
														<?php 
															if (strlen($upcoming_speaker_post_work_place) > 50) {
																echo substr($upcoming_speaker_post_work_place, 0, 50) . '...';
															} else {
																echo $upcoming_speaker_post_work_place;
															} 
														?>
													</p>
													<p class="title-post"><i class="fas fa-microphone"></i>
														<?php 
															if (strlen($upcoming_speaker_post_title) > 90) {
																echo substr($upcoming_speaker_post_title, 0, 90) . '...';
															} else {
																echo $upcoming_speaker_post_title;
															} 
														?>
													</p>
													<p class="time_takes_place"><i class="fas fa-clock"></i> <?php echo $upcoming_speaker_post_timing; ?></p>
												</div>
											</div>
										<?php
									}
									wp_reset_postdata();
									// echo '</div></div>';
								} else {
									echo '<p style="margin: 0">Stay tuned to get inspired, informed, and motivated by our leading experts & notable speakers.</p>';
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
    <?php }
}
get_footer() ?>