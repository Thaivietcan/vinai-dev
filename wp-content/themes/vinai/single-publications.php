<?php
    get_header( );
    if(have_posts(  )) : 
        while (have_posts(  )): 
            the_post(  ); 
            $postId = get_the_ID(  );
            $thumbnail = get_the_post_thumbnail_url($postId, 'fullsize') ? get_the_post_thumbnail_url($postId, 'fullsize') : null;
            $title = get_the_title( $postId );
			$category = get_the_terms( $postId, 'research-area' );
            $authors = get_field('authors', $postId);
            $conference_name = get_field('conference_name', $postId);
            $abstract = get_field('abstract', $postId);
            $pdf = get_field('pdf', $postId);
            $bibtex = get_field('bibtex', $postId);
            $code = get_field('code', $postId);
            $blog = get_field('blog', $postId);
			$the_time = get_post_time( $postId );
?>
<div class="publication_detail detail_single">
	<div class="section-1">
		<div class="tw-container">
			<div class="tw-content">
				<p class="location">
					<span><i class="fas fa-clock"></i>
					<?php echo get_the_time('F j, Y'); ?></span>
					<span><i class="fas fa-folder-open"></i>
					<?php 
						foreach ( $category as $cat){
						   echo $cat->name;
						}
					?></span>
				</p>
				<h2 class="tw-title"><?php the_title(); ?></h2>
			</div>
		</div>
	</div>
	<div class="section-2">
		<div class="tw-container">
			<div class="tw-content">
				<div class="careers_detail_contents">
					<div class="the_content">
						<div class="careers_detail_inner d-flex justify-content-between flex-direction-colum">
							<div class="__left w-100" id="desktop">
								<div class="__left_item __left_item_info w-100">
									<ul>
										<li class="__item_time d-flex "><img src="<?php echo THEME_ASSETS . '/images/oclock.svg' ?>" /> <?php the_time( 'i' ); ?> minutes</li>
										<li class="__item_user d-flex"><img src="<?php echo THEME_ASSETS . '/images/user-icon.svg' ?>" /> <p><?php echo $authors; ?></p></li>
										<li class="__item_calender d-flex"><img src="<?php echo THEME_ASSETS . '/images/calender.svg' ?>" /> <?php echo $conference_name; ?></li>
									</ul>
								</div>
								
								<div class="__left_item __left_item_download w-100">
									<ul>
										<li class="__item_time d-flex ">
											<a class="btn-view-detail" href="<?php echo $pdf; ?>" download>Download <img src="<?php echo THEME_ASSETS . '/images/arrow-down.svg' ?>" /></a>
										</li>
									</ul>
								</div>
								
								<div class="__left_item __left_item_share w-100">
									<span>Share</span>
									<ul class="__list-social">
										<li class="__item_time">
											<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="btn_face popup_windown" rel="nofollow noopener"> 
												<i class="fab fa-facebook-f"></i>
											</a>
										</li>
										<li class="__item_time">
											<a href="https://twitter.com/share?url=<?php the_permalink(); ?>" class="btn_skype popup_windown" rel="nofollow noopener"> <i class="fab fa-twitter"></i></a>
										</li>
										<li class="__item_time">
											<a href="mailto:<?php the_permalink(); ?>"> <i class="fas fa-envelope-open"></i></a>
										</li>
									</ul>
								</div>
							</div>
							
							<div class="__right w-100">
								<div class="option">
									<?php
										if($abstract){
											echo '<span class="abstract item active"><a data-content="abstract">Abstract</a></span>';
										}
										if($pdf){
											echo '<span><a href="'.$pdf.'" target="_blank" class="pdf item">PDF</a></span>';
										}
										if($bibtex){
											echo '<span class="bibtex item"><a data-content="bibtex">Bibtex</a></span>';
										}
										if($code){
											echo '<span><a href="'.$code.'" target="_blank" class="code item">Code</a></span>';
										}
										if($blog){
											echo '<span><a href="'.$blog.'" target="_blank" class="blog item">Blog</a></span>';
										}
									?>
								</div>

								<div class="content-option is-active" id="abstract">
									<?php if($abstract){
											echo '<h3 class="title-item">Abstract</h3>';
										} ?>
									<?php echo $abstract; ?>
								</div>
								<div class="content-option" id="bibtex">
									<?php if($bibtex){
											echo '<h3 class="title-item">Bibtex</h3>';
										} ?>
									<?php echo $bibtex; ?>
								</div>
								
								<a href="<?php bloginfo('url');_e('/publications', 'corex'); ?>" class="btn btn-view-detail btn-view-detail-home">
									<img src="<?php echo THEME_ASSETS . '/images/icon-left.svg' ?>" alt="">
									<span>Back to Research</span>
								</a>
							</div>

                            <!-- Mobile -->
                            <div class="__left w-100" id="mobile">
								<div class="__left_item __left_item_info w-100">
									<ul>
										<li class="__item_time d-flex "><img src="<?php echo THEME_ASSETS . '/images/oclock.svg' ?>" /> <?php the_time( 'i' ); ?> minutes</li>
										<li class="__item_user d-flex"><img src="<?php echo THEME_ASSETS . '/images/user-icon.svg' ?>" /> <p><?php echo $authors; ?></p></li>
										<li class="__item_calender d-flex"><img src="<?php echo THEME_ASSETS . '/images/calender.svg' ?>" /> <?php echo $conference_name; ?></li>
									</ul>
								</div>
								
								<div class="__left_item __left_item_download w-100">
									<ul>
										<li class="__item_time d-flex ">
											<a class="btn-view-detail" href="<?php echo $pdf; ?>" download>Download <img src="<?php echo THEME_ASSETS . '/images/arrow-down.svg' ?>" /></a>
										</li>
									</ul>
								</div>
								
								<div class="__left_item __left_item_share w-100">
									<span>Share</span>
									<ul class="__list-social">
										<li class="__item_time">
											<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="btn_face popup_windown" rel="nofollow noopener"> 
												<i class="fab fa-facebook-f"></i>
											</a>
										</li>
										<li class="__item_time">
											<a href="https://twitter.com/share?url=<?php the_permalink(); ?>" class="btn_skype popup_windown" rel="nofollow noopener"> <i class="fab fa-twitter"></i></a>
										</li>
										<li class="__item_time">
											<a href="mailto:<?php the_permalink(); ?>"> <i class="fas fa-envelope-open"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="section-3">
		<div class="tw-container">
        <div class="tw-content">
            <div class="_right">
                <h2><?php _e('Related publications', 'corex'); ?></h2>
                <div class="_related_list list-publications-global">
					<div class="swiper swiper-container swiper-pub">
					  <div class="swiper-wrapper">
                    <?php
                        $postCat = new WP_Query([
                            'post_type'       => 'publications',
                            'posts_per_page'  => 3,
                            'post__not_in'    => array($postId),
                            'post_status'     => 'publish'
                        ]);
                    ?>
                    <?php
                        if($postCat->have_posts(  )){
                            while ($postCat->have_posts(  )) {
                                $postCat->the_post(  );
                                $_postId = get_the_ID(  );
								$_category = get_the_terms( $_postId, 'research-area' );
                                $_thumbnail = get_the_post_thumbnail_url( $_postId, 'full' ) ? get_the_post_thumbnail_url( $_postId, 'full' ) : NOT_IMAGE;
                                $_title = get_the_title( $_postId );
								$_author = get_field('authors', $_postId);
                                $conference_name = get_field('conference_name', $_postId);
                                $_permalink = get_the_permalink($_postId);
								$pdf = get_field('pdf', $_postId);
								$is_hover_download = $pdf ? 'class="is_hover"' : null;

                                ?>
									<div class="swiper-slide">
									<div class="__item">
										<div class="--description">
											<div class="--title_doc">
												<span class="sub-heading">
													<?php 
														foreach ( $_category as $cat){
														   echo $cat->name;
														}
													?>
												</span>
												<a href="<?php echo $_permalink; ?>">
													<h4>
														<?php echo $_title; ?>
													</h4>
												</a>
											</div>
											<div class="--member">
												<img src="<?php echo THEME_ASSETS . '/images/user-icon.svg' ?>" />
												<?php 
													if (strlen($_author) > 60) {
														echo substr($_author, 0, 60) . '<span style="color: #000">...et al.</span>';
													} else {
														echo $_author;
													}  ?> 
													<span style="color: #000">(<?php echo $conference_name; ?>)</span>
											</div>
										</div>
										<div class="--control d-flex justify-content-between align-items-center">
											<a href="<?php echo $_permalink; ?>" class="btn btn-view-detail d-flex align-items-center">
												<span><?php _e('View detail', 'corex') ?></span>
												<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>">
											</a>
											<a class="--control_download" href="<?php echo $pdf; ?>" <?php echo $is_hover_download; ?> target="_blank"><i class="fas fa-arrow-down"></i></a>
										</div>
									</div>
									</div>
								<?php
                            }
                        }
                    ?>
					</div>
					<div class="swiper-pagination-pub text-align-center" id="mobile"></div>
					</div>
                </div>

                    
            </div>
        </div>
    </div>
</div>
<?php endwhile; wp_reset_postdata(); endif; ?>
<script>
    jQuery(document).ready(function($){
		var swiper_pub = new Swiper(".swiper-pub", {
			slidesPerView: 3,
			spaceBetween: 30,
			loop: true,
			pagination: {
				el: ".swiper-pagination-pub",
				clickable: true,
			},
			breakpoints: {
            1024: {
                slidesPerView: 3,
				spaceBetween: 10,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
      });
	  
        $('[data-content]').on('click', function(){
            const dataContent = $(this).attr('data-content');
            const dataContentEl = $('#'+dataContent+'');
            $('.content-option').removeClass('is-active');
            dataContentEl.addClass('is-active');
        });
		
		// Tab publication_detail
		$(".publication_detail .option span").click(function () {
			$(".publication_detail .option span").removeClass("active");
			$(this).addClass("active");   
		});
		
		// Popup window 
		$('.popup_windown').click(function (event) {
			event.preventDefault();
			window.open($(this).attr("href"), "popupWindow", "width=600,height=600,scrollbars=yes");
		});
		
    })
</script>
<?php get_footer();