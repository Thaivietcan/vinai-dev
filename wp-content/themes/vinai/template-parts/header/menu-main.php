<?php 
    $logo_black = get_field('op_logo_black', 'option');
    $logo_white = get_field('op_logo_white', 'option');
?>
<div class="header-main">
    <div class="header-content">
        <div class="tw-container">
            <div class="tw-content">
                <div class="_logo">
                    <a href="<?php bloginfo('url'); ?>">
                        <img src="<?php echo $logo_black; ?>" alt="" class="logo-black">
                        <img src="<?php echo $logo_white; ?>" alt="" class="logo-white">
                        <?php //include THEME_DIR . '/assets/images/common/logo.svg' ?>
                    </a>
                </div>
                <?php wp_nav_menu( 
                    array( 
                        'theme_location' => 'main-menu', 
                        'container' => 'false', 
                        'menu_id' => 'menu-main', 
                        'menu_class' => 'menu-main'
                    ) 
                ); ?>
            </div>
        </div>
    </div>
</div>

<div class="navigation_m">
    <a href="<?php bloginfo('url'); ?>" class="logo_m">
        <img src="<?php echo $logo_black; ?>" alt="" class="logo-black">
        <img src="<?php echo $logo_white; ?>" alt="" class="logo-white">
    </a>
    <div class="pull" id="pull">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>

<div class="coating-site"></div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    if ($(window).width() >= 768) {
        $('#menu-main > li').hover(function(e) {
            $(this).children('ul').slideDown(200);
        }, function() {
            $(this).children('ul').slideUp(200);
        });
    } else {
         $("#menu-main #icon-menu").click(function(e){
			e.preventDefault();
			 var target = $(this).parent().parent().children(".sub-menu");
			$(target).toggleClass('add_show');
		});
    }

    $('#pull, .coating-site').on('click', function() {
        $('#pull').toggleClass('is--change');
        $('.header-main, .coating-site, .navigation_m').toggleClass('is--active'); 


    })
});
</script>