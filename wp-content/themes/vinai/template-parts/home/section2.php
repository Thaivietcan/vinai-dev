<?php
	$h_show_about_us = get_field('h_show_about_us', 'option');
	$home_about_title = get_field('home_about_title', 'option');
	$home_about_sub_title = get_field('home_about_sub_title', 'option');
	$home_about_description = get_field('home_about_description', 'option');
	$home_about_image = get_field('home_about_image', 'option');
?>
<?php if(!empty($h_show_about_us)) : ?>
<section class="h-section2" style="background-image: url(<?php echo $home_about_image; ?>);">
	<div class="tw-container">
		<div class="tw-content d-flex justify-content-between flex-direction-colum">
			<div class="left w-100" data-aos="fade-up">
				<h3 class="tw-ttitle-text"><?php echo $home_about_title; ?></h3>
				<p><?php echo $home_about_sub_title; ?></p>
			</div>
			<div class="right w-100" data-aos="fade-up">
				<p><?php echo $home_about_description; ?></p>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) { 
		
	});
</script>