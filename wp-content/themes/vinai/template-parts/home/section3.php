<?php
    $home_talents_title = get_field('home_talents_title', 'option');
    $home_talents_description = get_field('home_talents_description', 'option');
	$home_research_image = get_field('home_research_image', 'option');
    $home_research_buttons = get_field('home_research_buttons', 'option');

    $home_ai_product_title = get_field('home_ai_product_title', 'option');
    $home_ai_products = get_field('home_ai_products', 'option');
    $home_ai_products_button = get_field('home_ai_products_button', 'option');
?>
<section class="h-section3">
	<div class="tw-content image-member d-flex align-items-center flex-direction-colum">
		<div class="content_temp w-100">
			<div class="tw-container">
				<div class="tw-content">
					<h2 class="tw-title" data-aos="fade-up"><?php echo $home_talents_title; ?></h2>
					<p data-aos="fade-up"><?php echo $home_talents_description; ?></p>
					<div class="btn btn-buttons" data-aos="fade-up">
						<a href="<?php echo $home_research_buttons['home_button_link_publications']; ?>" class="tw-button-common btn-dark"><span><?php echo $home_research_buttons['home_about_text_btn_publications']; ?></span></a>
						<a href="<?php echo $home_research_buttons['home_about_link_blog']; ?>" class="tw-button-common btn-light"><span><?php echo $home_research_buttons['home_about_text_btn_blog']; ?></span></a>
					</div> 
				</div>
				<div class="background-opacity"></div>
			</div>
		</div>
		<div class="image_temp w-100 pc" data-aos="fade-up" style="background-image:url(<?php echo $home_research_image['url'] ?>)"></div>
		
	</div>
    <!-- Ai Product -->
    <div class="tw-container">
        <div class="tw-content">
            <div class="ai-products">
                <div class="content_temp_product">
                    <h2 class="tw-title" data-aos="fade-up"><?php echo $home_ai_product_title; ?></h2>
                </div>
                <!-- Swiper -->
                <div class="swiper swiper_home_product" data-aos="fade-up">
                    <div class="swiper-wrapper">
                        <?php if( have_rows('home_ai_products', 'option') ): ?>
                            <?php $i = 1; ?>
                                <?php while( have_rows('home_ai_products', 'option') ): the_row(); 
                                    $image = get_sub_field('home_ai_products_image');
                                    $sub_title = get_sub_field('home_ai_products_sub_title');
                                    $title = get_sub_field('home_ai_products_title');
                                    $description = get_sub_field('home_ai_products_description');
                                    $button = get_sub_field('home_ai_products_button');
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="swiper-slide-inner d-flex flex-direction-colum">
                                            <div class="swiper_home_product_image">
                                                <img src="<?php echo $image ?>" alt="">
                                            </div>
                                            <div class="swiper_home_product_content">
                                                <h1>0<?php echo $i; ?></h1>
                                                <span class="sub-heading"><?php echo $sub_title ?></span>
                                                <h3><?php echo $title; ?></h3>
                                                <p><?php echo $description; ?></p>
                                                <a href="<?php echo $button['link_button'] ?>" class="btn btn-view-detail btn-view-detail-home">
                                                    <span><?php echo $button['text_button'] ?></span>
                                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon-right.svg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
jQuery(document).ready(function($) {
    var swiper_home_product = new Swiper('.swiper_home_product', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 15,
        speed: 1000,
        autoplay: false,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        1024: {
            slidesPerView: 1,
            
        },
        768: {
            slidesPerView: 1,
        },
        480: {
            slidesPerView: 1,
            
        }
    });
});
</script>