<?php
    $title = get_field('home_logo_partners_title', 'option');
    $image = get_field('home_bg_partner', 'option');
    $logo_partners = get_field('home_logo_partners', 'option');
?>
<section class="h-section6" id="h-section6" style="background-image: url(<?php echo $image; ?>)">
    <div class="tw-container">
        <div class="tw-content">
            <div class="">
                <h2 class="tw-title aos-init aos-animate" data-aos="fade-up"><?php echo $title; ?></h2> 
                <div class="list-logo-partners d-flex align-items-center justify-content-center pc aos-init aos-animate" data-aos="fade-up">
                    <?php 
                        if(!empty($logo_partners)) {
                            foreach($logo_partners as $logos) {
                                ?>
                                    <div class="_img">
                                        <img src="<?php echo $logos; ?>" alt=""> 
                                    </div>
                                <?php
                            }
                        }
                    ?>
                </div>
				<!--Mobile-->
				<div class="list-logo-partners mb">
					<div class="swiper swiper-container swiper-logo-partner">
					  <div class="swiper-wrapper grid-container">
						<?php 
							if(!empty($logo_partners)) {
								foreach($logo_partners as $logos) {
									?>
										<div class="swiper-slide">
											<div class="_img">
												<img src="<?php echo $logos; ?>" alt=""> 
											</div>
										</div>
										
									<?php
								}
							}
						?>
					  </div>
					  <div class="swiper-pagination-logo"></div>
					</div>
				</div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
jQuery(document).ready(function($) {
    var swiper_logo = new Swiper(".swiper-logo-partner", {
        slidesPerView: 2,
        slidesPerColumn: 5,
		spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination-logo",
          clickable: true,
        },
    });
});
</script>