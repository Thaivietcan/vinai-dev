<?php
    $title = get_field('home_ex_talents_title', 'option');
    $description = get_field('home_ex_talents_description', 'option');
    $image = get_field('home_ex_talents_image', 'option');
	$button = get_field('home_ex_button', 'option');
?>
<section class="h-section4" id="h-section4">
    <div class="tw-content d-flex align-items-center justify-content-between flex-direction-colum">
        <div class="_left aos-init aos-animate" data-aos="fade-up">
            <div class="_img" style="background-image:url('<?php echo $image; ?>');">
                <img src="<?php echo $image; ?>" alt="">
            </div>
        </div>
        <div class="_right w-100 aos-init aos-animate" data-aos="fade-up">
            <div class="tw-container">
				<div class="tw-content">
					<h2 class="tw-title"><?php echo $title; ?></h2>
				<p><?php echo $description; ?></p>
				<?php if(!empty($button)) : ?>
				<a href="<?php echo $button['link']; ?>" class="tw-button-common btn-dark"><span><?php echo $button['text']; ?></span></a>
				<?php endif; ?>
				</div>
			</div>
        </div>
    </div>
</section>
<script type="text/javascript">
jQuery(document).ready(function($) {
    
});
</script>