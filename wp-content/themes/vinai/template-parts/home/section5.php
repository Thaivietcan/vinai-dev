<?php 
	$home_audience = get_field('home_audience', 'option');
	$home_benefits = get_field('home_benefits', 'option');
	$home_requi_rements = get_field('home_requi_rements', 'option');
?>
<section class="h-section5 news">
	<div class="tw-container">
		<div class="tw-content">
            <h2 class="tw-title" data-aos="fade-up"><?php _e('AI Residency Program', 'corex'); ?></h2>
            <div class="list_post">
                <!-- Swiper -->
                <div class="swiper list-post-residency" data-aos="fade-up">
                    <div class="swiper-wrapper">
                        <?php
                            $post_content = get_field('post_content_achievements_air', 'option');
                            if($post_content && count($post_content) > 0){
                                foreach ($post_content as $post) {
                                    $post_ID = $post['post']->ID;
                                    $category = get_the_category($post_ID);
                                    $permalink = get_the_permalink($post_ID);
                                    $thumbnail = get_the_post_thumbnail_url($post_ID, "full") ?: NOT_IMAGE;
                                    $title = get_the_title($post_ID);
                                    $theExcerpt = get_the_excerpt($post_ID);
                                    $day = get_the_time('d/m/Y', $post_ID);
                                    ?>
                                        <div class="swiper-slide">
                                        <div class="__item">
                                            <a href="<?php echo $permalink ?>" class="thumbnail">
                                                <div class="--bg"
                                                    style="background-image: url(<?php echo $thumbnail ?>)">
													<img src="<?php echo $thumbnail ?>" class="thumbnail"/>
                                                </div>
                                            </a>
                                            <div class="text">
                                                <span class="sub-heading"><?php echo $category[0]->name; ?></span>
                                                <a href="<?php echo $permalink ?>" class="--title">
                                                    <h4><?php if (strlen($title) > 40) {
                                                            echo substr($title, 0, 40) . '...';
                                                        } else {
                                                            echo $title;
                                                        } ?></h4>
                                                </a>
                                                <p>
                                                    <?php 
                                                        if (strlen($theExcerpt) > 100) {
                                                            echo substr($theExcerpt, 0, 100) . '(...)';
                                                        } else {
                                                            echo $theExcerpt;
                                                        }
                                                    ?>
                                                </p>
                                                
                                                <div class="--meta d-flex align-items-center justify-content-between">
                                                    <a href="<?php echo $permalink ?>" class="btn btn-view-detail btn-view-detail-home">
                                                        <span><?php _e('View details', 'corex'); ?></span>
                                                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon-right.svg" alt="">
                                                    </a>
                                                    <p class="--time"><?php echo $day; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                    <div class="swiper-pagination-home-ai"></div>
                </div>
            </div>
		</div>
	</div>
	<div class="shape-one"></div>
	<div class="shape-two"></div>
</section>
<script type="text/javascript">
	jQuery(document).ready(function($) { 
        var swiper = new Swiper(".list-post-residency", {
        speed: 800,
        slidesPerView: 3,
        spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination-home-ai",
          clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 15
            }
        }
      });
	});
</script>