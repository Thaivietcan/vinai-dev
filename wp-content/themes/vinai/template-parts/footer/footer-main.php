<?php
    $facebook_link = get_field('facebook_link', 'option');
    $youtube_link = get_field('youtube_link', 'option');
    $twitter_link = get_field('twitter_link', 'option');
    $linkedin_link = get_field('linkedin_link', 'option');
    $email_ft = get_field('email_ft', 'option');
	
	$show_contact = get_field('of_show_contact' , 'option');
	$title_contact = get_field('of_title_contact' , 'option');
	$sub_contact = get_field('of_sub_contact' , 'option');
	$text_button = get_field('of_text_button' , 'option');
	$form_contact = get_field('of_form_contact' , 'option');
?>
<?php if(!empty($show_contact)) : ?>
<div class="footer-top">
    <div class="tw-container">
        <h2 class="tw-title"><?php echo $title_contact; ?></h2>
		<p><?php echo $sub_contact; ?></p>
        <a data-fancybox href="#popup-form" class="tw-button-common btn-dark">
            <span><?php echo $text_button; ?></span>
        </a>
		<div id="popup-form" style="display:none">
			<?php echo do_shortcode($form_contact); ?>
		</div>
    </div>
</div>
<?php endif; ?>
<div class="footer-main">
    <div class="footer_menu">
        <div class="tw-container">
            <div class="tw-content">
                <div class="_menu_ft d-flex">
                    <div class="__item">
                        <h4><?php _e('About', 'corex'); ?></h4>
                        <ul class="__list">
                            <li><a href="/our-story/"><?php _e('Our story', 'corex'); ?></a></li>
                            <li><a href="/our-team/"><?php _e('Our team', 'corex'); ?></a></li>
                            <li><a href="/careers/"><?php _e('Careers', 'corex'); ?></a></li>
                            <li><a href="/seminar-series/"><?php _e('Seminar & Workshop', 'corex'); ?></a></li>
                            <li><a href="/news/"><?php _e('News', 'corex'); ?></a></li>
                        </ul>
                    </div>
                    <div class="__item">
                        <h4><?php _e('Our Products', 'corex'); ?></h4>
                        <ul class="__list text-product-footer">
							<?php
								$menus = get_field('op_menus', 'option');
								
								foreach($menus as $menu) {
									?>
										<li>
											<span><?php echo $menu['menu_label']; ?></span>
											<?php
												foreach($menu['menu_text'] as $item) {
													echo '<a href="'.$item['link'].'">'.$item['text'].'</a>';
												}
											?>
										</li>
									<?php
								}
							?>
                        </ul>
                    </div>
                    <div class="__item">
                        <h4><?php _e('VinAI', 'corex'); ?></h4>
                        <ul class="__list">
                            <li class="text-mail-footer">
                                <?php foreach($email_ft as $email) {
                                    echo '<a href="mailto:'.$email['email_address'].'">'.$email['email_address'].'</a>';
                                } ?>
                                
                            </li>
                        </ul>
                        <ul class="__list __list-social d-flex">
                            <li>
                                <a href="<?php echo $facebook_link ? $facebook_link : 'javascript:void(0);'; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li>
                                <a href="<?php echo $twitter_link ? $twitter_link : 'javascript:void(0);'; ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="<?php echo $youtube_link ? $youtube_link : 'javascript:void(0);'; ?>" target="_blank"><i class="fab fa-youtube"></i></a>
                            </li>
                            <li>
                                <a href="<?php echo $linkedin_link ? $linkedin_link : 'javascript:void(0);'; ?>" target="_blank"><i class="fab fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer_contact">
        <div class="tw-container">
            <div class="tw-content">
                <div class="_item _item-copyright d-flex align-items-center justify-content-between flex-direction-colum">
                    <ul class="__list __list-copyright d-flex">
                        <li><a data-fancybox href="#popup-form"><?php _e('Contact', 'corex'); ?></a></li>
                        <li><a href="/#/"><?php _e('Privacy', 'corex'); ?></a></li>
                        <li><a href="/#/"><?php _e('Terms of use', 'corex'); ?></a></li>
                    </ul>
                    <p>© 2019-2021 VINAI Artificial Intelligence Application and Research JSC</p>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    //js AOS:
    AOS.init({
        duration: 700,
        easing: 'linear',
        disable: 'mobile',
        delay: 300,
    });
});
</script>