<?php
get_header();
if(have_posts(  )) : while(have_posts(  )) : the_post(  );
	
	$detail_carrers_title = get_field('detail_carrers_title');

    $_careerID = get_the_ID();
    $_locations  = get_the_terms( $_careerID, "location-category" );
    $_teams  = get_the_terms( $_careerID, "teams-category" );
    $_featured_image = get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : NOT_IMAGE;
    $_the_content = get_the_content();
	
	$taxonomy_team = 'teams-category';
    $taxonomy_location = 'location-category';
    $taxonomy_jobs = 'jobs-category';
    $terms_teams_category = get_terms([
        'taxonomy'   => $taxonomy_team,
        'hide_empty' => false,
        'orderby'       => 'id', 
        'order'         => 'ASC',
    ]);
    $terms_location_category = get_terms([
        'taxonomy'   => $taxonomy_location,
        'hide_empty' => false,
        'orderby'       => 'id', 
        'order'         => 'ASC',
    ]);
    $terms_jobs_category = get_terms([
        'taxonomy'   => $taxonomy_jobs,
        'hide_empty' => false,
        'orderby'       => 'id', 
        'order'         => 'DESC',
    ]);
?>
<div class="careers_detail">
	<div class="section-1">
		<div class="tw-container">
			<div class="tw-content">
				<p class="location">
					<i class="fas fa-map-marker-alt"></i>
					<?php
						if($_locations){
							$i = 0;
							foreach ($_locations as $location) {
								$i++;
								$termID = $location->term_id;
								$shortened_name = get_field('location_shortened_name', 'term_'.$termID);
								if( $i == count($_locations)){
									echo $shortened_name;
								}else{
									echo $shortened_name . ', ';
								}
							}
						}
					?>
				</p>
				<h2 class="tw-title"><?php the_title(); ?></h2>
			</div>
		</div>
	</div>
    <div class="section-2">
		<div class="tw-container">
			<div class="tw-content">
				<div class="careers_detail_contents">
					<div class="the_content">
						<?php
							if($detail_carrers_title){
							foreach($detail_carrers_title as $details) {
								echo '
									<div class="careers_detail_inner d-flex justify-content-between flex-direction-colum">
										<div class="careers_detail_title">
											<h4 class="sub-heading">'.$details['detail_carrer_title'].'</h4>
										</div>
										<div class="careers_detail_desc w-100">'.$details['detail_carrer_description'].'</div>
									</div>
								';
							}
							}else{
								echo '<p>'.__('Updating', 'corex').'...</p>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="section-3">
		<div class="tw-container">
			<div class="tw-content">
				<h2 class="tw-title">
					Interested applicants are invited to<br> send your application to <br> <a href="mailto:job@vinai.io ">job@vinai.io </a>
				</h2>
			</div>
		</div>	
		
		<div class="shape-two"></div>
		<div class="shape-one"></div>
	</div>
	
	<div class="section_04 careers_main" id="<?php _e('internship', 'corex'); ?>">
        <div class="tw-container">
            <div class="tw-content d-flex justify-content-between flex-direction-colum">
				
                <div class="_tabs" id="js_tabs_type_job" style="display: none;"> 
                    <?php
                        if(!empty($terms_jobs_category)){
                            $i = 0;
                            foreach ($terms_jobs_category as $key => $job) {
                                $i++;
                                $_classActive = $i == 1 ? 'is--active' : null;
                                echo '<a href="'.$job->slug.'" class="'.$_classActive.'">'.$job->name.'</a>';
                            };
                            echo '<input type="hidden" name="job-type" id="job-type" value="'.$terms_jobs_category[0]->slug.'"/>';
                        }
                    ?>
                </div> 
				
				<div class="_left">
					<p class="title-explore-jobs"><?php _e('Explore more jobs <br> at vinai', 'corex'); ?></p>
					<div class="form_search">
						<form action="#" id="frmOutTeam">
							<div class="all-feild">
								<div class="field-item --select">
									<label for="team"><?php _e('Team', 'corex') ?></label>
									<div class="_input">
										<select name="team-careers" id="team-careers">
											<option value=""><?php _e('All teams', 'corex') ?></option>
										<?php
											if(!empty($terms_teams_category)){
												foreach ($terms_teams_category as $teams) {
													$_termID = $teams->term_id;
													$_name = $teams->name;
													$_slug = $teams->slug;
													echo '<option value="'.$_slug.'">'.$_name.'</option>';
												}
											}
										?>
										</select>
									</div>
								</div>
								<div class="field-item --select">
									<label for="team"><?php _e('Location', 'corex'); ?></label>
									<div class="_input location">
										<select name="location-careers" id="location-careers">
											<option value=""><?php _e('All offices', 'corex') ?></option>
											<?php
												if(!empty($terms_location_category)){
													foreach ($terms_location_category as $location) {
														$_termID = $location->term_id;
														$_name = $location->name;
														$_slug = $location->slug;
														echo '<option value="'.$_slug.'">'.$_name.'</option>';
													}
												}
											?>
										</select>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="_right w-100">
					<?php
						$_careers = new WP_Query([
							'post_type'      => 'careers',
							'posts_per_page' => 5, //5
							'jobs-category'  => $terms_jobs_category[0]->slug,
							'post_status'    => 'publish',
						]);
					?>
					<div class="preloader_member">
						<div class="loading_animation">
							<?php include_once THEME_DIR . '/assets/images/preloader.svg' ?>
						</div>
						<div class="_jobs" id="position_hiring">
							<ul>
								<?php
									if($_careers->have_posts(  )){
										while ($_careers->have_posts(  )) {
											$_careers->the_post(  );
											$_careerID = get_the_ID(  );
											$_title = get_the_title($_careerID);
											$_link = get_the_permalink($_careerID);
											$_locations  = get_the_terms( $_careerID, "location-category" );
										?>
											<li>
												<a href="<?php echo $_link; ?>" class="--name"><?php echo $_title; ?></a>
												<p class="--location">
													<i class="fas fa-map-marker-alt"></i>
													<?php
														if($_locations){
															$i = 0;
															foreach ($_locations as $location) {
																$i++;
																$termID = $location->term_id;
																$shortened_name = get_field('location_shortened_name', 'term_'.$termID);
																if( $i == count($_locations)){
																	echo $shortened_name;
																}else{
																	echo $shortened_name . ', ';
																}
															}
														}
													?>
												</p>
											</li>
										<?php
										}
									}
								?>
							</ul>
							<div class="pagination-common js_panigator">
								<?php echo wp_navigation_paged_ajax($_careers); ?>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; endif; ?>
<form>
    <?php wp_nonce_field( 'ajax_loading_jobs_careers', 'jobs_careers_nonce' ); ?>
</form>
<script>
   jQuery(document).ready(function($) {

		const teamCareersInput = $('#team-careers');
		const locationCareersInput = $('#location-careers');
		const careersNonce = $('#jobs_careers_nonce');
		const positionHiring = $('#position_hiring');
		const jobTypeInput = $('#job-type');
		var teamSlug = getParameterByName('team');
		if(teamSlug){
			teamCareersInput.val(teamSlug);
			loadingAjaxJobs();
			setTimeout(() => {
				$('html, body').animate({
					scrollTop: $("#js_tabs_type_job").offset().top
				}, 2000)
			}, 1000);
		}

		$('.js-preview-jobs').on('click', function(e){
			e.preventDefault();
			const slug = $(this).parent('.js-slug').attr('term-slug');
			teamCareersInput.val(slug);
			loadingAjaxJobs();
			$('html, body').animate({
				scrollTop: $("#js_tabs_type_job").offset().top
			}, 1000)
		});

		$('#js_tabs_type_job').on('click', 'a', function(e){
			e.preventDefault();
			const href = $(this).attr('href');
			$(this).addClass('is--active');
			$(this).siblings('a').removeClass('is--active');
			jobTypeInput.val(href);
			jobTypeInput.change();
		});
		jobTypeInput.on('change', function(e){
			const typeValue = $(this).val();
			const nonceValue = careersNonce.val();
			teamCareersInput.val("");
			locationCareersInput.val("");
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: obj.AJAX_URL,
				data: {
					action: "ajax_jobs_type",
					jobType: typeValue,
					nonce: nonceValue,
				},
				beforeSend: () => {
					$('.loading_animation').addClass('is--active');
				},
				complete: () => {
					setTimeout(() => {
						$('.loading_animation').removeClass('is--active');
					}, 800);
				},
				success: (response) => {
					setTimeout(() => {
						positionHiring.empty();
						positionHiring.append($(response.data));
					}, 600);
				},
				error: (jqXHR, textStatus, errorThrown) => {
					console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
				}
			});
		})

		positionHiring.on('click', '.js_panigator .paginate_links a', function(e){
			e.preventDefault();
			var hrefThis = $(this).attr('href');
			var page = hrefThis.match(/\/\d+\//)[0];
			page = page.match(/\d+/)[0];
			if(!page) page = 1;
			loadingAjaxJobs(page);
		});

		teamCareersInput.on('change', function(e){
			e.preventDefault();
			loadingAjaxJobs();
		});

		locationCareersInput.on('change', function(e){
			e.preventDefault();
			loadingAjaxJobs();
		});

		function loadingAjaxJobs(paged = 1){
			const typeValue = jobTypeInput.val();
			const teamValue = teamCareersInput.val();
			const loactionValue = locationCareersInput.val();
			const nonceValue = careersNonce.val();
			const page = paged;
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: obj.AJAX_URL,
				data: {
					action: "ajax_navigation_load_jobs",
					type: typeValue,
					team: teamValue,
					location: loactionValue,
					paged: page,
					nonce: nonceValue,
				},
				beforeSend: () => {
					$('.loading_animation').addClass('is--active');
				},
				complete: () => {
					setTimeout(() => {
						$('.loading_animation').removeClass('is--active');
					}, 800);
				},
				success: (response) => {
					setTimeout(() => {
						positionHiring.empty();
						positionHiring.append($(response.data));
					}, 600);
				},
				error: (jqXHR, textStatus, errorThrown) => {
					console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
				}
			});
		}
	})

</script>
<?php get_footer();