<?php
    get_header( );
    if(have_posts(  )) : 
        while (have_posts(  )): 
            the_post(  ); 
            $post_id = get_the_ID();
            $thumbnail = get_the_post_thumbnail_url($post_id, 'full') ?: null;
            $featured_video = get_field('featured_video_workshop', $post_id) ?: null;
            $title = get_the_title($post_id);
			
			$info_workshop = get_field('info_workshop', $post_id);
			$buttons = get_field('w_button_register', $post_id);
			
            $dateObject = new DateTime(get_field('time_takes_place', $post_id));
            $timeStamp = $dateObject->getTimestamp();
            $timing = get_field('time_takes_place', $post_id) ? date('l, M d Y - h:i a', $timeStamp) . ' (GMT + 7)' : null;
            
			$content = get_the_content($post_id);
            $program_details = get_field('program_details', $post_id);
            $about_workshop = get_field('about_workshop', $post_id);
            $more_information = get_field('more_information', $post_id);
?>
    <div class="news_detail  workshop-page-single">
        <div class="section-1">
            <div class="tw-container">
                <div class="tw-content">
                    <p class="location">
                        <span><i class="fas fa-clock"></i>
                        <?php echo $timing; ?></span>
                    </p>
                    <h2 class="tw-title"><?php echo $title; ?></h2>
                </div>
            </div>
        </div>
        
        <div class="thumbnail">
            <img src="<?php echo $thumbnail; ?>" alt="">
        </div>

        <div class="section-2">
            <div class="tw-container">
                <div class="tw-content">
                    <div class="about-workshop d-flex justify-content-between flex-direction-colum">
                        <div class="about-content w-100">
                            <?php if(!empty($about_workshop)) : ?>
                                <span class="sub-heading"><?php _e('About the workshop', 'corex'); ?></span>
                                <p><?php echo $about_workshop; ?></p>
                            <?php endif; ?>
							<?php if(!empty($buttons['text_button'])) : ?>
                            <a href="<?php echo $buttons['link_button'] ? $buttons['link_button'] : "#" ?>" target="_blank" class="tw-button-common btn-dark aos-init aos-animate">
                                <span><?php echo $buttons['text_button']; ?></span>
                            </a>
							<?php endif; ?>
                        </div>
                        <div class="about-content-time w-100">
                            <ul>
                                <li>
                                    <P>Time</P>
                                    <?php if(!empty($timing)) : ?>
                                        <span><?php echo $timing; ?></span>
                                    <?php endif; ?>
                                </li>
                                <li>
                                    <P>The language used</P>
                                    <?php if(!empty($info_workshop['language_use'])) : ?>
                                        <span><?php echo $info_workshop['language_use']; ?></span>
                                    <?php endif; ?>
                                </li>
                                <li>
									<?php if(!empty($info_workshop['online_participants'])) : ?>
                                    <P><?php echo $info_workshop['online_participants']['text'] ?></P>
                                    <a href="<?php echo $info_workshop['online_participants']['link'] ?>" target="_blank"><span class="fontWieght"><?php echo $info_workshop['online_participants']['link'] ?></span></a>
                                    <?php endif; ?>
                                </li>
                                <li>
                                    <?php if(!empty($info_workshop['livestream_location'])) : ?>
                                        <span class="fontWieght">Livestream from <?php echo $info_workshop['livestream_location']; ?></span>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
         
        <?php if(!empty($featured_video)) : ?>
            <div class="section-3">
                <div class="tw-container">
                    <div class="tw-content">
                        <div class="video-speaker">
                            <p class="feature_image feature_video">
                                <iframe src="<?php echo $featured_video; ?>?rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

		<div class="section-4">
			<div class="tw-container">
				<div class="tw-content">
					<div class="program-detail-list">
						<?php if(!empty($program_details)) : ?>
							<h3><?php _e('Program details are as follows:', 'corex'); ?></h3>
							<div class="detail-list">
								<div class="title-detail-list d-flex align-items-center">
									<p class="program-time">Time</p>
									<p class="program-topic">Topic</p>
									<p class="program-speaker">Speaker</p>
								</div>
								<?php 
									foreach($program_details as $programs) {
										?>
											<div class="content-detail-list d-flex">
												<p class="program-time"><?php echo $programs['times_workshop'] ?></p>
												<p class="program-topic"><?php echo $programs['topic_workshop'] ?></p>
												<p class="program-speaker"><?php echo $programs['speaker_author'] ?></p>
											</div>
										<?php
									}
								?>
							</div>
							<p>*: co-first authors</p>
						<?php endif; ?>
						<?php if(!empty($more_information)) : ?>
							<div class="more-infor">
								<?php echo $more_information; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
			
		<div class="section-5">
			<div class="tw-container">
				<div class="tw-content">
					<div class="upcoming_speakers">
						<h2 class="tw-title">Upcoming Workshops</h2>
						<div class="grid-speakers">
							<?php
							$upcoming_speaker_posts = fn_get_posts('workshops', 1, -1, [
								'post__not_in'    => [$post_id],
								'meta_query'      => array(
									array(
										'key'     => 'upcoming_workshops',
										'value'   => 'yes',
										'compare' => 'LIKE'
									)
								)
							]);
							if ($upcoming_speaker_posts->have_posts()) {
								while ($upcoming_speaker_posts->have_posts()) {
									$upcoming_speaker_posts->the_post();
									$post_id = get_the_ID();
									$thumbnail = get_the_post_thumbnail_url($post_id, 'full') ?: null;
									$wsp_post_title = get_the_title($post_id);
									$dateObject = new DateTime(get_field('time_takes_place', $post_id));
									$timeStamp = $dateObject->getTimestamp();
									$wsp_post_timing = get_field('time_takes_place', $post_id) ? date('l, M d Y - h:i a', $timeStamp).' (GMT + 7)' : null;
									$wsp_post_permalink = get_the_permalink($post_id);
									?>
										<div class="speaker-item">
											<div class="avatar">
												<a href="<?php echo $wsp_post_permalink; ?>"><img src="<?php echo $thumbnail; ?>" alt="<?php echo $wsp_post_title; ?>"></a>
											</div>
											<div class="info">
												<a href="<?php echo $wsp_post_permalink; ?>">
													<h3 class="name">
														<?php 
															if (strlen($wsp_post_title) > 55) {
																echo substr($wsp_post_title, 0, 55) . '...';
															} else {
																echo $wsp_post_title;
															} 
														?>
													</h3>
												</a>
												<p class="time_takes_place"><i class="fas fa-clock"></i> <?php echo $wsp_post_timing; ?></p>
											</div>
										</div>
									<?php
								}
								wp_reset_postdata();
								// echo '</div></div>';
							} else {
								echo '<p style="margin: 0">Stay tuned to get inspired, informed, and motivated by our leading experts & notable speakers.</p>';
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>    

    </div>

<?php endwhile; wp_reset_postdata(); endif; ?>
<?php get_footer(); ?>