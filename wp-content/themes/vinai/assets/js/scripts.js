(function ($) {
  $.fn.tabs = function () {
    var wrap = $(this);
    var head = wrap.find("[data-head]");
    var content = wrap.find("[data-content]");
    this.reset = function () {
      head.not(head.first()).removeClass("is-active");
      content.not(content.first()).hide();
    }.call(this);

    this.headClick = head.click(function (event) {
      event.preventDefault();

      if ($(this).hasClass("is-active")) {
        return false;
      }

      var content_target = $(this).attr("href");

      head.removeClass("is-active");
      content.hide();

      $(this).addClass("is-active");
      $(content_target).fadeIn();
    });

    return this;
  };

  // Scroll header animation
  var shrinkHeader = 100;
  $(window).scroll(function () {
    var scroll = getCurrentScroll();
    if (scroll >= shrinkHeader) {
      $("#header-main").addClass("shrink");
    } else {
      $("#header-main").removeClass("shrink");
    }
  });
  function getCurrentScroll() {
    return window.pageYOffset;
  }
  
  // Scroll tab product animation
  var shrinkTab = 485;
  $(window).scroll(function () {
    var scroll = getCurrentScrollTag();
    if (scroll >= shrinkTab) {
      $(".tab_products").addClass("fixed");
    } else {
      $(".tab_products").removeClass("fixed");
    }
  });
  function getCurrentScrollTag() {
    return window.pageYOffset;
  }
  
})(jQuery);

// Click scroll
jQuery(document).ready(function($){
	$('.tab_products a').bind('click', function(e) {
		e.preventDefault();
		var target = $(this).attr("href");
		$('html, body').stop().animate({
				scrollTop: $(target).offset().top - 70
		}, 1000);

		return false;
	});

})

// Scoll mouse
jQuery(document).ready(function($){
$(window).scroll(function() {
	var scrollDistance = $(window).scrollTop();
	$('.page-section').each(function(i) {
		if ($(this).position().top - 70 <= scrollDistance) {
			$('.tab_products .category_link a.is--active').removeClass('is--active');
			$('.tab_products .category_link a').eq(i).addClass('is--active');
		}
	});
}).scroll();
})
//get param url
function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return "";
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}
