<?php 
    get_header( );
    $_categories = get_categories(array(
        'hide_empty' => 0,
        'orderby'    => 'id',
        'order'      => 'ASC'
    ));
    $_currentCat = get_queried_object();
    $news_banner = get_field('news_banner', 'option') ? get_field('news_banner', 'option'): NOT_IMAGE;

    $flag = $_GET['event'] ? $_GET['event'] : 'upcoming';
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $timeNow = time();
    $eventID = [];
    $postsEventAll = new WP_Query([
        'post_type'  => 'post',
        'cat' => $_currentCat->term_id,
        'posts_per_page' => -1,
        'post_status' => 'publish',
    ]);
    if($postsEventAll->have_posts(  )){
        while ($postsEventAll->have_posts(  )) {
            $postsEventAll->the_post(  );
            $_date = get_field('event_date', $_postID);
            $_time = strtotime(date($_date));
            $postID = get_the_ID(  );
            switch ($flag) {
                case 'upcoming':
                    if($timeNow > $_time){
                        array_push($eventID, $postID);
                    }
                    break;
                case 'previous':
                    if($timeNow < $_time){
                        array_push($eventID, $postID);
                    }
                    break;
                default:
                    if($timeNow > $_time){
                        array_push($eventID, $postID);
                    }
                    break;
            }
        }
    }

    $postsEvent = new WP_Query([
        'post_type'  => 'post',
        'cat' => $_currentCat->term_id,
        'posts_per_page' => 12,
        'post__not_in' => $eventID,
        'post_status' => 'publish',
        'paged' => $paged
    ]);
?>
<div class="news">
    <div class="banner_page section_01"
        style="background-image: url('<?php echo $news_banner; ?>')">
        <div class="tw-container">
            <div class="tw-content">
                <h1><?php _e('news', 'corex') ?></h1>
            </div>
        </div>
    </div>
    <div class="section_02">
        <div class="tw-container">
            <div class="tw-content">
                <?php if(!empty($_categories)){
                    echo '<div class="category_link">';
                    foreach ((array) $_categories as $_category) {
                        $_class = $_currentCat->term_id == $_category->term_id ? 'is--active' : '';
                        if($_category->slug !== 'blog' && $_category->slug !== 'customer-cases' && $_category->slug !== 'products-solutions'){
                            echo '<a href="'.get_category_link($_category->term_id).'" class="'.$_class.'">'.$_category->name.'</a>';
                        }
                    }
                    echo '</div>';
                } ?>

                <div class="_button_filter">
                    <a href="<?php echo get_category_link($_currentCat->term_id) . '?event=upcoming' ?>" class="<?php echo $flag=="upcoming" ? 'is--active' : null; ?>">Upcoming Events</a>
                    <a href="<?php echo get_category_link($_currentCat->term_id) . '?event=previous' ?>" class="<?php echo $flag=="previous" ? 'is--active' : null; ?>">Previous Events</a>
                </div>
                <div class="the_content">
                    <div class="_list_news">
                        <?php
                        if($postsEvent->have_posts(  )): while ($postsEvent->have_posts(  )): $postsEvent->the_post(  );
                            $_postID = get_the_ID(  );
                            $_featuredImage = get_the_post_thumbnail_url($_postID) ?  get_the_post_thumbnail_url($_postID) : NOT_IMAGE;
                            $_title = get_the_title($_postID);
                            $_permalink = get_the_permalink($_postID);
                            $_location = get_field('event_address', $_postID);
                            $_date = get_field('event_date', $_postID);
                            $_date = date('d/m/Y', strtotime($_date));
                            $_author = get_field('event_talker', $_postID);
                        ?>
                            <div class="__item event_item">
                                <a href="<?php echo $_permalink; ?>" class="thumbnail">
                                    <div class="--bg"
                                        style="background-image: url('<?php echo $_featuredImage; ?>')">
                                    </div>
                                </a>
                                <div class="text">
                                    <a href="<?php echo $_permalink; ?>" class="--title">
                                        <h3><?php echo $_title; ?></h3>
                                    </a>
                                    <p class="--time --location"><span><img src="<?php echo THEME_ASSETS .'/images/common/location.png'; ?>" alt="location"></span><span> <?php echo $_location; ?></span></p>
                                    <p class="--time"><span><img src="<?php echo THEME_ASSETS .'/images/common/calendar.png'; ?>" alt="time"></span><span> <?php echo $_date; ?></span></p>
                                    <p class="--time"><span><img src="<?php echo THEME_ASSETS .'/images/common/people.png'; ?>" alt="location"></span><span> <?php echo $_author; ?></span></p>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata(  ); endif; ?>
                    </div>
                    <div class="pagination-common">
                        <?php wp_navigation_paged($postsEvent, $paged); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer( ); ?>