<?php
// <i class="fa fa-caret-down" aria-hidden="true"></i>
define('THEME_DIR', get_template_directory());
define('THEME_URI', get_template_directory_uri());
define('THEME_ASSETS', get_template_directory_uri() . '/assets');
define('ADMIN_AJAX_URL', admin_url('admin-ajax.php'));
define('HOME_URL', home_url('/'));
define('NOT_IMAGE', get_template_directory_uri() . '/assets/images/not-image.jpg');
define('NOT_AVATAR', get_template_directory_uri() . '/assets/images/not-avatar.jpg');
define('NOT_LEAD', get_template_directory_uri() . '/assets/images/leadership.png');

//admin
require_once('inc/Admin.php');
//common
require_once('inc/Common.php');
//post taxonomy
require_once('inc/Posts.php');
//delete 'category' url 
require_once('inc/Delete-category-url.php');
//navigation custom
require_once('inc/Navigation-custom.php');
//options page
require_once('inc/Options.php');
//menu
require_once('inc/Menu.php');
//post type
require_once('inc/Post-type.php');
//careers
require_once('inc/Careers.php');
//Research
require_once('inc/Research.php');
//out teams
require_once('inc/Out-teams.php');

//contact form
// require_once('inc/contact-form/ApplyForm.php');
// $contactFrom = new ContactForm(array(
//     'name', 'email', 'phone', 'linkedin', 'resume', 'job', 'apply_nonce'
// ));


function vinasupport_remove_custom_post_type_slug( $post_link, $post ) {
    if ( 'race' === $post->post_type && 'publish' === $post->post_status ) {
        $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    }
    return $post_link;
}
add_filter( 'post_type_link', 'vinasupport_remove_custom_post_type_slug', 10, 2 );

function vinasupport_add_post_names_to_main_query( $query ) {
  // Bail if this is not the main query.
  if ( ! $query->is_main_query() ) {
    return;
  }
  // Bail if this query doesn't match our very specific rewrite rule.
  if ( ! isset( $query->query['page'] ) || 2 !== count( $query->query ) ) {
    return;
  }
  // Bail if we're not querying based on the post name.
  if ( empty( $query->query['name'] ) ) {
    return;
  }
  // Add CPT to the list of post types WP will include when it queries based on the post name.
  $query->set( 'post_type', array( 'post', 'page', 'workshops' ) );
}
add_action( 'pre_get_posts', 'vinasupport_add_post_names_to_main_query' );

// Replace editor old
add_filter('use_block_editor_for_post', '__return_false');

// Create shortcode menu mega
function create_megamenu(){
	ob_start();
	?>	
		<?php
			$menus = get_field('op_menus', 'option');
			
			foreach($menus as $menu) {
				?>
					<li>
						<span><?php echo $menu['menu_label']; ?></span>
						<?php
							foreach($menu['menu_text'] as $item) {
								echo '<a href="'.$item['link'].'">'.$item['text'].'</a>';
							}
						?>
					</li>
				<?php
			}
		?>
		
	<?php
	$wpg = ob_get_contents();
	ob_end_clean();
	return $wpg;
}
add_shortcode('megamenu', 'create_megamenu');

// Add Icon to menu items with ACF
add_filter('wp_nav_menu_objects', 'smd_nav_menu_objects', 10, 2);
function smd_nav_menu_objects($items, $args)
{
	// loop
	foreach ($items as &$item) {
		// vars
		$shortcode= get_field('shortcode', $item);

		// append icon
		if ($shortcode) {
			$item->title .= '<ul class="sub-menu list_mega">'.do_shortcode($shortcode).'</ul>';
			
		}
	}
	// return
	return $items;
}

// crop image
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
     add_image_size( 'slider-thumb', 750, 375, true); 
     add_image_size( 'thumbnail', 300, 200, true);
}

add_filter('single_template', 'check_for_category_single_template');
function check_for_category_single_template( $t )
{
foreach( (array) get_the_category() as $cat )
{
if ( file_exists(TEMPLATEPATH . "/single-cat-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-cat-{$cat->slug}.php";
if($cat->parent)
{
$cat = get_the_category_by_ID( $cat->parent );
if ( file_exists(TEMPLATEPATH . "/single-cat-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-cat-{$cat->slug}.php";
}
}
return $t;
}

class MainCore
{
    private static $_instance = null;

    function __construct()
    {
        add_action('after_setup_theme', array($this, 'afterSetupTheme'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue'));
    }

    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function afterSetupTheme()
    {
        load_theme_textdomain('corex', get_template_directory() . '/languages');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
    }

    public function enqueue()
    {
        /*if (!is_single() && !is_tax('teams-category') && !is_page('seminar-single') && !is_page('ai-residency')) {
            wp_enqueue_style('bulma', THEME_URI . '/assets/css/bulma.css');
        }*/
        wp_enqueue_style('styles', THEME_URI . '/style.css');

        wp_enqueue_style('swiper-css', THEME_URI . '/assets/lib/swiper/swiper.min.css');

        wp_enqueue_style('fancybox-css', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css');

        wp_enqueue_style('aos-css', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css');
		
		wp_enqueue_style('custom_c-css', THEME_URI . '/assets/css/custom_c.css');

        wp_enqueue_style('responsive_c-css', THEME_URI . '/assets/css/responsive_c.css'); 
		
		wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js');
		
		wp_enqueue_script('three-js', 'https://cdnjs.cloudflare.com/ajax/libs/three.js/r125/three.min.js');

        wp_enqueue_script('scripts', THEME_URI . '/assets/js/scripts.js', array('jquery'));

        wp_enqueue_script('swiper-js', THEME_URI . '/assets/lib/swiper/swiper.min.js', array('jquery'));

        wp_enqueue_script('fancybox-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', array('jquery'));

        wp_enqueue_script('aos-js', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js', array('jquery'));
		
		wp_enqueue_script('chart-js', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js', array('jquery'));
		
		wp_enqueue_script('font-awesome-js', 'https://kit.fontawesome.com/d9472a5678.js', array('jquery'));

        $wp_script_data = array(
            'AJAX_URL' => ADMIN_AJAX_URL,
            'HOME_URL' => HOME_URL
        );

        wp_localize_script('scripts', 'obj', $wp_script_data);
    }
}

MainCore::instance();


/**
 * custom single file  category
 */
function get_custom_cat_template($single_template)
{
    global $post;
    if (in_category('seminars-events')) {
        $single_template = dirname(__FILE__) . '/single-seminars-events.php';
    }
    return $single_template;
}
add_filter("single_template", "get_custom_cat_template");


//
add_action('pre_get_posts', 'mr_modify_archive_taxonomy_query');

function mr_modify_archive_taxonomy_query($query)
{
    if (!is_admin() && $query->is_main_query()) {
        if (is_post_type_archive('seminar') || is_tax('seminar-category')) {
            $query->set('posts_per_page', 9);
        }
    }
}


function disable_author_page()
{
    global $wp_query;
    if ($wp_query->is_author) {
        wp_safe_redirect(get_bloginfo('url'), 301);
        exit;
    }
}
add_action('wp', 'disable_author_page');

