<?php 
    get_header( );
        $_categories = get_categories(array(
            'hide_empty' => 0,
            'orderby'    => 'id',
            'order'      => 'ASC'
        ));
        $_currentCat = get_queried_object();
        $news_banner = get_field('news_banner', 'option') ? get_field('news_banner', 'option'): NOT_IMAGE;
?>
<div class="news">
    <div class="banner_page section_01"
        style="background-image: url('<?php echo $news_banner; ?>')">
        <div class="tw-container">
            <div class="tw-content">
                <h1><?php _e('news', 'corex') ?></h1>
            </div>
        </div>
    </div>

    <div class="section_02">
        <div class="tw-container">
            <div class="tw-content">
                <?php if(!empty($_categories)){
                    echo '<div class="category_link">';
                    foreach ((array) $_categories as $_category) {
                        $_class = $_currentCat->term_id == $_category->term_id ? 'is--active' : '';
                        if($_category->slug !== 'blog' && $_category->slug !== 'customer-cases' && $_category->slug !== 'products-solutions'){
                            echo '<a href="'.get_category_link($_category->term_id).'" class="'.$_class.'">'.$_category->name.'</a>';
                        }
                    }
                    echo '</div>';
                } ?>

                <div class="the_content">
                    <div class="_list_news">
                        <?php
                        if(have_posts(  )): while (have_posts(  )): the_post(  );
                            $_postID = get_the_ID(  );
                            $_featuredImage = get_the_post_thumbnail_url($_postID) ?  get_the_post_thumbnail_url($_postID) : NOT_IMAGE;
                            $_title = get_the_title($_postID);
                            $_date = get_the_time( 'd/m/Y', $_postID );
                            $_permalink = get_the_permalink($_postID);
                        ?>
                            <div class="__item">
                                <a href="<?php echo $_permalink; ?>" class="thumbnail">
                                    <div class="--bg"
                                        style="background-image: url('<?php echo $_featuredImage; ?>')">
                                    </div>
                                </a>
                                <div class="text">
                                    <a href="<?php echo $_permalink; ?>" class="--title">
                                        <h3><?php echo $_title; ?></h3>
                                    </a>
                                    <p class="--time"><?php echo $_date; ?></p>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata(  ); endif; ?>
                    </div>
                    <?php navigation_custom_ulli(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer( ); ?>