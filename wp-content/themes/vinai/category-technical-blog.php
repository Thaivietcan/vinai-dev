<?php
get_header();
$_currentCat = get_queried_object();
$taxonomy = 'research-area';
$terms_research_area = get_terms([
    'taxonomy'   => $taxonomy,
    'hide_empty' => false,
    'orderby'       => 'id', 
    'order'         => 'ASC',
]);

$blogs = new WP_Query([
    'post_type'      => 'post',
    'category_name'  => $_currentCat->slug,
    'posts_per_page' => 12,
    'post_status'    => 'publish',
]);

?>
<div class="research-page">
	<div class="banner-research">
		<section class="banner_page hero-background" id="h-section1">
			<div class="tw-container">
				<div class="tw-content d-flex justify-content-between flex-direction-colum">
					<div class="research-title">
						<h1 class="tw-title"><?php _e('Research', 'corex'); ?></h1>
						<p class="title_soldier"><?php _e('Explore our research across:', 'corex'); ?></p>
						<p class="_soldier">
							<?php
								if($terms_research_area){
									foreach ($terms_research_area as $key => $cat) {
										?>
											<a href="<?php echo $cat->slug; ?>" class="btn-view-detail change-research" data-slug="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?> <img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt=""></a>
											
										<?php
										
									}
								}
							?>
						</p>
					</div>
					<div class="research-description w-100">
						<?php
							if($terms_research_area){
								$i = 0;
								foreach ($terms_research_area as $key => $cat) {
									$i++;
									$classActive = $i == 1 ? 'is--active' : null;
									echo '<p class="des_research_area '.$classActive.'" id="'.$cat->slug.'">'.$cat->description.'</p>';
								}
							}
						?>
					</div>
				</div>
			</div>
			<div class="shape-one"></div>
			<div class="shape-two"></div>
		</section>
	</div>

	<div class="research-blog news">
		<div class="section_02">
			<div class="tw-container">
				<div class="tw-content">
					<div class="category_link d-flex">
						<a href="<?php bloginfo('url');_e('/publications', 'corex'); ?>"><?php _e('Publications', 'corex'); ?></a>
						<a href="<?php echo get_term_link($_currentCat->term_id); ?>" class="is--active"><?php _e('Blog', 'corex'); ?></a>
					</div>

					<div class="form_search three_input">
						<form action="#" id="frmSearchBlogs">
							<div class="all-feild d-flex align-items-center justify-content-between flex-direction-colum">
								<div class="all-feild-select d-flex ">
									<div class="field-item --select">
										<label for="research_area"><?php _e('Research field', 'corex'); ?></label>
										<div class="_input">
											<select name="research_area" id="research_area">
												<option value="0"><?php _e('All research fields', 'corex'); ?></option>
												<?php if($terms_research_area){
													foreach ($terms_research_area as $key => $cat) {
														echo '<option value="'.$cat->slug.'">'.$cat->name.'</option>';
													}
												} ?>
											</select>
										</div>
									</div>
									<div class="field-item --select">
										<label for="years"><?php _e('Year', 'corex'); ?></label>
										<div class="_input">
											<select name="years" id="years">
												<option value="0"><?php _e('All years', 'corex'); ?></option>
												<?php
													$_now = getdate();
													$_year = intval($_now['year']);
													for ($i = $_year; $i >= 1990 ; $i--) {
														echo '<option value="'.$i.'">'.$i.'</option>';
													};
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="field-item --input ">
									<div class="_input">
										<input type="text" id="searchtxt" name="searchtxt" placeholder="<?php _e('Search', 'corex'); ?>">
										<i class="ion-ios-search-strong"></i>
									</div>
								</div>
							</div>
						</form>
					</div>

					<div class="preloader_member">
						<div class="loading_animation">
							<?php include_once THEME_DIR . '/assets/images/preloader.svg' ?>
						</div>
						<div class="the_content" id="js_content_blog" data-cat="<?php echo $_currentCat->slug; ?>">
							<div class="_list_news list-publications-global d-flex flex-wrap">
								<?php 
									if($blogs->have_posts(  )){ 
										while ($blogs->have_posts(  )){ 
											$blogs->the_post(  ); 
											$blogID = get_the_ID(  );
											$_thumbnail = get_the_post_thumbnail_url($blogID) ? get_the_post_thumbnail_url($blogID) : NOT_IMAGE;
											$_title = get_the_title($blogID);
											$_theExcerpt = get_the_excerpt($blogID);
											$_category = get_the_terms( $blogID, 'research-area' );
											$_time = get_the_time( 'd/m/Y', $blogID );
											$_permalink = get_the_permalink($blogID);
											?>
												<div class="__item">
													<a href="<?php echo $_permalink ?>" class="thumbnail">
														<div class="--bg"
															style="background-image: url(<?php echo $_thumbnail ?>)">
														</div>
													</a>
													<div class="text">
														<span class="sub-heading">
															<?php 
																foreach ( $_category as $cat){
																   echo $cat->name;
																}
															?>
														</span>
														<a href="<?php echo $_permalink ?>" class="--title">
															<h4><?php echo $_title; ?></h4>
														</a>
														<p>
															<?php 
																if (strlen($_theExcerpt) > 100) {
																	echo substr($_theExcerpt, 0, 100) . '(...)';
																} else {
																	echo $_theExcerpt;
																}
															?>
														</p>
														
														<div class="--meta d-flex align-items-center justify-content-between">
															<a href="<?php echo $_permalink ?>" class="btn btn-view-detail">
																<span><?php _e('View details', 'corex'); ?></span>
																<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt="">
															</a>
															<p class="--time"><?php echo $_time; ?></p>
														</div>
													</div>
												</div>
											<?php
										} wp_reset_postdata(  ); 
									} 
								?>
							</div>
							<div class="pagination-common js_panigator">
								<?php wp_navigation_paged_ajax($blogs); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form>
    <?php wp_nonce_field( 'ajax_loading_blogs', 'blogs_nonce' ); ?>
</form>
<script>
jQuery(document).ready(function($){
    const contentBlogEl = $('#js_content_blog');
    const searchtxt = $('#searchtxt');
    const researchArea = $('#research_area');
    const years = $('#years');
    const nonceValue = $('#blogs_nonce').val();
    $('#frmSearchBlogs').on('submit', function(e){
        e.preventDefault();
    })

    $('.change-research').on('click', function(e){
        e.preventDefault();
        const value = $(this).attr('href');
        const desTermEL = $('#'+value+'');
        desTermEL.siblings('.des_research_area').removeClass('is--active');
        desTermEL.addClass('is--active');
        researchArea.val(value);
        setTimeout(() => {
            const _searchtxt = searchtxt.val();
            const _researchArea = researchArea.val();
            const _years = years.val();
            const _slug = contentBlogEl.attr('data-cat');
            searchBlogsFunc(_searchtxt, _researchArea, _years, _slug);
        }, 0);
    });

    $('#searchtxt').keyup(function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _searchtxt = $(this).val();
            const _researchArea = researchArea.val();
            const _years = years.val();
            const _slug = contentBlogEl.attr('data-cat');
            searchBlogsFunc(_searchtxt, _researchArea, _years, _slug);
        }, 800);
    } );

    researchArea.on('change', function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _searchtxt = searchtxt.val();
            const _researchArea = $(this).val();
            const _years = years.val();
            const _slug = contentBlogEl.attr('data-cat');
            searchBlogsFunc(_searchtxt, _researchArea, _years, _slug);
        }, 0);
    });

    years.on('change', function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _searchtxt = searchtxt.val();
            const _researchArea = researchArea.val();
            const _years = $(this).val();
            const _slug = contentBlogEl.attr('data-cat');
            searchBlogsFunc(_searchtxt, _researchArea, _years, _slug);
        }, 0);
    });

    function searchBlogsFunc(searchtxt="", researchArea=0, years=0, slug){
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: obj.AJAX_URL,
            data: {
                action: "ajax_search_load_blogs",
                searchtxt: searchtxt,
                researchArea: researchArea,
                year: years,
                slug: slug,
                nonce: nonceValue,
            },
            beforeSend: () => {
                $('.loading_animation').addClass('is--active');
            },
            complete: () => {
                setTimeout(() => {
                    $('.loading_animation').removeClass('is--active');
                }, 500);
            },
            success: (response) => {
                setTimeout(() => {
                    contentBlogEl.empty();
                    contentBlogEl.append($(response.data));
                }, 300);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
            }
        });
    };

    contentBlogEl.on('click', '.js_panigator .paginate_links a', function(e){
        e.preventDefault();
        const _searchtxt = searchtxt.val();
        const _researchArea = researchArea.val();
        const _years = years.val();
        const slug = contentBlogEl.attr('data-cat');
        var hrefThis = $(this).attr('href');
        var page = hrefThis.match(/\/\d+\//)[0];
        page = page.match(/\d+/)[0];
        if(!page) page = 1;
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: obj.AJAX_URL,
            data: {
                action: "ajax_navigation_load_blogs",
                searchtxt: _searchtxt,
                researchArea: _researchArea,
                year: _years,
                paged: page,
                slug: slug,
                nonce: nonceValue,
            },
            beforeSend: () => {
                $('.loading_animation').addClass('is--active');
            },
            complete: () => {
                setTimeout(() => {
                    $('.loading_animation').removeClass('is--active');
                }, 500);
            },
            success: (response) => {
                setTimeout(() => {
                    contentBlogEl.empty();
                    contentBlogEl.append($(response.data));
                }, 300);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
            }
        });
    });
});
</script>
<?php get_footer(); ?>