<?php
// Template name: Product Platform
get_header();
$title_flatform = get_field('product_flatform_title', 'option');
$sub_flatform = get_field('product_flatform_sub', 'option');

$tab_flatform = get_field('product_flatform_title_tag', 'option');

$p_chart_sub = get_field('p_chart_sub', 'option');
$p_chart_title_number = get_field('p_chart_title_number', 'option');
$p_chart_description_number = get_field('p_chart_description_number', 'option');
$p_chart_detail_1 = get_field('p_chart_detail_1', 'option');
$p_chart_detail_2 = get_field('p_chart_detail_2', 'option');
$p_chart_content = get_field('p_chart_content', 'option');
$p_chart_image = get_field('p_chart_image', 'option');

$p_section_data_title = get_field('p_section_data_title', 'option');
$p_section_data_sub = get_field('p_section_data_sub', 'option');
$p_section_data_number = get_field('p_section_data_number', 'option');
$p_section_data_icon = get_field('p_section_data_icon', 'option');
$p_section_data_description = get_field('p_section_data_description', 'option');

$p_section_gallery = get_field('p_section_gallery', 'option');

$p_section_reimagining_title = get_field('p_section_reimagining_title', 'option');
$p_section_reimagining_sub = get_field('p_section_reimagining_sub', 'option');
$p_section_reimagining_title_mature = get_field('p_section_reimagining_title_mature', 'option');
$p_section_reimagining_mature_list = get_field('p_section_reimagining_mature_list', 'option');
$p_section_reimagining_title_advanced = get_field('p_section_reimagining_title_advanced', 'option');
$p_section_reimagining_advanced_list = get_field('p_section_reimagining_advanced_list', 'option');
$reimagining_description = get_field('p_section_reimagining_description', 'option');

?>
<div class="products product-driver product-flatform">
	<div class="banner_page section_01">
        <div class="tw-container">
            <div class="tw-content">
                <div class="_text w-100">
					<span class="sub-heading" data-aos="fade-up"><?php echo $sub_flatform; ?></span>
					<h2 class="tw-title" data-aos="fade-up"><?php echo $title_flatform; ?></h2>
				</div>
            </div>
        </div>
		<div class="shape-one"></div>
		<div class="shape-two"></div>
    </div>
	<?php if(!empty($tab_flatform)) : ?>
		<div class="tab-seminar tab_products">
			<div class="tw-container">
				<div class="tw-content">
					<div class="category_link d-flex">
						<?php if(!empty($tab_flatform)) : ?>
							<?php if( have_rows('product_flatform_title_tag', 'option') ): ?>
							<?php $i = 1; ?>
							<?php while( have_rows('product_flatform_title_tag', 'option') ): the_row(); 
								$title = get_sub_field('text');
								?>
									<?php 
										if($i === 1) {
											?>
												<a href="#tab-<?php echo $i; ?>" class="is--active"><?php echo $title; ?></a>
											<?php
										}
										else {
											?>
												<a href="#tab-<?php echo $i; ?>"><?php echo $title; ?></a>
											<?php
										}
									?>
								<?php $i++; ?>
							<?php endwhile; ?>
						<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="section_02">
		<div class="tw-container">
            <div class="tw-content d-flex align-items-center justify-content-center flex-direction-colum">
                <div class="_left w-100" data-aos="fade-up">
					<p class="sub_text_chart"><?php echo $p_chart_sub; ?></p>
					<span class="text_nummber">$<span class="count"><?php echo $p_chart_title_number; ?></span>B</span>
					<p class="text_sub"><?php echo $p_chart_description_number; ?></p>
					<div class="number-chart">
						<ul>
							<li class="blue blue-1">
								<span></span>
								<?php echo $p_chart_detail_1; ?>
							</li>
							<li class="blue blue-2">
								<span></span>
								<?php echo $p_chart_detail_2; ?>
							</li>
						</ul>
						<p class="desc"><?php echo $p_chart_content; ?></p>
					</div>
				</div>
				<div class="_right w-100">
					<div class="wrapper prodcut-chart">
						<canvas id="myChart"></canvas>
					</div>
					
				</div>
            </div>
        </div>
	</div>
	<div class="smart-data">
		<div class="tw-container">
            <div class="tw-content">
				<h2 data-aos="fade-up"><?php echo $p_section_data_title; ?></h2>
				<p data-aos="fade-up"><?php echo $p_section_data_sub; ?></p>
				<div class="smart-data-number" data-aos="fade-up">
					<?php if(!empty($p_section_data_number)) : ?>
						<?php 
							foreach($p_section_data_number as $data) {
								echo '
									<div class="_item" data-aos="fade-up">
										<h2>'.$data['text_number'].'</h2>
										<h4>'.$data['title'].'</h4>
										<p>'.$data['description'].'</p>
									</div>
								';
							}
						?>
					<?php endif; ?>
				</div>
			</div>
			<div class="tw-content smart-data-icon">
				<div class="data-icon">
					<?php if(!empty($p_section_data_icon)) : ?>
						<?php 
							foreach($p_section_data_icon as $icons) {
								echo '
									<div class="_item" data-aos="fade-up">
										<img src="'.$icons['icon'].'" />
										<p>'.$icons['text'].'</p>
									</div>
								';
							}
						?>
					<?php endif; ?>
				</div>
				<p data-aos="fade-up" class="description w-100"><?php echo $p_section_data_description; ?></p>
			</div>
		</div>	
	</div>
	
	<div class="gallery">
		<div class="tw-content">
			<div class="list-gallery" data-aos="fade-up">
				<div class="swiper swiper-container swiper-gallery">
					<div class="swiper-wrapper">
						<?php
							foreach($p_section_gallery as $gallerys) {
								?>
									<div class="swiper-slide">
										<img src="<?php echo $gallerys; ?>" />
									</div>
								<?php
							}
						?>
					</div>
					<div class="swiper-pagination-gallery"></div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="section_03 features">
		<div class="tw-container">
			<div class="title-reimaging">
				<h2 data-aos="fade-up"><?php echo $p_section_reimagining_title; ?></h2>
				<p data-aos="fade-up" class=""><?php echo $p_section_reimagining_sub; ?></p>
			</div>
            <div class="tw-content d-flex justify-content-center flex-direction-colum">
                <div class="_left w-100" data-aos="fade-up">
					<h3><?php echo $p_section_reimagining_title_mature; ?></h3>
					
					<ul>
						<?php 
							foreach($p_section_reimagining_mature_list as $lists) {
								echo '<li class="d-flex align-items-center">
										<img src="'.$lists['icon'].'" />
										<span>'.$lists['text'].'</span>
									</li>';
							}
						?>
					</ul>
				</div>
				<div class="_right w-100" data-aos="fade-up">
					<h3><?php echo $p_section_reimagining_title_advanced; ?></h3>
					<ul>
						<?php 
							foreach($p_section_reimagining_advanced_list as $titles) {
								echo '<li class="d-flex align-items-center">
										<img src="'.$titles['icon'].'" />
										<span>'.$titles['text'].'</span>
									</li>';
							}
						?>
					</ul>
				</div>
            </div>
			<p data-aos="fade-up" class="description"><?php echo $reimagining_description; ?></p>
        </div>
	</div>
	
	

	

</div>
<script>
    jQuery(document).ready(function($){
		// Count Number
		$('.count').each(function () {
			$(this).prop('Counter',0).animate({
				Counter: $(this).text()
			}, {
				duration: 4000,
				easing: 'swing',
				step: function (now) {
					$(this).text(Math.ceil(now));
				}
			});
		});
		
		// Tab 
		var swiper_cate_link = new Swiper(".swiper-cate-link", {
			slidesPerView: 2,
			spaceBetween: 30,
      });
	  
	  // Slide
	  var swiper_gallery = new Swiper(".swiper-gallery", {
			slidesPerView: 3,
			spaceBetween: 30,
			loop: true,
			centeredSlides: true,
			pagination: {
				el: ".swiper-pagination-gallery",
				clickable: true,
			},
			breakpoints: {
            1024: {
                slidesPerView: 2,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
      });

		// Tab
		$(".tab_products .category_link a").click(function () {
			$(".tab_products .category_link a").removeClass("is--active");
			$(this).addClass("is--active");   
		});
		
		// Chart Product
		createChart('myChart', myChartData);
		
    })
	
	const myChartData = {
    type: "doughnut",
    data: {
		labels: ["Building training sets","Cleaning and organizing data", "Collecting data sets", "Mining data for patterns", "Refining algorithms", "Other"],
      datasets: [
        {
          label: "Test",
          data: [3,60,19,9,4,5],
          backgroundColor: [
			"rgba(204, 204, 204, 1)",
            "rgba(24, 124, 250, 1)",
            "rgba(130, 255, 255, 1)",
            "rgba(0, 0, 0, 1)",
            "rgba(51, 51, 51, 1)",
            "rgba(159, 159, 159, 1)",
            "rgba(204, 204, 204, 1)",
          ],
          borderColor: [
			"rgba(204, 204, 204, 1)",
            "rgba(24, 124, 250, 1)",
            "rgba(130, 255, 255, 1)",
            "rgba(0, 0, 0, 1)",
            "rgba(51, 51, 51, 1)",
            "rgba(159, 159, 159, 1)",
            "rgba(204, 204, 204, 1)",
          ],
          borderWidth: 1,
        },
      ],
    },
    options: {
      title: {
        display: false,
        text: "This is your 401(k) account's current asset mix",
      },
      animation: {
        animateScale: true,
        animateRotate: true,
      },
      responsive: true,
      maintainAspectRatio: true,

      legend: {
        display: false,
      },
    },
  };
  
  function createChart(chartId, chartData) {
    const ctx = document.getElementById(chartId);
    const myChart = new Chart(ctx, {
      type: chartData.type,
      data: chartData.data,
      options: chartData.options,
    });
  };
  
</script>
<?php get_footer(); ?>