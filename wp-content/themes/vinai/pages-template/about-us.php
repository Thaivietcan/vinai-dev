<?php
    //Template Name: About us
    get_header();
    $taxonomy_team = 'teams-category';
    $taxonomy_location = 'location-category';
    $locations_internship_excrept = get_field('locations_internship_excrept', 'option');
    $terms_teams_category = get_terms([
        'taxonomy'   => $taxonomy_team,
        'hide_empty' => false,
        'orderby'       => 'id', 
        'order'         => 'ASC',
    ]);
    $terms_location_category = get_terms([
        'taxonomy'   => $taxonomy_location,
        'hide_empty' => false,
        'orderby'       => 'id', 
        'order'         => 'ASC',
    ]);
    // Banner
    $title = get_field('about_title', 'option');
    $description = get_field('about_description', 'option');
    // Who we are
    $title_who = get_field('about_who_title', 'option');
    $description_who = get_field('about_who_description', 'option');
    $about_list_who = get_field('about_list_who', 'option');
    // Our Team
    $title_team = get_field('about_team_title', 'option');
    $image_team = get_field('about_background_team', 'option');

    $about_banner = get_field('about_image_banner', 'option');
    $about_color = get_field('about_background_color', 'option');
?>
<div class="about-us">
    <section class="h-section1 hero-background" id="h-section1">
        <div class="banner_site" style="background-color: <?php echo $about_color ?>"></div>
        <div class="content_banner_site">
            <div class="tw-container">
                <div class="tw-content">
                    <h1 data-aos="fade-up" data-aos-delay="300">
                        <?php echo $title; ?><br>
                        <span><?php echo $description; ?></span>
                    </h1>
                </div>
            </div>
        </div>
		<div id="webgl-canvas"></div>
    </section>
    <div class="section_02" id="<?php _e('our-story', 'corex'); ?>">
        <div class="tw-container">
            <div class="tw-content">
                <div class="_item d-flex flex-direction-colum">
                    <div class="__left">
                        <h2 data-aos="fade-up"><?php echo $title_who; ?></h2>
                    </div>
                    <div class="__right">
                       <div class="__item_description" data-aos="fade-up">
                            <?php echo $description_who; ?>
                       </div>
                        <div class="slide-list-who">
                            <!-- Swiper -->
                            <div class="swiper swiper-container all-list-who pc" data-aos="fade-up" data-aos-delay="300">
                                <div class="swiper-wrapper">
                                    <?php 
                                        foreach($about_list_who as $value) {
                                            ?>
                                                <div class="swiper-slide">
                                                    <div class="list-who-item">
                                                        <img src="<?php echo $value['about_list_who_image'] ?>" alt="">
                                                        <h3><?php echo $value['title_about_list_who'] ?></h3>
                                                        <p><?php echo $value['about_list_who_description'] ?></p>
                                                    </div>
                                                </div>
                                            <?php
                                        } 
                                    ?>
                                </div>
                            </div>

                            <!-- Mobile -->
                            <div class="mb">
                                <?php 
                                    foreach($about_list_who as $value) {
                                        ?>
                                            <div class="list-who-item">
                                                <img src="<?php echo $value['about_list_who_image'] ?>" alt="">
                                                <h3><?php echo $value['title_about_list_who'] ?></h3>
                                                <p><?php echo $value['about_list_who_description'] ?></p>
                                            </div>
                                        <?php
                                    } 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section_04 our-teams-common" id="<?php _e('teams', 'corex'); ?>" style="background-image: url(<?php echo $image_team; ?>)">
        <div class="tw-container">
            <div class="tw-content">
                <div class="_item d-flex flex-direction-colum">
                    <div class="__left">
                        <h2 data-aos="fade-up" data-aos-delay="300"><?php echo $title_team; ?></h2>
                    </div>
                    <div class="__right">
                        <div class="slide-list-team">
                            <div class="_our_team_item">
                            <div class="swiper swiper-team-about">
                            <div class="swiper-wrapper">
                            <?php
                                if(!empty($terms_teams_category)){
                                    foreach ($terms_teams_category as $teams) {
                                        $_termID = $teams->term_id;
                                        $_name = $teams->name;
                                        $_description = $teams->description;
                                        $_image = get_field('careers_image_category', 'term_'.$_termID);
                                        $_link = get_term_link($_termID);
                                        $_linkCareersParam = get_bloginfo('url').'/'.__('careers').'/?team='.$teams->slug;
                                        echo '
                                            <div class="swiper-slide" data-aos="fade-up" data-aos-delay="300">
                                                <div class="__item" term-slug="'.$teams->slug.'">
                                                <a href="'.$_linkCareersParam.'" class="--image">
                                                    <img class="icon_main" src="'.$_image.'" /> 
                                                    <p class="tw-title">'.$_name.'</p>
                                                </a>
                                                <p class="--txt">'.$_description.'</p>
                                                <a href="'.$_linkCareersParam.'" class="btn btn-view-detail btn-view-detail-home">
                                                    <span>'.__('Explore Jobs', 'corex').'</span>
                                                    <img src="'.THEME_ASSETS . '/images/icon-right.svg'.'"/>
                                                </a>
                                                </div>
                                            </div>
                                            ';
                                    }
                                }
                            ?>
                            </div>
							<div class="swiper-pagination-our-team mb"></div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function($){
        var swiperListWho = new Swiper(".all-list-who", {
        slidesPerView: 'auto',
        spaceBetween: 30,
        loop: true, 
        speed: 800,
        grabCursor: true,
        parallax: true,
        observer: true,
        observeParents: true,
        pagination: {
			clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 15
            }
        }
      });

      // Our Team
      var swiper = new Swiper(".swiper-team-about", {
        slidesPerView: 3,
        spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination-our-team",
          clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 15
            }
        }
      });
    });
</script>

 <script type="text/javascript">
      const particleWave = () => {
        const heroBanner = document.querySelector(".hero-background");
        let w = heroBanner.clientWidth;
        let h = heroBanner.clientHeight;
        const dpr = window.devicePixelRatio;

        const fov = 60;
        const fovRad = fov * (Math.PI / 180);
        const dist = h / 2 / Math.tan(fovRad);

        const clock = new THREE.Clock();
        const pointSize = 7 * dpr;

        const renderer = new THREE.WebGLRenderer({ alpha: true });
        renderer.setSize(w, h);
        renderer.setClearColor(0x000000, 0);
        renderer.setPixelRatio(dpr);

        const container = document.getElementById("webgl-canvas");
        container.appendChild(renderer.domElement);

        const camera = new THREE.PerspectiveCamera(fov, w / h, 1, dist * 8);
        camera.position.x = 0;
        camera.position.y = 20;
        camera.position.z = 150;

        const scene = new THREE.Scene();

        // TODO: This fog isn't working for some reason
        const fogColor = "#000"; // white
        const near = 1.8;
        const far = 2;
        scene.fog = new THREE.Fog(fogColor, near, far);

        const geo = new THREE.BufferGeometry();
        const positions = [];

        const width = 250 * (w / h);
        const depth = 100;
        const distance = 5;

        for (let x = 0; x < width; x += distance) {
          for (let z = 0; z < depth; z += distance) {
            positions.push(-width / 2 + x, -30, -depth / 2 + z);
          }
        }
        const positionAttribute = new THREE.Float32BufferAttribute(
          positions,
          3
        );
        geo.setAttribute("position", positionAttribute);

        const mat = new THREE.ShaderMaterial({
          uniforms: {
            u_time: {
              value: 0.0,
            },

            color1: {
              value: new THREE.Color("#01196B"),
            },

            color2: {
              value: new THREE.Color("#01196B"),
            },

            color3: {
              value: new THREE.Color("#01196B"),
            },

            color4: {
              value: new THREE.Color("#01196B"),
            },

            color5: {
              value: new THREE.Color("#01196B"),
            },

            resolution: {
              type: "v2",
              value: new THREE.Vector2(w * dpr, h * dpr),
            },
            pointSize: { value: pointSize },
          },

          vertexShader: `
        precision highp float;
        #define M_PI 3.1415926535897932384626433832795
        
        uniform float u_time;
        uniform float pointSize;
        
        void main() {
          vec3 p = position;
          p.y += (
             cos(p.x / M_PI * 8.0 + u_time * 0.5) * 15.0 +
             sin(p.z / M_PI * 8.0 + u_time * 0.5) * 15.0 + 
             60.0
           ) ;
          
          gl_PointSize = pointSize;
          gl_Position = projectionMatrix * modelViewMatrix * vec4(p, 1.0);
        }   
    
    `,
          fragmentShader: `
        precision highp float;
        
        uniform vec3 color1;
        uniform vec3 color2;
        uniform vec3 color3;
        uniform vec3 color4;
        uniform vec3 color5;
        uniform vec2 resolution;
        
        void main() {
          // create circles instead of squares
          if ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.475 ) discard;
          
          float x = gl_FragCoord.x;
          float step1 = 0.25;
          float step2 = 0.45;
          float step3 = 0.55;
          float step4 = 0.75;
          float step5 = 1.00;
          
          float mixValue = x / resolution.x;
          
          // create color stops using 'mix', and convert the progress through each 'step'
          // to a value between 0 and 1
          vec3 mixedColor;
          if(mixValue < step1) {
            mixedColor = mix(color1, color2, mixValue / step1);
          } else if (mixValue >= step1 && mixValue < step2) {
            mixedColor = mix(color2, color3, ((mixValue - step1) / (step2 - step1)));
          } else if (mixValue >= step2 && mixValue < step3) {
            mixedColor = color3;
          } else if (mixValue >= step3 && mixValue < step4) {
            mixedColor = mix(color3, color4, ((mixValue - step3) / (step4 - step3)));
          } else {
            mixedColor = mix(color4, color5, ((mixValue - step4) / (step5 - step4)));
          }
          
          gl_FragColor = vec4(mixedColor, 1.0);
        }
    `,
        });

        const mesh = new THREE.Points(geo, mat);
        scene.add(mesh);

        function render() {
          const time = clock.getElapsedTime();
          mesh.material.uniforms.u_time.value = time;
          renderer.render(scene, camera);
          requestAnimationFrame(render);
        }
        render();

        function onWindowResize() {
          w = heroBanner.clientWidth;
          h = heroBanner.clientHeight;
          camera.aspect = w / h;
          camera.updateProjectionMatrix();
          renderer.setSize(w, h);
        }
        window.addEventListener("resize", onWindowResize);
      };

      window.addEventListener("load", particleWave);
    </script>
<?php
    get_footer();
?>