<?php
//Template name: AI Residency
get_header();
?>
<?php 
    $home_audience = get_field('home_audience', 'option');
    $home_benefits = get_field('home_benefits', 'option');
    $home_requi_rements = get_field('home_requi_rements', 'option');
    $des_banner_left = get_field('ai_residency_des_01', 'option');
    $des_banner_right = get_field('ai_residency_des_02', 'option');
    $description_site = get_field('ai_residency_des_03', 'option');
    $description_requirement = get_field('ai_residency_req_description', 'option');
    $requirements = get_field('ai_residency_requirements', 'option');
?>
<div class="ai-residency">
    <div class=" setions_01 banner" style="background-image: url('<?php echo THEME_ASSETS .'/images/ai-residency/banner.png'; ?>');">
        <div class="tw-container">
            <div class="tw-content">
                <h1>- <?php _e('AI Residency', 'corex'); ?> <span><?php _e('Program', 'corex'); ?></span></h1>
                <div class="_description">
                    <p><?php echo $des_banner_left; ?></p>
                    <p><?php echo $des_banner_right; ?></p>
                </div>
                <a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSdo_wsntIkZvxxbvQoHXrF9QiIJeGlgQvD7bz0Ike1v--_ANA/viewform" class="tw-button-common">
					<span><?php _e('Apply Now', 'corex'); ?></span>
					<img src="<?php echo THEME_ASSETS .'/images/common/right.png'; ?>" alt="">
				</a>
            </div>
        </div>
    </div>
    <div class="section_011 description_2">
        <div class="tw-container">
            <div class="tw-content">
                <?php echo $description_site; ?>
            </div>
        </div>
    </div>

    <div class="h-section5 section_02">
        <div class="tw-container">
            <div class="tw-content">
                <div class="left">
                    <div class="_item">
                        <h3><?php _e('Audience', 'corex'); ?></h3>
                        <p><?php echo $home_audience; ?></p>
                    </div>
                </div>
                <div class="right">
                    <div class="_item">
                        <h3><?php _e('Benefits', 'corex'); ?></h3>
                        <p><?php echo $home_benefits; ?></p>

                        <div class="__dot">
                            <img src="<?php echo THEME_ASSETS .'/images/homes/dot2.png'; ?>" alt="">
                        </div>
                    </div>
                    <div class="_item">
                        <h3>
                            <img src="<?php echo THEME_ASSETS .'/images/homes/dot3.png'; ?>" alt="">
                            <p><span><?php _e('Require', 'corex'); ?></span><span class="line"></span></p><p><?php _e('ment', 'corex'); ?></p>
                        </h3>
                        <p><?php echo $home_requi_rements; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_03">
        <div class="tw-container">
            <div class="tw-content">
                <p class="_label"><?php echo $description_requirement; ?></p>

                <div class="_process">
                    <?php 
                    if(!empty($requirements) && count($requirements) > 0){
                        $i = 0;
                        foreach ($requirements as $key => $requirement) {
                            $i++;
                            $i = $i < 10 ? '0'.$i : $i;
                            echo '<div class="__item">
                                    <p class="--number">'.$i.'.</p>
                                    <p>'.$requirement['ai_residency_des'].'</p>
                                </div>';
                        }
                    } ?>
                </div>

                <a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSdo_wsntIkZvxxbvQoHXrF9QiIJeGlgQvD7bz0Ike1v--_ANA/viewform" class="tw-button-common">
					<span><?php _e('Apply Now', 'corex'); ?></span>
					<img src="<?php echo THEME_ASSETS .'/images/common/right.png'; ?>" alt="">
				</a>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>