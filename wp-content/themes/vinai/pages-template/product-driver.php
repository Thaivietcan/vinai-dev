<?php

// Template name: Product Driver

get_header();

$title_driver = get_field('product_driver_title', 'option');
$sub_driver = get_field('product_driver_sub', 'option');
$content_driver = get_field('product_driver_content', 'option');

$tab_driver = get_field('product_driver_title_tab', 'option');

$text_number = get_field('section_driver_text_number', 'option');
$description = get_field('section_driver_description', 'option');
$section_driver_content = get_field('section_driver_content', 'option');
$section_driver_image = get_field('section_driver_image', 'option');

$section_feature_title_main = get_field('section_feature_title_main', 'option');
$section_feature_main = get_field('section_feature_main', 'option');
$section_feature_title_advanced = get_field('section_feature_title_advanced', 'option');
$section_feature_advanced = get_field('section_feature_advanced', 'option');

$section_video_link = get_field('section_video_link', 'option');
$section_video_image = get_field('section_video_image', 'option');

$section_technology_title = get_field('section_technology_title', 'option');
$section_technology_list = get_field('section_technology_list', 'option');

$section_why_title = get_field('section_why_title', 'option');
$d_section_why_list = get_field('d_section_why_list', 'option');

?>

<div class="products product-driver">

	<div class="banner_page section_01">

        <div class="tw-container">

            <div class="tw-content">

                <div class="_text w-100">

					<span class="sub-heading"  data-aos="fade-up"><?php echo $sub_driver; ?></span>

					<h2 class="tw-title"  data-aos="fade-up"><?php echo $title_driver; ?></h2>

					<p  data-aos="fade-up"><?php echo $content_driver; ?></p>

				</div>

            </div>

        </div>

		<div class="shape-one"></div>

		<div class="shape-two"></div>

    </div>

	<div class="tab-seminar tab_products">

		<div class="tw-container">

			<div class="tw-content pc">

				<div class="category_link d-flex">

					<?php if(!empty($tab_driver)) : ?>

						<?php if( have_rows('product_driver_title_tab', 'option') ): ?>

						<?php $i = 1; ?>

						<?php while( have_rows('product_driver_title_tab', 'option') ): the_row(); 

							$title = get_sub_field('title');

							?>

								<?php 

									if($i === 1) {

										?>

											<a href="#tab-<?php echo $i; ?>" class="is--active"><?php echo $title; ?></a>

										<?php

									}

									else {

										?>

											<a href="#tab-<?php echo $i; ?>"><?php echo $title; ?></a>

										<?php

									}

								?>

							<?php $i++; ?>

						<?php endwhile; ?>

					<?php endif; ?>

					<?php endif; ?>

                </div>

			</div>

			<div class="tw-content mb">

				<div class="category_link">

					<div class="swiper swiper-container swiper-cate-link">

						<div class="swiper-wrapper">

								<?php if( have_rows('product_driver_title_tab', 'option') ): ?>

								<?php $i = 1; ?>

								<?php while( have_rows('product_driver_title_tab', 'option') ): the_row(); 

									$title = get_sub_field('title');

									?>

										<div class="swiper-slide">

											<?php 

												if($i === 1) {

													?>

														<a href="#tab-<?php echo $i; ?>" class="is--active"><?php echo $title; ?></a>

													<?php

												}

												else {

													?>

														<a href="#tab-<?php echo $i; ?>"><?php echo $title; ?></a>

													<?php

												}

											?>

										</div>

									<?php $i++; ?>

								<?php endwhile; ?>

							<?php endif; ?>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<div class="section_02">

		<div class="tw-container">

            <div class="tw-content d-flex align-items-center justify-content-center flex-direction-colum">

                <div class="_left w-100">

					<span class="text_nummber"><?php echo $text_number; ?></span>

					<p class="text_sub"><?php echo $description; ?></p>

					<p class="text_heading"  data-aos="fade-up"><?php echo $section_driver_content; ?></p>

				</div>

				<div class="_right w-100"  data-aos="fade-up">

					<img class="border-radius-10" src="<?php echo $section_driver_image; ?>" />

				</div>

            </div>

        </div>

	</div>

	<div class="section_03 page-section" id="tab-1">

		<div class="tw-container">

            <div class="tw-content d-flex justify-content-center flex-direction-colum">

                <div class="_left w-100"  data-aos="fade-up">

					<h3><?php echo $section_feature_title_main; ?></h3>

					<ul>

						<?php 

							foreach($section_feature_main as $title_feature) {

								echo '<li class="d-flex align-items-center">

										<img src="'.$title_feature['section_feature_icon'].'" />

										<span>'.$title_feature['section_feature_text'].'</span>

									</li>';

							}

						?>

					</ul>

				</div>

				<div class="_right w-100"  data-aos="fade-up">

					<h3><?php echo $section_feature_title_advanced; ?></h3>

					<ul>

						<?php 

							foreach($section_feature_advanced as $title_feature) {

								echo '<li class="d-flex align-items-center">

										<img src="'.$title_feature['section_feature_ad_icon'].'" />

										<span>'.$title_feature['section_feature_ad_text'].'</span>

									</li>';

							}

						?>

					</ul>

				</div>

            </div>

        </div>

	</div>

	<div class="section-video" >

		<div class="tw-container">

            <div class="tw-content">

				<div class="card-video"  data-aos="fade-up">

				  <!--<a data-fancybox href="<?php echo $section_video_link ? $section_video_link : 'javascript:void(0);'; ?>">

					<img class="card-img-top img-fluid border-radius-10" src="<?php echo $section_video_image; ?>" />

					<span><i class="fas fa-play"></i></span>

				  </a> -->

				  <iframe src="<?php echo $section_video_link; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

				</div>

			</div>

		</div>	

	</div>

	<div class="section_04 page-section" id="tab-2">

		<div class="tw-container">

            <div class="tw-content">

				<h2><?php echo $section_technology_title; ?></h2>

				<div class="list-techonogy"  data-aos="fade-up">

					<?php if(!empty($section_technology_list)) : ?>

						<?php 

							foreach($section_technology_list as $list) {

								echo '

									<div class="_item text-align-center">

										<img src="'.$list['technology_icon'].'"/>

										<p>'.$list['technology_text'].'</p>

									</div>

								';

							}

						?>

					<?php endif; ?>

				</div>

			</div>

		</div>	

	</div>

	<div class="section_05 page-section" id="tab-3">

		<div class="tw-container">

            <div class="tw-content d-flex justify-content-center flex-direction-colum">

				<h2><?php echo $section_why_title; ?></h2>

				<div class="list-why-us w-100">

					<ul class=""  data-aos="fade-up">

					<?php if(!empty($d_section_why_list)) : ?>

						<?php 

							foreach($d_section_why_list as $lists) {

								?>

									<li class="d-flex">

										<i class="fas fa-check"></i>

										<span><?php echo $lists['d_section_why_text']; ?></span>

									</li>

								<?php

							}

						?>

					<?php endif; ?>

					</ul>

				</div>

			</div>

		</div>	

	</div>

</div>

<script>

    jQuery(document).ready(function($){

		var swiper_cate_link = new Swiper(".swiper-cate-link", {

			slidesPerView: 2,

			spaceBetween: 30,

      });



		// Tab

		$(".tab_products .category_link a").click(function () {

			$(".tab_products .category_link a").removeClass("is--active");

			$(this).addClass("is--active");   

		});

		

    })

</script>



<?php get_footer(); ?>