<?php
// Template name: Product Surround
get_header();
$title_surround = get_field('product_surround_title', 'option');
$sub_surround = get_field('product_surround_sub', 'option');
$content_surround = get_field('product_surround_content', 'option');

$surround_tab = get_field('product_s_title_tab', 'option');

$video_sub = get_field('s_section_video_sub', 'option');
$video_title = get_field('s_section_video_title', 'option');
$video_link = get_field('s_section_video_link', 'option');
$video_image = get_field('s_section_video_image', 'option');

$feature_title_main = get_field('s_section_feature_title_main', 'option');
$feature_main_list = get_field('s_section_feature_main_list', 'option');
$feature_title_advanced = get_field('s_section_feature_title_advanced', 'option');
$feature_advanced_list = get_field('s_section_feature_advanced_list', 'option');

$why_title = get_field('s_section_why_title', 'option');
$why_list = get_field('s_section_why_list', 'option');

?>
<div class="products product-driver product-surround">
	<div class="banner_page section_01">
        <div class="tw-container">
            <div class="tw-content">
                <div class="_text w-100">
					<span class="sub-heading" data-aos="fade-up"><?php echo $sub_surround; ?></span>
					<h2 class="tw-title" data-aos="fade-up"><?php echo $title_surround; ?></h2>
					<p data-aos="fade-up"><?php echo $content_surround; ?></p>
				</div>
            </div>
        </div>
		<div class="shape-one"></div>
		<div class="shape-two"></div>
    </div>
	<div class="tab-seminar tab_products">
		<div class="tw-container">
			<div class="tw-content pc">
				<div class="category_link d-flex">
					<?php if(!empty($surround_tab)) : ?>
						<?php if( have_rows('product_s_title_tab', 'option') ): ?>
						<?php $i = 1; ?>
						<?php while( have_rows('product_s_title_tab', 'option') ): the_row(); 
							$title = get_sub_field('text');
							?>
								<?php 
									if($i === 1) {
										?>
											<a href="#tab-<?php echo $i; ?>" class="is--active"><?php echo $title; ?></a>
										<?php
									}
									else {
										?>
											<a href="#tab-<?php echo $i; ?>"><?php echo $title; ?></a>
										<?php
									}
								?>
							<?php $i++; ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php endif; ?>
                </div>
			</div>
			<div class="tw-content mb">
				<div class="category_link">
					<div class="swiper swiper-container swiper-cate-link">
						<div class="swiper-wrapper">
								<?php if( have_rows('product_s_title_tab', 'option') ): ?>
								<?php $i = 1; ?>
								<?php while( have_rows('product_s_title_tab', 'option') ): the_row(); 
									$title = get_sub_field('text');
									?>
										<div class="swiper-slide">
											<?php 
												if($i === 1) {
													?>
														<a href="#tab-<?php echo $i; ?>" class="is--active"><?php echo $title; ?></a>
													<?php
												}
												else {
													?>
														<a href="#tab-<?php echo $i; ?>"><?php echo $title; ?></a>
													<?php
												}
											?>
										</div>
									<?php $i++; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section_02">
		<div class="tw-container">
            <div class="tw-content d-flex align-items-center justify-content-center flex-direction-colum">
                <div class="_left w-100">
					<p class="text_sub" data-aos="fade-up"><?php echo $video_sub; ?></p>
					<p class="text_heading" data-aos="fade-up"><?php echo $video_title; ?></p>
				</div>
            </div>
        </div>
	</div>
	<div class="section-video">
		<div class="tw-container">
            <div class="tw-content">
				<div class="card-video" data-aos="fade-up">
				  <iframe src="<?php echo $video_link; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>	
	</div>
	<div class="section_03 page-section" id="tab-1">
		<div class="tw-container">
            <div class="tw-content d-flex justify-content-center flex-direction-colum">
                <div class="_left w-100" data-aos="fade-up">
					<h3><?php echo $feature_title_main; ?></h3>
					<ul >
						<?php 
							foreach($feature_main_list as $titles) {
								echo '<li class="d-flex align-items-center">
										<img src="'.$titles['icon'].'" />
										<span>'.$titles['text'].'</span>
									</li>';
							}
						?>
					</ul>
				</div>
				<div class="_right w-100" data-aos="fade-up">
					<h3><?php echo $feature_title_advanced; ?></h3>
					<ul>
						<?php 
							foreach($feature_advanced_list as $titles) {
								echo '<li class="d-flex align-items-center">
										<img src="'.$titles['icon'].'" />
										<span>'.$titles['text'].'</span>
									</li>';
							}
						?>
					</ul>
				</div>
            </div>
        </div>
	</div>
	
	<div class="section_05 page-section" id="tab-2">
		<div class="tw-container">
            <div class="tw-content">
				<h2><?php echo $why_title; ?></h2>
				<div class="list-why-us w-100">
					<ul class="">
					<?php if(!empty($why_list)) : ?>
						<?php 
							foreach($why_list as $lists) {
								?>
									<li class="d-flex" data-aos="fade-up">
										<i class="fas fa-check"></i>
										<span><?php echo $lists['tex']; ?></span>
									</li>
								<?php
							}
						?>
					<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>	
	</div>
</div>
<script>
    jQuery(document).ready(function($){
		var swiper_cate_link = new Swiper(".swiper-cate-link", {
			slidesPerView: 2,
			spaceBetween: 30,
			
      });

		// Tab
		$(".tab_products .category_link a").click(function () {
			$(".tab_products .category_link a").removeClass("is--active");
			$(this).addClass("is--active");   
		});
    })
</script>
<?php get_footer(); ?>