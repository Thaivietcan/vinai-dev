<?php
//Template name: Research Blog
get_header();
?>
<div class="banner-research" style="background-image: url('<?php echo THEME_ASSETS .'/images/research-blog/banner.svg'; ?>')">
    <div class="tw-container">
        <div class="tw-content">
            <h1>Research</h1>
            <p class="_soldier">
                <a href="Javascript:void(0);">Machine learning</a>
                <a href="Javascript:void(0);">Deep Learning</a>
                <a href="Javascript:void(0);">Computer Vision </a>
                <a href="Javascript:void(0);">Natural Language Understanding</a>
            </p>
            <p>Our research aims to advance our understanding of the fundamentals in Machine Learning, Deep Learning,
                and to investigate how they enable new AI methods in Computer Vision and Language Understanding. As part
                of the VinGroup ecosystem, we are well positioned to be at the forefront of the digitalization of
                customer experience via AI-based systems that can understand human natural interaction through voices,
                gestures, behaviors, or from smart sensors and devices. Due to our unique location, we are also
                naturally drawn towards important problems in developing countries that might otherwise be overlooked in
                the research community.</p>
        </div>
    </div>
</div>

<div class="research-blog news">
    <div class="section_02">
        <div class="tw-container">
            <div class="tw-content">
                <div class="category_link">
                    <a href="<?php bloginfo('url');_e('/research-blog', 'corex'); ?>" class="is--active">Blog</a>
                    <a href="<?php bloginfo('url');_e('/research-publication', 'corex'); ?>">Publication</a>
                </div>

                <div class="form_search three_input">
                    <form action="#" id="frmOutTeam">
                        <div class="all-feild">
                            <div class="field-item --input">
                                <label for="searchtxt">Search</label>
                                <div class="_input">
                                    <img src="<?php echo THEME_ASSETS . '/images/common/search.png'; ?>" alt="">
                                    <input type="text" id="searchtxt" name="searchtxt" placeholder="Search">
                                </div>
                            </div>
                            <div class="field-item --select">
                                <label for="research_area">Research Area</label>
                                <div class="_input">
                                    <select name="research_area" id="research_area">
                                        <option value="0">All</option>
                                        <option value="27">Applied AI</option>
                                        <option value="29">Operations</option>
                                        <option value="25">Research</option>
                                    </select>
                                </div>
                            </div>
                            <div class="field-item --select">
                                <label for="years">Years</label>
                                <div class="_input">
                                    <select name="years" id="years">
                                        <option value="0">All</option>
                                        <option value="1990">1990</option>
                                        <option value="1991">1991</option>
                                        <option value="1992">1992</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="the_content">
                    <div class="_list_news">
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                        <div class="__item">
                            <a href="#" class="thumbnail">
                                <div class="--bg"
                                    style="background-image: url('<?php echo THEME_ASSETS .'/images/news/banner.jpg'; ?>')">
                                </div>
                            </a>
                            <div class="text">
                                <a href="#" class="--title">
                                    <h3>Fugiat nulla pariatur. Excepteur sint occaecat</h3>
                                </a>
                                <p class="--time">12/08/2020</p>
                            </div>
                        </div>
                    </div>
                    <div class="pagination-common js_panigator">
                        <div class="paginate_links">
                            <a class="prev page-numbers" href="http://nsrp.vn/page/1/">&lt;</a>
                            <a class="page-numbers" href="http://nsrp.vn/page/1/">1</a>
                            <span aria-current="page" class="page-numbers current">2</span>
                            <a class="page-numbers" href="http://nsrp.vn/page/3/">3</a>
                            <span class="page-numbers dots">…</span>
                            <a class="page-numbers" href="http://nsrp.vn/page/8/">8</a>
                            <a class="next page-numbers" href="http://nsrp.vn/page/3/">&gt;</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>