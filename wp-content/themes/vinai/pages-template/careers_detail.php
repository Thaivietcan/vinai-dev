<?php
// Template name: Careers detail
get_header();
?>
<div class="careers_detail">
    <div class="tw-container">
        <div class="tw-content">
            <h1>BrSE (bonus 1/2 monthly salary)</h1>
            <p class="view_jobs">at VinAi <a href="<?php echo bloginfo('url').'/'.__('careers', 'corex') ?>">(View all jobs)</a></p>
            <p class="location">Ha Noi, HCM City</p>
            <div class="thumbnail">
                <img src="<?php echo THEME_ASSETS .'/images/news/detail.jpg'; ?>" alt="">
            </div>
            <div class="the_content">
                <p>Show content...</p>
            </div>
            <div class="form_join_us">
                <h3><?php _e('Join us and make awesome things that matter', 'corex') ?>.</h3>
                <p class="_apply"><?php _e('Apply for this Job','corex') ?></p>
                <form action="" id="frm_apply">
                    <div class="field_all">
                        <div class="field_group">
                            <label for="txtname"><?php _e('Full Name', 'corex'); ?></label>
                            <input type="text" name="name" id="txtname" placeholder="<?php _e('Input your Name', 'corex'); ?>">
                        </div>
                        <div class="field_group">
                            <label for="txtemail"><?php _e('Email', 'corex'); ?></label>
                            <input type="email" name="enail" id="txtemail" placeholder="<?php _e('Input your Email', 'corex'); ?>">
                        </div>
                        <div class="field_group">
                            <label for="txtphone"><?php _e('Phone', 'corex'); ?></label>
                            <input type="text" name="phone" id="txtphone" placeholder="<?php _e('Input your PhoneNumber', 'corex'); ?>">
                        </div>
                        <div class="field_group">
                            <label for="txtlinkedin"><?php _e('Linkedin Profile', 'corex'); ?></label>
                            <input type="text" name="linkedin" id="txtlinkedin" placeholder="<?php _e('Input your Link', 'corex'); ?>">
                        </div>
                        <div class="field_group">
                            <label><?php _e('Resume', 'corex'); ?></label>
                            <label for="txtfile"><?php _e('Attach, Dropbox, Paste') ?></label>
                            <input type="file" name="file" id="txtfile">
                        </div>
                    </div>
                    <button type="submit" id="btn_submit" class="tw-button-common">
                        <span><?php _e('Apply', 'corex'); ?></span>
                        <img src="<?php echo THEME_ASSETS .'/images/common/right.png'; ?>" alt="">
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php get_footer();