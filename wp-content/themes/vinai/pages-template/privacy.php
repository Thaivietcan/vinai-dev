<?php 
	//Template Name: Privacy
	get_header();
 ?>
 <div class="page-privacy">
	<div class="tw-container">
		<div class="content">
			<h1 class="tw-title"><?php the_title(); ?></h1>
			<div class="article">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
 </div>
 <?php get_footer(); ?>