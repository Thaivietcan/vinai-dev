<?php
//Template name: Research Publication
get_header();
$taxonomy = 'research-area';
$terms_research_area = get_terms([
    'taxonomy'   => $taxonomy,
    'hide_empty' => false,
    'orderby'       => 'id', 
    'order'         => 'ASC',
]);
?>
<div class="banner-research">
    <div class="bg-banner" style="background-image: url('<?php echo THEME_ASSETS .'/images/research-blog/banner.svg'; ?>')"></div>
    <div class="tw-container">
        <div class="tw-content">
            <h1><?php _e('Research', 'corex'); ?></h1>
            <p class="_soldier">
                <?php
                    if($terms_research_area){
                        foreach ($terms_research_area as $key => $cat) {
                            echo '<a href="'.$cat->term_id.'" class="change-research" data-slug="'.$cat->slug.'">'.$cat->name.'</a>';
                        }
                    }
                ?>
            </p>
            <?php
                if($terms_research_area){
                    $i = 0;
                    foreach ($terms_research_area as $key => $cat) {
                        $i++;
                        $classActive = $i == 1 ? 'is--active' : null;
                        echo '<p class="des_research_area '.$classActive.'" id="'.$cat->slug.'">'.$cat->description.'</p>';
                    }
                }
            ?>
        </div>
    </div>
</div>

<div class="research-blog publication news">
    <div class="section_02">
        <div class="tw-container">
            <div class="tw-content">
                <div class="category_link">
                    <a href="<?php bloginfo('url');_e('/publications', 'corex'); ?>" class="is--active"><?php _e('Publications', 'corex'); ?></a>
                    <a href="<?php bloginfo('url');_e('/blog', 'corex'); ?>"><?php _e('Blog', 'corex'); ?></a>
                </div>

                <div class="form_search three_input">
                    <form action="#" id="frmPublication">
                    <div class="all-feild">
                            <div class="field-item --input">
                                <label for="searchtxt"><?php _e('Search', 'corex'); ?></label>
                                <div class="_input">
                                    <img src="<?php echo THEME_ASSETS . '/images/common/search.png'; ?>" alt="">
                                    <input type="text" id="searchtxt" name="searchtxt" placeholder="<?php _e('Search', 'corex'); ?>">
                                </div>
                            </div>
                            <div class="field-item --select">
                                <label for="research_area"><?php _e('Research Area', 'corex'); ?></label>
                                <div class="_input">
                                    <select name="research_area" id="research_area">
                                        <option value="0"><?php _e('All', 'corex'); ?></option>
                                        <?php if($terms_research_area){
                                            foreach ($terms_research_area as $key => $cat) {
                                                echo '<option value="'.$cat->term_id.'">'.$cat->name.'</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="field-item --select">
                                <label for="years"><?php _e('Years', 'corex'); ?></label>
                                <div class="_input">
                                    <select name="years" id="years">
                                        <option value="0"><?php _e('All', 'corex'); ?></option>
                                        <?php
                                            $_now = getdate();
                                            $_year = intval($_now['year']);
                                            for ($i = $_year; $i >= 1990 ; $i--) {
                                                echo '<option value="'.$i.'">'.$i.'</option>';
                                            };
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="preloader_member pubs">
                    <div class="loading_animation">
                        <?php include_once THEME_DIR . '/assets/images/preloader.svg' ?>
                    </div>
                    <div class="the_content" id="loading_document_ajax">
                        <div class="_list_news">
                            <?php
                                $publications = get_field('publications', 'option');
                                $total = count($publications);
                                $current = 18; //18
                                $pageNumber = ceil($total/$current);
                                if($publications){
                                    $i = 0;
                                    foreach ($publications as $publication) {
                                        $i++;
                                        $year_conference = $publication['year_conference'] ? '('.$publication['year_conference'].')' : null;
                                        $addfile = $publication['document'];
                                        $hyperlink = $publication['hyperlink'];
                                        $document = $addfile ? $addfile : $hyperlink;
                                        echo '<div class="__item">
                                                <div class="--description">
                                                    <div class="--title_doc">
                                                        <p>'.$publication['title'].'</p>
                                                        <img src="'. THEME_ASSETS . '/images/common/doc.png'.'" alt="">
                                                    </div>
                                                    <div class="--member">
                                                        '.$publication['author'].' <span>'.$year_conference.'</span>
                                                    </div>
                                                </div>
                                                <div class="--control">
                                                    <a href="'.$document.'" download><img src="'. THEME_ASSETS . '/images/common/download.png'.'" alt=""><span>'.__('Download', 'corex').'</span></a>
                                                    <a href="'.$publication['copy_bibtex'].'" target="_blank" copy-bibtex="'.$publication['copy_bibtex'].'" class="copy-bibtex"><img src="'. THEME_ASSETS . '/images/common/copy.png'.'" alt=""><span>'.__('Copy Bibtex', 'corex').'</span></a>
                                                </div>
                                            </div>';
                                        if($i === $current){
                                            break;
                                        }
                                    }
                                }
                            ?>
                        </div>
                        <div class="pagination-common js_panigator">
                            <div class="paginate_links">
                                <?php
                                    if($pageNumber > 1){
                                        for ($i = 1; $i <= $pageNumber; $i++) { 
                                            if($i === 1){
                                                echo '<span class="page-numbers current">'.$i.'</span>';
                                            }else{
                                                echo '<a class="page-numbers" href="'.get_bloginfo('url').'/'.__('publication', 'corex').'/page/'.$i.'/">'.$i.'</a>';
                                            }
                                        }
                                        echo '<a class="next page-numbers" href="'.get_bloginfo('url').'/'.__('publication', 'corex').'/page/2/">&gt;</a>';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form>
    <?php wp_nonce_field( 'ajax_loading_publication', 'publication_nonce' ); ?>
</form>

<script>
jQuery(document).ready(function($){
    const contentPubEl = $('#loading_document_ajax');
    const searchtxt = $('#searchtxt');
    const researchArea = $('#research_area');
    const years = $('#years');
    contentPubEl.on('click', '.copy-bibtex', function(e){
        e.preventDefault();
        const bibtex = $(this).attr('copy-bibtex');
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(bibtex).select();
        document.execCommand("copy");
        $temp.remove();
    })

    $('#frmPublication').on('submit', function(e){
        e.preventDefault();
    });

    $('.change-research').on('click', function(e){
        e.preventDefault();
        const value = $(this).attr('href');
        const dataSlug = $(this).attr('data-slug');
        const desTermEL = $('#'+dataSlug+'');
        desTermEL.siblings('.des_research_area').removeClass('is--active');
        desTermEL.addClass('is--active');
        researchArea.val(value);
        setTimeout(() => {
            const _searchtxt = searchtxt.val();
            const _researchArea = researchArea.val();
            const _years = years.val();
            searchBlogsFunc(_searchtxt, _researchArea, _years);
        }, 0);
    });

    $('#searchtxt').keyup(function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _searchtxt = $(this).val();
            const _researchArea = researchArea.val();
            const _years = years.val();
            searchBlogsFunc(_searchtxt, _researchArea, _years);
        }, 800);
    } );

    researchArea.on('change', function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _searchtxt = searchtxt.val();
            const _researchArea = $(this).val();
            const _years = years.val();
            searchBlogsFunc(_searchtxt, _researchArea, _years);
        }, 800);
    });

    years.on('change', function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _searchtxt = searchtxt.val();
            const _researchArea = researchArea.val();
            const _years = $(this).val();
            searchBlogsFunc(_searchtxt, _researchArea, _years);
        }, 800);
    });

    function searchBlogsFunc(searchtxt = "", researchArea = 0, years = 0){
        const nonce = $('#publication_nonce').val();
        const current = 18; //18
        $.ajax({
                type: "post",
                dataType: "JSON",
                url: obj.AJAX_URL,
                data: {
                    action: "ajax_search_pubs",
                    searchtxt: searchtxt,
                    researchArea: researchArea,
                    years: years,
                    current: current,
                    nonce: nonce,
                },
                beforeSend: () => {
                    $('.loading_animation').addClass('is--active');
                },
                complete: () => {
                    $('.loading_animation').removeClass('is--active');
                },
                success: function(response) {
                    contentPubEl.empty();
                    contentPubEl.append($(response.data));
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
                }
            });
    }

    contentPubEl.on('click', '.js_panigator .paginate_links a', function(e){
        e.preventDefault();
        const nonce = $('#publication_nonce').val();
        const _searchtxt = searchtxt.val();
        const _researchArea = researchArea.val();
        const _years = years.val();
        var hrefThis = $(this).attr('href');
        const current = 18; //18
        var page = hrefThis.match(/\/\d+\//)[0];
        page = page.match(/\d+/)[0];
        if(!page) page = 1;
        $.ajax({
            type: "post",
            dataType: "JSON",
            url: obj.AJAX_URL,
            data: {
                action: "ajax_load_pubs",
                paged: page,
                current: current,
                searchtxt: _searchtxt,
                researchArea: _researchArea,
                years: _years,
                nonce: nonce,
            },
            beforeSend: () => {
                $('.loading_animation').addClass('is--active');
            },
            complete: () => {
                setTimeout(() => {
                    $('.loading_animation').removeClass('is--active');
                }, 500);
            },
            success: function(response) {
                if(response.success){
                    setTimeout(() => {
                        contentPubEl.empty();
                        contentPubEl.append($(response.data));
                    }, 300);
                }else{
                    console.log(response.data);
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
            }
        });
    });
})
</script>
<?php get_footer(); ?>