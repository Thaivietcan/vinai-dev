<?php
//Template name: Applied AI
get_header();

$applied_description = get_field('applied_description', 'option');
$applied_product_description = get_field('applied_product_description', 'option');
$applied_products = get_field('applied_products', 'option');
$core_technologies = get_field('core_technologies', 'option');
$applied_product_image = get_field('applied_product_image', 'option') ? get_field('applied_product_image', 'option') : NOT_IMAGE;
$core_technologies = get_field('core_technologies', 'option');
$products_solutions = get_field('products_solutions', 'option');
$customer_cases = get_field('customer_cases', 'option');
$business_contact = get_field('business_contact', 'option');
$support_contact = get_field('support_contact', 'option');
$email_all = get_field('email_ft', 'option');
$email_us = $email_all ? 'mailto:'.$email_all[0]['email_address'] : 'javascript:void(0);';
?>
<div class="banner-research pdB108">
    <div class="--bg" style="background-image: url('<?php echo THEME_ASSETS .'/images/applied-ai/banner.png'; ?>');">
    </div>
    <div class="tw-container">
        <div class="tw-content">
            <h1><?php _e('Applied ai', 'corex'); ?></h1>
            <p><?php echo $applied_description; ?></p>
        </div>
    </div>
</div>

<div class="applied-ai">

    <div class="section_01_update" id="<?php _e('core-technologies', 'corex'); ?>">
        <div class="tw-container">
            <div class="tw-content">
                <h2 class="tw-title"><?php _e('Core Technologies', 'corex'); ?></h2>
                <div class="_grid_core">
                    <?php
                        if(!empty($core_technologies)){
                            foreach ($core_technologies as $core) {
                                echo '<div class="__item">
                                            <h4>'.$core['title'].'</h4>
                                            <p class="--image">
                                                <img src="'.$core['icon'].'" alt="">
                                            </p>
                                            <p>'.$core['description'].'</p>
                                        </div>';
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section_02_update" id="<?php _e('products-solutions', 'corex'); ?>">
        <div class="tw-container">
            <div class="tw-content">
                <h2 class="tw-title"><?php _e('Products & Solutions', 'corex'); ?></h2>
                <div class="_grid_product">
                    <?php
                        if(!empty($products_solutions)){
                            foreach ($products_solutions as $product) {
                                echo '<div class="__item">
                                        <div class="--icon">
                                            <img src="'.$product['icon'].'" alt="">
                                        </div>
                                        <p>'.$product['title'].'</p>
                                    </div>';
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section_04" id="<?php _e('customer-cases', 'corex'); ?>">
        <div class="tw-container">
            <div class="tw-content">
                <h2 class="tw-title"><?php _e('Customer Cases', 'corex'); ?></h2>
                <div class="_slide js-applied-ai">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php
                                if(!empty($customer_cases)){
                                    foreach ($customer_cases as $customer) {
                                        echo '<div class="swiper-slide">
                                                <a href="javascript:void(0);" class="__image"
                                                    style="background-image:url('.$customer['image'].');">
                                                </a>
                                                <div class="__txt">
                                                    <a href="javascript:void(0);">
                                                        <h4>'.$customer['title'].'</h4>
                                                    </a>
                                                </div>
                                                <a href="javascript:void(0);" class="__dot"><span></span><span></span><span></span></a>
                                                <a href="javascript:void(0);" class="__des">
                                                    <h3>'.$customer['title'].'</h3>
                                                    <p>'.$customer['description'].'</p>
                                                </a>
                                            </div>';
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_05">
        <div class="bg" style="background-image:url('<?php echo THEME_ASSETS .'/images/applied-ai/s5_bg.png'; ?>');"></div>
        <div class="tw-container">
            <div class="tw-content">
                <h2><?php _e('Contact Us', 'corex'); ?></h2>
                <p><span><?php _e('Business Contact', 'corex'); ?>: </span><a href="<?php echo $business_contact ? 'mailto:'.$business_contact : 'javascript:void(0);' ?>"><?php echo $business_contact; ?></a></p>
                <p><span><?php _e('Support Contact', 'corex'); ?>: </span><a href="<?php echo $support_contact ? 'mailto:'.$support_contact : 'javascript:void(0);' ?>"><?php echo $support_contact; ?></a></p>
                <a href="<?php echo $email_us; ?>" class="tw-button-common">
					<span><?php _e('Email Us', 'corex'); ?></span>
					<img src="<?php echo THEME_ASSETS . '/images/common/right_a.png' ?>" alt="">
				</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.js-in-click').on('click', function(e){
            e.preventDefault();
        });
        var swiper_s4_home = new Swiper('.js-applied-ai .swiper-container', {
            slidesPerView: 3,
            spaceBetween: 32,
            breakpoints: {
                1200: {
                    slidesPerView: 3,
                    spaceBetween: 22
                },
                1024: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
                480: {
                    slidesPerView: 1,
                    spaceBetween: 30
                }
            }
        });
    });
</script>
<?php get_footer(); ?>