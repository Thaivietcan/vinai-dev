<?php
//Template name: Publications
get_header();
$taxonomy = 'research-area';
$terms_research_area = get_terms([
    'taxonomy'   => $taxonomy,
    'hide_empty' => false,
    'orderby'       => 'id', 
    'order'         => 'ASC',
]);
$publications_post = new WP_Query([
    'post_type'      => 'publications',
    'paged'          => 1,
    'posts_per_page' => 12,
    'post_status'    => 'publish',
]);
?>

<div class="research-page">
	<div class="banner-research">
		<section class="banner_page hero-background" id="h-section1">
			<div class="tw-container">
					<div class="tw-content d-flex justify-content-between flex-direction-colum">
						<div class="research-title">
							<h1 class="tw-title"><?php _e('Research', 'corex'); ?></h1>
							<p class="title_soldier"><?php _e('Explore our research across:', 'corex'); ?></p>
							<p class="_soldier">
								<?php
									if($terms_research_area){
										foreach ($terms_research_area as $key => $cat) {
											?>
                                                <a href="<?php echo $cat->slug; ?>" class="btn-view-detail change-research" data-slug="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?> <img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt=""></a>
                                            <?php
										}
									}
								?>
							</p>
						</div>
						<div class="research-description w-100">
							<?php
								if($terms_research_area){
									$i = 0;
									foreach ($terms_research_area as $key => $cat) {
										$i++;
										$classActive = $i == 1 ? 'is--active' : null;
										echo '<p class="des_research_area '.$classActive.'" id="'.$cat->slug.'">'.$cat->description.'</p>';
									}
								}
							?>
						</div>
					</div>
			</div>
			<div class="shape-one"></div>
			<div class="shape-two"></div>
		</section>
	</div>
	<div class="research-blog publication">
    <div class="section_02">
        <div class="tw-container">
            <div class="tw-content">
                <div class="category_link d-flex"> 
                    <a href="<?php bloginfo('url');_e('/publications', 'corex'); ?>" class="is--active"><?php _e('Publications', 'corex'); ?></a>
                    <a href="<?php bloginfo('url');_e('/technical-blog', 'corex'); ?>"><?php _e('Blog', 'corex'); ?></a>
                </div>

                <div class="form_search three_input">
                    <form action="#" id="frmPublication">
                    <div class="all-feild d-flex align-items-center justify-content-between flex-direction-colum w-100">
                            <div class="all-feild-select d-flex w-100">
								<div class="field-item --select">
									<label for="research_area"><?php _e('Research field', 'corex'); ?></label>
									<div class="_input">
										<select name="research_area" id="research_area">
											<option value="0"><?php _e('All research fields', 'corex'); ?></option>
											<?php if($terms_research_area){
												foreach ($terms_research_area as $key => $cat) {
													echo '<option value="'.$cat->slug.'">'.$cat->name.'</option>';
												}
											} ?>
										</select>
									</div>
								</div>
								<div class="field-item --select">
									<label for="years"><?php _e('Year', 'corex'); ?></label>
									<div class="_input">
										<select name="years" id="years">
											<option value="0"><?php _e('All years', 'corex'); ?></option>
											<?php
												$_now = getdate();
												$_year = intval($_now['year']);
												for ($i = $_year; $i >= 1990 ; $i--) {
													echo '<option value="'.$i.'">'.$i.'</option>';
												};
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="field-item --input ">
                                <div class="_input">
                                    <input type="text" id="searchtxt" name="searchtxt" placeholder="<?php _e('Search', 'corex'); ?>">
									<i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="preloader_member pubs">
                    <div class="loading_animation">
                        <?php include_once THEME_DIR . '/assets/images/preloader.svg' ?>
                    </div>
                    <div class="the_content" id="loading_document_ajax">
                        <div class="_list_news list-publications-global d-flex flex-wrap">
                            <?php
                                if($publications_post->have_posts(  )){
                                    while ($publications_post->have_posts(  )) {
                                        $publications_post->the_post(  );
                                        $post_id = get_the_ID();
                                        $title = get_the_title($post_id);
										$category = get_the_terms( $post_id, 'research-area' );
                                        $author = get_field('authors', $post_id);
                                        $conference_name = get_field('conference_name', $post_id);
                                        $pdf = get_field('pdf', $post_id);
                                        $permalink = get_the_permalink($post_id);
                                        $is_hover_download = $pdf ? 'class="is_hover"' : null;
                                        ?>
											<div class="__item">
                                                <div class="--description">
                                                    <div class="--title_doc">
														<span class="sub-heading">
															<?php 
																foreach ( $category as $cat){
																   echo $cat->name;
																}
															?>
														</span>
                                                        <a href="<?php echo $permalink; ?>">
															<h4>
																<?php echo $title; ?>
															</h4>
														</a>
                                                    </div>
                                                    <div class="--member">
														<img src="<?php echo THEME_ASSETS . '/images/user-icon.svg' ?>" />
														<?php 
															if ($author) {
																echo $author;
															} 
														?>
														<span style="color: #000">(<?php echo $conference_name; ?>)</span>
                                                    </div>
                                                </div>
                                                <div class="--control d-flex justify-content-between align-items-center">
													<a href="<?php echo $permalink; ?>" class="btn btn-view-detail d-flex align-items-center">
														<span><?php _e('View detail', 'corex') ?></span>
														<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>">
													</a>
                                                    <a class="--control_download" href="<?php echo $pdf; ?>" <?php echo $is_hover_download; ?> target="_blank"><i class="fas fa-arrow-down"></i></a>
                                                </div>
                                            </div>
										<?php
                                    } wp_reset_postdata();
                                }
                            ?>
                        </div>
                        <div class="pagination-common js_panigator">
                            <?php wp_navigation_paged_ajax($publications_post); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<form>
    <?php wp_nonce_field( 'ajax_loading_publication', 'publication_nonce' ); ?>
</form>

<script>
jQuery(document).ready(function($){
    const contentPubEl = $('#loading_document_ajax');
    const searchtxt = $('#searchtxt');
    const researchArea = $('#research_area');
    const years = $('#years');
    const nonceValue = $('#publication_nonce').val();
    $('#frmSearchBlogs').on('submit', function(e){
        e.preventDefault();
    })

    $('.change-research').on('click', function(e){
        e.preventDefault();
        const value = $(this).attr('href');
        const desTermEL = $('#'+value+'');
        desTermEL.siblings('.des_research_area').removeClass('is--active');
        desTermEL.addClass('is--active');
        researchArea.val(value);
        setTimeout(() => {
            const _searchtxt = searchtxt.val();
            const _researchArea = researchArea.val();
            const _years = years.val();
            const _slug = contentPubEl.attr('data-cat');
            searchBlogsFunc(_searchtxt, _researchArea, _years, _slug);
        }, 0);
    });

    $('#searchtxt').keyup(function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _searchtxt = $(this).val();
            const _researchArea = researchArea.val();
            const _years = years.val();
            searchBlogsFunc(_searchtxt, _researchArea, _years);
        }, 800);
    } );

    researchArea.on('change', function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _searchtxt = searchtxt.val();
            const _researchArea = $(this).val();
            const _years = years.val();
            searchBlogsFunc(_searchtxt, _researchArea, _years);
        }, 0);
    });

    years.on('change', function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _searchtxt = searchtxt.val();
            const _researchArea = researchArea.val();
            const _years = $(this).val();
            searchBlogsFunc(_searchtxt, _researchArea, _years);
        }, 0);
    });

    function searchBlogsFunc(searchtxt="", researchArea=0, years=0){
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: obj.AJAX_URL,
            data: {
                action: "ajax_search_load_publications",
                searchtxt: searchtxt,
                researchArea: researchArea,
                year: years,
                nonce: nonceValue,
            },
            beforeSend: () => {
                $('.loading_animation').addClass('is--active');
            },
            complete: () => {
                setTimeout(() => {
                    $('.loading_animation').removeClass('is--active');
                }, 500);
            },
            success: (response) => {
                setTimeout(() => {
                    contentPubEl.empty();
                    contentPubEl.append($(response.data));
                }, 300);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
            }
        });
    };

    contentPubEl.on('click', '.js_panigator .paginate_links a', function(e){
        e.preventDefault();
        const _searchtxt = searchtxt.val();
        const _researchArea = researchArea.val();
        const _years = years.val();
        var hrefThis = $(this).attr('href');
        var page = hrefThis.match(/\/\d+\//)[0];
        page = page.match(/\d+/)[0];
        if(!page) page = 1;
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: obj.AJAX_URL,
            data: {
                action: "ajax_navigation_load_publications",
                searchtxt: _searchtxt,
                researchArea: _researchArea,
                year: _years,
                paged: page,
                nonce: nonceValue,
            },
            beforeSend: () => {
                $('.loading_animation').addClass('is--active');
            },
            complete: () => {
                setTimeout(() => {
                    $('.loading_animation').removeClass('is--active');
                }, 500);
            },
            success: (response) => {
                setTimeout(() => {
                    contentPubEl.empty();
                    contentPubEl.append($(response.data));
                }, 300);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
            }
        });
    });
})
</script>

 <script type="text/javascript">
      const particleWave = () => {
        const heroBanner = document.querySelector(".hero-background");
        let w = heroBanner.clientWidth;
        let h = heroBanner.clientHeight;
        const dpr = window.devicePixelRatio;

        const fov = 60;
        const fovRad = fov * (Math.PI / 180);
        const dist = h / 2 / Math.tan(fovRad);

        const clock = new THREE.Clock();
        const pointSize = 5 * dpr;

        const renderer = new THREE.WebGLRenderer({ alpha: true });
        renderer.setSize(w, h);
        renderer.setClearColor(0x000000, 0);
        renderer.setPixelRatio(dpr);

        const container = document.getElementById("webgl-canvas");
        container.appendChild(renderer.domElement);

        const camera = new THREE.PerspectiveCamera(fov, w / h, 1, dist * 8);
        camera.position.x = 0;
        camera.position.y = 20;
        camera.position.z = 150;

        const scene = new THREE.Scene();

        // TODO: This fog isn't working for some reason
        const fogColor = "#000"; // white
        const near = 1.8;
        const far = 2;
        scene.fog = new THREE.Fog(fogColor, near, far);

        const geo = new THREE.BufferGeometry();
        const positions = [];

        const width = 250 * (w / h);
        const depth = 100;
        const distance = 5;

        for (let x = 0; x < width; x += distance) {
          for (let z = 0; z < depth; z += distance) {
            positions.push(-width / 2 + x, -30, -depth / 2 + z);
          }
        }
        const positionAttribute = new THREE.Float32BufferAttribute(
          positions,
          3
        );
        geo.setAttribute("position", positionAttribute);

        const mat = new THREE.ShaderMaterial({
          uniforms: {
            u_time: {
              value: 0.0,
            },

            color1: {
              value: new THREE.Color("#fff"),
            },

            color2: {
              value: new THREE.Color("#fff"),
            },

            color3: {
              value: new THREE.Color("#fff"),
            },

            color4: {
              value: new THREE.Color("#fff"),
            },

            color5: {
              value: new THREE.Color("#fff"),
            },

            resolution: {
              type: "v2",
              value: new THREE.Vector2(w * dpr, h * dpr),
            },
            pointSize: { value: pointSize },
          },

          vertexShader: `
        precision highp float;
        #define M_PI 3.1415926535897932384626433832795
        
        uniform float u_time;
        uniform float pointSize;
        
        void main() {
          vec3 p = position;
          p.y += (
             cos(p.x / M_PI * 8.0 + u_time * 0.5) * 15.0 +
             sin(p.z / M_PI * 8.0 + u_time * 0.5) * 15.0 + 
             60.0
           ) ;
          
          gl_PointSize = pointSize;
          gl_Position = projectionMatrix * modelViewMatrix * vec4(p, 1.0);
        }   
    
    `,
          fragmentShader: `
        precision highp float;
        
        uniform vec3 color1;
        uniform vec3 color2;
        uniform vec3 color3;
        uniform vec3 color4;
        uniform vec3 color5;
        uniform vec2 resolution;
        
        void main() {
          // create circles instead of squares
          if ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.475 ) discard;
          
          float x = gl_FragCoord.x;
          float step1 = 0.25;
          float step2 = 0.45;
          float step3 = 0.55;
          float step4 = 0.75;
          float step5 = 1.00;
          
          float mixValue = x / resolution.x;
          
          // create color stops using 'mix', and convert the progress through each 'step'
          // to a value between 0 and 1
          vec3 mixedColor;
          if(mixValue < step1) {
            mixedColor = mix(color1, color2, mixValue / step1);
          } else if (mixValue >= step1 && mixValue < step2) {
            mixedColor = mix(color2, color3, ((mixValue - step1) / (step2 - step1)));
          } else if (mixValue >= step2 && mixValue < step3) {
            mixedColor = color3;
          } else if (mixValue >= step3 && mixValue < step4) {
            mixedColor = mix(color3, color4, ((mixValue - step3) / (step4 - step3)));
          } else {
            mixedColor = mix(color4, color5, ((mixValue - step4) / (step5 - step4)));
          }
          
          gl_FragColor = vec4(mixedColor, 1.0);
        }
    `,
        });

        const mesh = new THREE.Points(geo, mat);
        scene.add(mesh);

        function render() {
          const time = clock.getElapsedTime();
          mesh.material.uniforms.u_time.value = time;
          renderer.render(scene, camera);
          requestAnimationFrame(render);
        }
        render();

        function onWindowResize() {
          w = heroBanner.clientWidth;
          h = heroBanner.clientHeight;
          camera.aspect = w / h;
          camera.updateProjectionMatrix();
          renderer.setSize(w, h);
        }
        window.addEventListener("resize", onWindowResize);
      };

      window.addEventListener("load", particleWave);
    </script>
<?php get_footer(); ?>