<?php 
//Template name: Workshops
get_header();

$seminar_title = get_field('seminar_content_title', 'option');
$seminar_description = get_field('seminar_content_description', 'option');

$_currentTax = get_queried_object();
$_taxonomy = $_currentTax->taxonomy;
$_slug = $_currentTax->slug;
$_id = $_currentTax->term_id;
$_name = $_currentTax->name;
$upcoming_workshop_id = [];

$publications_post = new WP_Query([
    'post_type'      => 'workshop',
    'paged'          => 1,
    'posts_per_page' => 8,
    'post_status'    => 'publish',
]);

?>
<div class="seminar-page workshop-page">
	<div class="banner_page section_01">
        <div class="tw-container">
            <div class="tw-content d-flex justify-content-between flex-direction-colum">
                <div class="title_seminar">
					<h1 class="tw-title" data-aos="fade-up" data-aos-delay="300"><?php echo $seminar_title; ?></h1>
				</div>
				<div class="description_seminar w-100" data-aos="fade-up" data-aos-delay="350">
					<?php echo $seminar_description; ?>
				</div>
            </div>
        </div>
		<div class="shape-one"></div>
		<div class="shape-two"></div>
    </div>
	<div class="tab-seminar">
		<div class="tw-container">
			<div class="tw-content">
				<div class="category_link d-flex">
                    <a href="<?php bloginfo('url');_e('/seminar-series/', 'corex'); ?>" ><?php _e('Seminar series', 'corex'); ?></a>
                    <a href="<?php bloginfo('url');_e('/workshop', 'corex'); ?>" class="is--active"><?php _e('Workshop', 'corex'); ?></a>
                </div>
			</div>
		</div>
	</div>
	<div class="page-content">
        <div class="section_02 upcoming_speakers">
            <div class="tw-container">
                <div class="tw-content">
                    <h2 class="tw-title" data-aos="fade-up" data-aos-delay="300">Upcoming workshops</h2>
                    <div class="grid-speakers">
                        <?php
                            $upcoming_wsp_posts = fn_get_posts('workshops', 1, -1, [
                                'meta_query'      => array(
                                    array(
                                        'key'     => 'upcoming_workshops',
                                        'value'   => 'yes',
                                        'compare' => 'LIKE'
                                    )
                                )
                            ]);
                            if($upcoming_wsp_posts->have_posts(  ))
                            {
                                while ($upcoming_wsp_posts->have_posts(  )) {
                                    $upcoming_wsp_posts->the_post(  );
                                    $wsp_post_id = get_the_ID();
                                    $thumbnail = get_the_post_thumbnail_url($post_id, 'full') ?: null;
                                    $wsp_post_title = get_the_title($wsp_post_id);
                                    $dateObject = new DateTime(get_field('time_takes_place', $wsp_post_id));
                                    $timeStamp = $dateObject->getTimestamp();
                                    $wsp_post_timing = get_field('time_takes_place', $wsp_post_id) ? date('l, M d Y - h:i a', $timeStamp).' (GMT + 7)' : null;
                                    $wsp_post_permalink = get_the_permalink($wsp_post_id);
                                    array_push($upcoming_workshop_id, $wsp_post_id);
                                    ?>
										<div class="speaker-item" data-aos="fade-up" data-aos-delay="300">
                                            <div class="avatar">
                                                <a href="<?php echo $wsp_post_permalink; ?>"><img src="<?php echo $thumbnail; ?>" alt="<?php echo $wsp_post_title; ?>"></a>
                                            </div>
                                            <div class="info">
                                                <a href="<?php echo $wsp_post_permalink; ?>">
													<h3 class="name">
														<?php 
															if (strlen($wsp_post_title) > 55) {
																echo substr($wsp_post_title, 0, 55) . '...';
															} else {
																echo $wsp_post_title;
															} 
														?>
													</h3>
												</a>
                                                <p class="time_takes_place"><i class="fas fa-clock"></i> <?php echo $wsp_post_timing; ?></p>
                                            </div>
                                        </div>
									<?php
                                } wp_reset_postdata();
                            }
							else {
								echo "<p>Stay tuned to get inspired, informed, and motivated by our leading experts & notable speakers.</p>";
							}
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_03 previous_speakers">
            <div class="tw-container">
                <div class="tw-content">
                    <h2 class="tw-title" data-aos="fade-up" data-aos-delay="300">Previous workshops</h2>
                    <div class="preloader_member pubs"> 
                        <div class="loading_animation">
                            <?php include_once THEME_DIR . '/assets/images/preloader.svg' ?>
                        </div>
                        <div class="the_content" id="loading_seminar_ajax">
                            <div class="grid-speakers">
                                <?php
                                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                                    $previous_wsp_post = fn_get_posts('workshop', $paged, 8, [
                                        'post__not_in' => $upcoming_workshop_id,
                                    ]);
                                    if($previous_wsp_post->have_posts()){
                                        while ($previous_wsp_post->have_posts()) {
                                            $previous_wsp_post->the_post();
                                            $post_id = get_the_ID();
                                            $post_thumb = get_the_post_thumbnail_url($post_id, 'full') ?: null;
                                            $post_title = get_the_title($post_id);
                                            $dateObject = new DateTime(get_field('time_takes_place', $post_id));
                                            $timeStamp = $dateObject->getTimestamp();
                                            $post_timing = get_field('time_takes_place', $post_id) ? date('l, M d Y - h:i a', $timeStamp).' (GMT + 7)' : null;
                                            $post_permalink = get_the_permalink($post_id);
                                            ?>
												<div class="speaker-item" data-aos="fade-up" data-aos-delay="300">
                                                    <div class="avatar">
                                                        <a href="<?php echo $post_permalink; ?>"><img src="<?php echo $post_thumb; ?>" alt="<?php echo $post_title; ?>"></a>
                                                    </div>
                                                    <div class="info">
                                                        <a href="<?php echo $post_permalink; ?>">
															<h3 class="name">
																<?php 
																	if (strlen($post_title) > 35) {
																		echo substr($post_title, 0, 35) . '...';
																	} else {
																		echo $post_title;
																	} 
																?>
															</h3>
														</a>
                                                        <p class="time_takes_place"><i class="fas fa-clock"></i> <?php echo $post_timing; ?></p>
                                                    </div>
                                                </div>
											<?php
                                        }wp_reset_postdata();
                                    }
                                ?>
                            </div>
                            <div class="pagination-common js_panigator text-align-center">
                                <?php wp_navigation_paged_ajax($seminar_post); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>