<?php
    //Template Name: Achievements
    get_header();
    $category_slug_archivements = "achievements-blog";
?>
<div class="news achivements-page">
    <div class="banner_page section_01">
        <div class="tw-container">
            <div class="tw-content">
                <h2><?php _e('Achievements', 'corex'); ?></h2> 
            </div>
        </div>
    </div>

    <div class="section_02">
        <div class="tw-container">
            <div class="tw-content">
                <div class="category-item" id="vinai-news">
                    <div class="box-content-in-animation preloader_member">
                        <div class="loading_animation">
                            <?php include THEME_DIR . '/assets/images/preloader.svg' ?>
                        </div>
                        <div class="the_content" cat-slug="<?php echo $category_slug_archivements ?>" id="vinai-achievement">
                            <div class="loading_animation">
                                <?php include_once THEME_DIR . '/assets/images/preloader.svg' ?>
                            </div>
                            <div class="_list_news list-publications-global d-flex flex-wrap">
                                <?php
                                    $vinai_achi = new WP_Query([
                                        'post_type'      => 'post',
                                        'category_name'  => $category_slug_archivements,
                                        'paged'          => 1,
                                        'posts_per_page' => 12,
                                        'post_status'    => 'publish',
                                    ]);
                                    if($vinai_achi->have_posts()){
                                        while ($vinai_achi->have_posts()) {
                                            $vinai_achi->the_post();
                                            $post_id = get_the_ID();
                                            $permalink = get_the_permalink($post_id);
                                            $thumbnail = get_the_post_thumbnail_url($post_id, 'full') ?: NOT_IMAGE;
                                            $title = get_the_title($post_id);
											$category = get_the_category($post_id);
											//$category = get_the_terms( $post_id, 'research-area' );
											$theExcerpt = get_the_excerpt($post_id);
                                            $day = get_the_time('d/m/Y', $post_id);
                                            ?>
												<div class="__item">
													<a href="<?php echo $permalink ?>" class="thumbnail">
														<div class="--bg"
															style="background-image: url(<?php echo $thumbnail ?>)">
														</div>
													</a>
													<div class="text">
														<span class="sub-heading">
															<?php echo $category[0]->name; ?>
														</span>
														<a href="<?php echo $permalink ?>" class="--title">
															<h4><?php if (strlen($title) > 40) {
																	echo substr($title, 0, 40) . '...';
																} else {
																	echo $title;
																} ?></h4>
														</a>
														<p>
															<?php
																if (strlen($theExcerpt) > 100) {
																	echo substr($theExcerpt, 0, 100) . '(...)';
																} else {
																	echo $theExcerpt;
																}
															?>
														</p>
												
														<div class="--meta d-flex align-items-center justify-content-between">
															<a href="<?php echo $permalink ?>" class="btn btn-view-detail">
																<span><?php _e('View details', 'corex'); ?></span>
																<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt="<?php echo $title; ?>">
															</a>
															<p class="--time"><?php echo $day; ?></p>
														</div>
													</div>
												</div>
											<?php
                                        }
                                    }
                                ?>
                            </div>
                            <div class="pagination-common js_panigator text-align-center">
                                <?php wp_navigation_paged_ajax($vinai_achi); ?>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<form>
    <?php wp_nonce_field( 'ajax_loading_post', 'post_nonce' ); ?>
</form>

<script>

    jQuery(document).ready(function($){
        const vinaiAchi = $('#vinai-achievement');
        const nonceValue = $('#post_nonce').val();
        vinaiAchi.on('click', '.js_panigator .paginate_links a', function(e){
            e.preventDefault();
            perPage = 12;
            let hrefThis = $(this).attr('href');
            let page = hrefThis.match(/\/\d+\//)[0];
            page = page.match(/\d+/)[0];
            if(!page) page = 1;
            navigationLoadPost(vinaiAchi, page, perPage);
        });

        function navigationLoadPost(thisEl, paged, perPage){
            const slug = thisEl.attr('cat-slug');
            jQuery.ajax({
                type: "POST",
                dataType: "JSON",
                url: obj.AJAX_URL,
                data: {
                    action: "ajax_navigation_load_post_achievement",
                    slug: slug,
                    perPage: perPage,
                    paged: paged,
                    nonce: nonceValue,
                },
                beforeSend: () => {
                    thisEl.prev('.loading_animation').addClass('is--active');
                },
                complete: () => {
                    setTimeout(() => {
                        thisEl.prev('.loading_animation').removeClass('is--active');
                    }, 500);
                },
                success: (response) => {
                    setTimeout(() => {
                        thisEl.empty();
                        thisEl.append($(response.data));
                    }, 300);
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
                }
            });
        }
    })
</script>
<?php get_footer(); ?>