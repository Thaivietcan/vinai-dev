<?php
    //Template Name: AI Technology
    get_header();
?>
<div class="ai-technology-page">
    <div class="banner">
        <div class="swiper-container" id="banner-ai-technology">
            <div class="swiper-wrapper">
                <?php
                    $banner = get_field('ai_technology_banner', 'option');
                    if(!empty($banner) && count($banner) > 0){
                        foreach ($banner as $b) {
                            echo '<div class="swiper-slide">
                                    <video src="'.$b['file'].'" autoplay loop muted></video>
                                </div>';
                        }
                    }
                ?>
            </div>
            <div class="swiper-pagination ai-pagination"></div>
            <div class="swiper-button-next ai-btn-next"><img src="<?php echo THEME_ASSETS . '/images/common/right.svg' ?>" alt="right"></div>
            <div class="swiper-button-prev ai-btn-prev"><img src="<?php echo THEME_ASSETS . '/images/common/left.svg' ?>" alt="left"></div>
        </div>
    </div>

    <div class="ai-autonomous"  id="ai-autonomous">
        <div class="tw-container">
            <div class="tw-content">
                <h2 class="title"><?php _e('AI Autonomous', 'corex') ?><br><?php _e('Technology', 'corex') ?></h2>
                <div class="the-content">
                    <?php the_field('ai_autonomous_technology', 'option') ?>
                </div>
            </div>
        </div>
        <div class="decor-left">
            <img src="<?php echo THEME_ASSETS . '/images/common/bg-ai-l.svg' ?>" alt="AI Technology">
        </div>
        <div class="decor-top">
            <img src="<?php echo THEME_ASSETS . '/images/common/bg-ai-t.svg' ?>" alt="AI Technology">
        </div>
        <div class="decor-rb">
            <img src="<?php echo THEME_ASSETS . '/images/common/bg-ai-rb.svg' ?>" alt="AI Technology">
        </div>
    </div>

    <div class="ai-autonomous-sub">
        <img src="<?php the_field('ai_autonomous_picture', 'option') ?>" alt="AI Technology">
        <div class="tw-container">
            <div class="tw-content">
                <div class="sticky">
                    <div class="scroll-auto">
                        <?php the_field('ai_autonomous_description', 'option') ?>
                    </div>
                    <div class="car"><img src="<?php echo THEME_ASSETS . '/images/common/car.svg' ?>" alt="car"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="voice-language-personal-assistant" id="voice-language-personal-assistant">
        <div class="tw-container">
            <div class="tw-content">
                <h2 class="title"><?php _e('Voice, Language and', 'corex') ?><br><?php _e('Personal Assistant', 'corex') ?></h2>
                <div class="the-content">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php
                                $voice = get_field('voice_language_and_personal_assistant', 'option');
                                if(!empty($voice) && count($voice) > 0){
                                    foreach ($voice as $value) {
                                        echo '<div class="swiper-slide ai-card">
                                                <h3 class="title">'.$value['title'].'</h3>
                                                <div class="img">
                                                    <img src="'.$value['icon'].'" alt="'.$value['title'].'">
                                                </div>
                                                <div class="description">
                                                    <div class="scroll-auto">'.$value['description'].'</div>
                                                </div>
                                            </div>';
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <div class="swiper-pagination ai-pagination blue"></div>
                </div>
            </div>
        </div>
        <div class="decor-kc">
            <img src="<?php echo THEME_ASSETS . '/images/common/kc1.svg' ?>" alt="AI Technology">
        </div>
    </div>

    <div class="ai-edge-intelligence" id="ai-edge-intelligence">
        <div class="tw-container">
            <div class="tw-content">
                <h2 class="title"><?php _e('AI Edge Intelligence', 'corex') ?></h2>
                <div class="grid-edge">
                    <?php
                        $edge = get_field('ai_edge_intelligence', 'option');
                        if(!empty($edge) && count($edge) > 0){
                            foreach ($edge as $e) {
                                echo '<div class="item ai-card">
                                        <h3 class="title">'.$e['title'].'</h3>
                                        <div class="img">
                                            <img src="'.$e['icon'].'" alt="'.$e['title'].'">
                                        </div>
                                        <div class="description">
                                            <div class="scroll-auto">'.$e['description'].'</div>
                                        </div>
                                    </div>';
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="decor-1">
            <img src="<?php echo THEME_ASSETS . '/images/common/kc2.svg' ?>" alt="AI Technology">
        </div>
        <div class="decor-2">
            <img src="<?php echo THEME_ASSETS . '/images/common/kc3.svg' ?>" alt="AI Technology">
        </div>
    </div>
</div>
<script>
    var bannerAI = new Swiper('#banner-ai-technology.swiper-container', {
        speed: 1200,
        effect: 'fade',
        autoplay: {
            delay: 3000
        },
        loop: true,
        pagination: {
            el: '#banner-ai-technology .swiper-pagination',
        },
        navigation: {
            nextEl: '#banner-ai-technology .swiper-button-next',
            prevEl: '#banner-ai-technology .swiper-button-prev',
        },
    });

    var voiceAI = new Swiper('#voice-language-personal-assistant .swiper-container', {
        speed: 800,
        slidesPerView: 4,
        spaceBetween: 20,
        loop: true,
        // autoplay: {
        //     delay: 3000
        // },
        pagination: {
            el: '#voice-language-personal-assistant .swiper-pagination',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 15
            }
        }
    });
</script>
<?php get_footer() ?>