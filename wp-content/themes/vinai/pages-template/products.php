<?php
//Template Name: Product
get_header();
$product_sub_title = get_field('product_sub_title', 'option');
$product_title = get_field('product_title', 'option');
$product_image = get_field('product_image', 'option');

$products_list = get_field('products_list', 'option');

$logo_partners = get_field('home_logo_partners', 'option');
$title_partner = get_field('home_logo_partners_title', 'option');
 ?>
 <div class="product-page">
    <section class="h-section1" id="h-section1">
        <div class="banner_site" style="background-image: url('<?php echo $product_image; ?>')"></div>
        <div class="content_banner_site">
            <div class="tw-container">
                <div class="tw-content w-100">
                    <p class="sub-heading" data-aos="fade-up"><?php echo $product_sub_title; ?></p>
                    <h2 data-aos="fade-up"><?php echo $product_title; ?></h2>
                </div>
            </div>
        </div>
    </section>
	<section class="section-2">
        <div class="tw-container">
			<div class="tw-content">
				<?php if(!empty($products_list)) : ?>
					<?php if( have_rows('products_list', 'option') ): ?>
						<?php $i = 1; ?>
						<?php while( have_rows('products_list', 'option') ): the_row(); 
							$title = get_sub_field('product_section_title');
							$sub_title = get_sub_field('product_section_sub');
							$content = get_sub_field('product_section_content');
							$sub_title = get_sub_field('product_section_sub');
							$rearmore = get_sub_field('product_section_read_more');
							$image = get_sub_field('product_section_image');
							?>
							<div class="product-detail product-detail-<?php echo $i; ?> d-flex align-items-center justify-content-between flex-direction-colum">
								<div class="_left __left_<?php echo $i; ?> w-100"  data-aos="fade-up">
									<h2><?php echo $title; ?></h2>
									<h4><?php echo $sub_title; ?></h4>
									<div class="desc">
										<?php echo $content; ?>
									</div>
									<div class="readmore-product">
										<?php if(!empty($rearmore['title_main'])) : ?>
											<span class="title_main_btn"><?php echo $rearmore['title_main']; ?></span>
										<?php endif; ?>
										<?php if(!empty($rearmore['text_button_1'])) : ?>
											<a href="<?php echo $rearmore['link_button_1']; ?>" class="btn btn-view-detail btn-view-detail-home">
												<span><?php echo $rearmore['text_button_1']; ?></span>
												<img src="/wp-content/uploads/2021/11/icon-right.svg" alt="">
											</a>
										<?php endif; ?>
										<?php if(!empty($rearmore['text_button_2'])) : ?>
											<a href="<?php echo $rearmore['link_button_2']; ?>" class="btn btn-view-detail btn-view-detail-home">
												<span><?php echo $rearmore['text_button_2']; ?></span>
												<img src="/wp-content/uploads/2021/11/icon-right.svg" alt="">
											</a>
										<?php endif; ?>
									</div>
								</div>
								<div class="_right __right_<?php echo $i; ?> w-100"  data-aos="fade-up">
									<img src="<?php echo $image; ?>" />
								</div>
							</div>
							<?php $i++; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
    </section>
	<section class="h-section6" id="h-section6" style="background: #000;">
    <div class="tw-container">
        <div class="tw-content">
            <div class="">
                <h2 class="tw-title"><?php echo $title_partner; ?></h2> 
                <div class="list-logo-partners d-flex align-items-center justify-content-center pc" data-aos="fade-up" data-aos-anchor="#h-section6">
                    <?php 
                        if(!empty($logo_partners)) {
                            foreach($logo_partners as $logos) {
                                ?>
                                    <div class="_img">
                                        <img src="<?php echo $logos; ?>" alt=""> 
                                    </div>
                                <?php
                            }
                        }
                    ?>
                </div>
				<!--Mobile-->
				<div class="list-logo-partners mb">
					<div class="swiper swiper-container swiper-logo-partner">
					  <div class="swiper-wrapper grid-container">
						<?php 
							if(!empty($logo_partners)) {
								foreach($logo_partners as $logos) {
									?>
										<div class="swiper-slide">
											<div class="_img">
												<img src="<?php echo $logos; ?>" alt=""> 
											</div>
										</div>
										
									<?php
								}
							}
						?>
					  </div>
					  <div class="swiper-pagination-product text-align-center"></div>
					</div>
				</div>
            </div>
        </div>
    </div>
</section>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {
    var product_logo = new Swiper(".swiper-logo-partner", {
        slidesPerView: 2,
        slidesPerColumn: 5,
		spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination-product",
          clickable: true,
        },
    });
});
</script>
 <?php get_footer(); ?>