<?php
// Template name: Seminar single
get_header();
?>
<div class="seminar-page-single">
    <div class="tw-container">
        <div class="tw-content">
            <div class="featured_content">
                <h1 class="tw-title">Title: Network regression and its inference</h1>
                <p class="work_place">Speaker: Can Le, Department of Statistics at the University of California, Davis</p>
                <p class="timing">Date/Time: Friday, Nov 13th 2020 - 10.00 AM (GMT+7)</p>
                <p class="feature_image">
                    <img src="<?php echo THEME_ASSETS .'/images/seminar/featured.jpg'; ?>" alt="title">
                </p>
                <p class="video_recording">
                    <span>Video recording: </span><a href="#">https://youtu.be/yiOvq8PCGmw</a>
                </p>
            </div>
            <div class="the_content">
                <h2><strong>ABOUT SPEAKER:</strong></h2>
                <p>Dr Can - an Assistant Professor in the Department of Statistics at the University of California, Davis. He received his PhD in statistics from the University of Michigan, Ann Arbor. His main research interest is statistical network analysis.</p>
                <br>
                <h2><strong>ABSTRACT:</strong></h2>
                <p>Network data, representing interactions and relationships between units, have become ubiquitous with the rapid development of science and technology. Analyzing such complex and structurally novel data requires new ideas and tools beyond the scope of classical statistics. In this talk, we will assume that a standard response-predictors data set is available, and observations are connected by a network. We will introduce a new framework for estimating and making inference of regression coefficients with network effects. Inference requirements in the presence of observational errors related to the network structure will also be discussed.</p>
            </div>
            <div class="upcoming_speakers">
                <h2 class="tw-title">Upcoming Speakers</h2>
                <div class="grid-speakers">
                        <div class="speaker-item">
                            <div class="avatar">
                                <img src="<?php echo NOT_AVATAR; ?>" alt="title">
                            </div>
                            <div class="info">
                                <h3 class="name">Nguyen Van Ba</h3>
                                <p class="work_place">University of Toronto</p>
                                <h3 class="title-post">Title: Building Blocks of Generalizable Autonomy</h3>
                                <p class="time_takes_place">Date/Time: Friday, Mar 19th 2021 - 10.00 AM (GMT+7)</p>
                            </div>
                        </div>
                        <div class="speaker-item">
                            <div class="avatar">
                                <img src="<?php echo NOT_AVATAR; ?>" alt="title">
                            </div>
                            <div class="info">
                                <h3 class="name">Nguyen Van Ba</h3>
                                <p class="work_place">University of Toronto</p>
                                <h3 class="title-post">Title: Building Blocks of Generalizable Autonomy</h3>
                                <p class="time_takes_place">Date/Time: Friday, Mar 19th 2021 - 10.00 AM (GMT+7)</p>
                            </div>
                        </div>
                        <div class="speaker-item">
                            <div class="avatar">
                                <img src="<?php echo NOT_AVATAR; ?>" alt="title">
                            </div>
                            <div class="info">
                                <h3 class="name">Nguyen Van Ba</h3>
                                <p class="work_place">University of Toronto</p>
                                <h3 class="title-post">Title: Building Blocks of Generalizable Autonomy</h3>
                                <p class="time_takes_place">Date/Time: Friday, Mar 19th 2021 - 10.00 AM (GMT+7)</p>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>