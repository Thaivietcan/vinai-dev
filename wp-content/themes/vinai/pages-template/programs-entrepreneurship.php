<?php
//Template name: Entrepreneurship
get_header();
$entre_we_title = get_field('entre_we_title', 'option');
$entre_we_description = get_field('entre_we_description', 'option');
$work_environment = get_field('entre_we_work_en', 'option');
$description_vinai = get_field('description_vinai', 'option');
$Who = get_field('entre_who', 'option');
$what_do = get_field('entre_what_do', 'option');
$what_we = get_field('entre_what_we', 'option');
$journey_description = get_field('journey_description', 'option');
$journey_rounds = get_field('journey_rounds', 'option');
$our_startup_list = get_field('our_startup_list', 'option');
$entrepreneurship_banner = get_field('entrepreneurship_banner', 'option') ? get_field('entrepreneurship_banner', 'option'): NOT_IMAGE;
$our_start_hide = get_field('our_start_hide', 'option');
?>
<div class="entrepreneurship">
    <div class="section_01" style="background-image: url('<?php echo $entrepreneurship_banner; ?>');">
        <div class="tw-container">
            <div class="tw-content">
                <h1>- <?php _e('Entrepreneurship', 'corex'); ?> <span><?php _e('Program', 'corex'); ?></span></h1>
            </div>
        </div>
    </div>

    <div class="section_02">
        <div class="tw-container">
            <div class="tw-content">
                <div class="_left">
                    <p class="__title"><?php echo $entre_we_title; ?></p>
                    <p><?php echo $entre_we_description; ?></p>
                    <a target="_block" href="https://forms.gle/nvyQLzUJwkVAtASh6" class="tw-button-common">
                        <span><?php _e('Apply Now', 'corex'); ?></span>
                        <img src="<?php echo THEME_ASSETS .'/images/common/right.png'; ?>" alt="">
                    </a>
                </div>
                <div class="_right">
                    <?php
                        if($work_environment){
                            foreach ($work_environment as $key => $work) {
                                echo '<div class="__item">
                                        <p><img src="'.$work['icon'].'" alt=""></p>
                                        <p>'.$work['excrept'].'</p>
                                    </div>';
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section_03">
        <div class="tw-container">
            <div class="tw-content">
                <?php echo $description_vinai; ?>
            </div>
        </div>
    </div>

    <div class="section_04">
        <div class="tw-container">
            <div class="tw-content">
                <div class="__item">
                    <p class="--title">
                        <?php _e('Who are', 'corex'); ?><br><?php _e('we looking for', 'corex'); ?>?
                    </p>
                    <div class="--content">
                        <?php echo $Who; ?>
                    </div>
                </div>
                <div class="__item">
                    <p class="--title">
                        <?php _e('What do we expect', 'corex'); ?><br><?php _e('from a startup', 'corex'); ?>?
                    </p>
                    <div class="--content">
                        <?php echo $what_do; ?>
                    </div>
                </div>
                <div class="__item">
                    <p class="--title">
                        <?php _e('What we do', 'corex'); ?>
                    </p>
                    <div class="--content">
                        <?php echo $what_we; ?>
                    </div>
                    <div class="--dot">
                        <img src="<?php echo THEME_ASSETS .'/images/entre/dot.png'; ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_05">
        <div class="tw-container">
            <div class="tw-content">
                <h2 class="tw-title"><?php _e('Startup Journey', 'corex'); ?></h2>
                <p><?php echo $journey_description; ?></p>
                <a target="_block" href="https://forms.gle/nvyQLzUJwkVAtASh6" class="tw-button-common">
                    <span><?php _e('Apply Now', 'corex'); ?></span>
                    <img src="<?php echo THEME_ASSETS .'/images/common/right.png'; ?>"
                        alt="">
                </a>

                <div class="rounds">
                    <?php if($journey_rounds){
                        $i = 0;
                        foreach ($journey_rounds as $key => $round) {
                            $i++;
                            $classActive = $i == 1 ? 'is--active' : null ;
                            echo '<div class="round-item '.$classActive.'" data-round="round-'.$i.'">
                                    <div class="__title">
                                        <p>'.$round['title'].'</p>
                                        <p>'.$round['label'].'</p>
                                    </div>
                                    <span class="__line"></span>
                                    <div class="__dots">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>';
                        }
                    } ?>
                </div>

                <div class="round-content">
                    <?php if($journey_rounds){
                        $i = 0;
                        foreach ($journey_rounds as $key => $round) {
                            $i++;
                            $classActive = $i == 1 ? 'is--active' : null ;
                            $roundContent = $round['content'];
                        ?>
                            <div class="round_content_item <?php echo $classActive; ?>" id="round-<?php echo $i; ?>">
                                <div class="border_top"></div>
                                <div class="border_bottom"></div>
                                <div class="_hr">
                                    <p><?php _e('Milestone', 'corex') ?></p>
                                    <p><?php _e('Activies', 'corex') ?></p>
                                </div>
                                <?php if($roundContent){
                                    foreach ($roundContent as $key => $content) {
                                        echo '<div class="_tr">
                                                <p class="--title">'.$content['milestone'].'</p>
                                                <div class="__content">
                                                    <p>
                                                        '. $content['activies'] .'
                                                    </p>
                                                </div>
                                            </div>';
                                    }
                                } ?>
                            </div>
                        <?php
                        }
                    } ?>
                </div>
            </div>
        </div>
    </div>

    <?php
        if(empty($our_start_hide)){
    ?>
        <div class="section_06">
            <div class="tw-container">
                <div class="tw-content">
                    <h2 class="tw-title">
                        <?php _e('Our startup', 'corex'); ?>
                    </h2>
                    <div class="_startup_list">
                        <?php if($our_startup_list){
                            foreach ($our_startup_list as $key => $our_startup) {
                                echo '<div class="__item">
                                        <div class="--img">
                                            <img src="'.$our_startup['our_icon'].'" alt="">
                                        </div>
                                        <p class="--title">'.$our_startup['our_title'].'</p>
                                        <p>'.$our_startup['our_description'].'</p>
                                        <a href="#" class="--explore-more">'.__('Explore More', 'corex').'</a>
                                    </div>';
                            }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<script>
    jQuery(document).ready(function($){
        $('[data-round]').on('click', function(e){
            const dataRoundEl = $(this);
            const data_round = dataRoundEl.attr('data-round');
            const dataContentEl = $('#'+data_round+'');
            dataRoundEl.siblings('.round-item').removeClass('is--active');
            dataRoundEl.addClass('is--active');
            dataContentEl.siblings('.round_content_item').removeClass('is--active');
            dataContentEl.addClass('is--active');
        });
    });
</script>
<?php get_footer(); ?>