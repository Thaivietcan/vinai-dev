<?php
//Template name: AI Residency new
get_header();

$title_banner = get_field('residency_title_banner', 'option');
$description_banner = get_field('banner_description_air', 'option');
$link_apply = get_field('banner_apply_now_air', 'option');

$url_video = get_field('residency_video_url', 'option');
$img_video = get_field('residency_image_video', 'option');

$icon_research_residents = get_field('icon_research_residents_air', 'option');
$title_research_residents = get_field('title_research_residents_air', 'option');
$content_research = get_field('des_research_residents_air', 'option');
$title_benefits_research = get_field('title_benefits_research', 'option');
$benefit_research = get_field('benefits_research_residents_air', 'option');
$title_requirements_research = get_field('title_requirements_research', 'option');
$require_research = get_field('requirements_research_residents_air', 'option');

$icon_engineering = get_field('icon_engineering', 'option');
$title_engineering_residents = get_field('title_engineering_residents', 'option');
$content_engineer = get_field('des_engineering_residents_air', 'option');
$title_benefit_engineering = get_field('title_benefit_engineering', 'option');
$benefit_engineer = get_field('benefits_engineering_residents_air', 'option');
$title_requirement_engineering = get_field('title_requirement_engineering', 'option');
$require_engineer = get_field('requirements_engineering_residents_air', 'option');

$title_achievements = get_field('title_achievements', 'option');
$read_more_achievement = get_field('read_more_achievement', 'option');
$post_achive = get_field('post_content_achievements_air', 'option');
$button_job = get_field('explore_jobs_achievements_air', 'option');

$title_alumni = get_field('title_alumni', 'option');
$list_network = get_field('residency_list_network', 'option');

$title_faqs = get_field('title_faqs', 'option');
$list_faq = get_field('questions_answers', 'option');

$category_slug_achievements = "achievements-blog";
?>
<div class="ai-residency">
    <section class="h-section1">
		<div class="banner_site">
			<div class="tw-container">
				<div class="tw-content text-align-center ">
					<h1 data-aos="fade-up"><?php echo $title_banner; ?></h1>
					<p data-aos="fade-up"><?php echo $description_banner; ?></p>
                    <a href="<?php echo $link_apply; ?>" class="tw-button-common btn-dark" data-aos="fade-up" target="_blank">
                        <span><?php _e('Apply now', 'corex'); ?></span>
                    </a>
				</div>
			</div>
			<div class="shape-one"></div>
			<div class="shape-two"></div>
		</div>
	</section>

    <div class="section-video">
		<div class="tw-container">
            <div class="tw-content">
				<div class="card-video" data-aos="fade-up">
				  <iframe src="<?php echo $url_video; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>	
	</div>

    <div class="section-2">
        <div class="tw-container">
            <div class="tw-content">
                <div class="research-track research-engineer-track d-flex justify-content-between flex-direction-colum">
                    <div class="title-research w-100">
                        <img data-aos="fade-up" src="<?php echo $icon_research_residents; ?>" alt="<?php _e('Research Track', 'corex') ?>">
                        <h2 class="tw-title" data-aos="fade-up"><?php echo $title_research_residents; ?></h2>
                        <?php echo $content_research; ?> 
                    </div>
                    <div class="benefit-require w-100" >
                        <div class="benefit-research benefit-require-detail" data-aos="fade-up">
                        <h3><?php echo $title_benefit_engineering; ?></h3>
                        <p><?php echo $benefit_research; ?></p>
                        </div>
                        <div class="require-research benefit-require-detail" data-aos="fade-up">
                            <h3><?php echo $title_requirement_engineering; ?></h3>
                            <p><?php echo $require_research; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-3">
        <div class="tw-container">
            <div class="tw-content">
                <div class="research-track research-engineer-track d-flex justify-content-between flex-direction-colum">
                    <div class="title-research w-100" id="mobile" data-aos="fade-up">
                        <img src="<?php echo $icon_engineering; ?>" alt="<?php _e('Engineer Track', 'corex') ?>">
                        <h2 class="tw-title"><?php echo $title_engineering_residents; ?></h2>
                        <?php echo $content_engineer; ?> 
                    </div>
                    <div class="benefit-require w-100" data-aos="fade-up">
                        <div class="benefit-research benefit-require-detail">
                        <h3><?php echo $title_benefit_engineering; ?></h3>
                        <p><?php echo $benefit_engineer; ?></p>
                        </div>
                        <div class="require-research benefit-require-detail">
                            <h3><?php echo $title_requirement_engineering; ?></h3>
                            <p><?php echo $require_engineer; ?></p>
                        </div>
                    </div>
                    <div class="title-research w-100" id="desktop" data-aos="fade-up">
                        <img src="<?php echo $icon_engineering; ?>" alt="<?php _e('Engineer Track', 'corex') ?>">
                        <h2 class="tw-title"><?php echo $title_engineering_residents; ?></h2>
                        <?php echo $content_engineer; ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-4 news">
        <div class="tw-container">
            <div class="tw-content">
                <div class="achievements">
                    <div class="achievement-inner d-flex align-items-center justify-content-between">
                        <h2 class="tw-title" data-aos="fade-up"><?php echo $title_achievements; ?></h2>
                        <a href="<?php echo $read_more_achievement['link']; ?>" class="btn btn-view-detail" data-aos="fade-up">
                            <span><?php echo $read_more_achievement['text']; ?></span>
                            <img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt="">
                        </a>
                    </div>
                    <div class="_list_news list-publications-global d-flex flex-wrap" data-aos="fade-up">
                        <?php
                            $vinai_news = new WP_Query([
                                'post_type'      => 'post',
                                'category_name'  => $category_slug_achievements,
                                'paged'          => 1,
                                'posts_per_page' => 3,
                                'post_status'    => 'publish',
                            ]);
                            if($vinai_news->have_posts()){
                                while ($vinai_news->have_posts()) {
                                    $vinai_news->the_post();
                                    $post_id = get_the_ID();
                                    $permalink = get_the_permalink($post_id);
                                    $thumbnail = get_the_post_thumbnail_url($post_id, 'full') ?: NOT_IMAGE;
                                    $title = get_the_title($post_id);
                                    $category = get_the_category( $post_id);
                                    //$category = get_the_terms( $post_id, 'research-area' );
                                    $theExcerpt = get_the_excerpt($post_id);
                                    $day = get_the_time('d/m/Y', $post_id);
                                    ?>
                                        <div class="__item">
                                            <a href="<?php echo $permalink ?>" class="thumbnail">
                                                <div class="--bg"
                                                    style="background-image: url(<?php echo $thumbnail ?>)">
                                                </div>
                                            </a>
                                            <div class="text">
                                                <span class="sub-heading">
													<?php echo $category[0]->name; ?>
                                                    
                                                </span>
                                                <a href="<?php echo $permalink ?>" class="--title">
                                                    <h4><?php if (strlen($title) > 40) {
                                                            echo substr($title, 0, 40) . '...';
                                                        } else {
                                                            echo $title;
                                                        } ?></h4>
                                                </a>
                                                <p>
                                                    <?php 
                                                        if (strlen($theExcerpt) > 100) {
                                                            echo substr($theExcerpt, 0, 100) . '(...)';
                                                        } else {
                                                            echo $theExcerpt;
                                                        }
                                                    ?>
                                                </p>
                                                
                                                <div class="--meta d-flex align-items-center justify-content-between">
                                                    <a href="<?php echo $permalink ?>" class="btn btn-view-detail">
                                                        <span><?php _e('View details', 'corex'); ?></span>
                                                        <img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt="">
                                                    </a>
                                                    <p class="--time"><?php echo $day; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-5 alumni_network">
        <div class="tw-container">
            <div class="tw-content  d-flex justify-content-between">
                <div class="alumni_network_inner" data-aos="fade-up">
                    <span class="sub-heading"><?php echo $title_alumni; ?></span>
                </div>
                <div class="alumni_network_list">
                    <?php 
                        foreach($list_network as $lists) {
                            ?>
                                <div class="list-member-inner" data-aos="fade-up">
                                    <img src="<?php echo $lists['residency_avatar']; ?>" />
                                    <h4><?php echo $lists['residency_title']; ?></h4>
                                    <p><?php echo $lists['residency_sub_title']; ?></p>
                                </div>
                            <?php 
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section-6 faqs">
        <div class="tw-container">
            <div class="tw-content d-flex justify-content-between">
                <div class="alumni_network_inner">
                    <span class="sub-heading" data-aos="fade-up"><?php echo $title_faqs; ?></span>
                </div>
                <div class="alumni_network_list d-flex justify-content-between flex-wrap">
                    <ul class="accordion">
                        <?php 
                            foreach($list_faq as $faqs) {
                                ?>
                                    <li class="accordion-chevron" data-aos="fade-up">
                                        <a class="accordion-toggle" href="#">
										<i class="fas fa-question-circle"></i>
										<p><?php echo $faqs['questions'] ?></p>
										<i class="fa fa-chevron-down" id="icon-faqs"></i></a>
                                        <div class="accordion-inner">
                                            <?php echo $faqs['answers'] ?>
                                        </div>
                                    </li>
                                <?php 
                            }
                        ?>
                        
                    </ul>
                   
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($){
        $('.accordion-toggle').click(function(e) {
            e.preventDefault();  
            $("a:first-of-type").removeClass('chev');
            
            var $this = $(this);
            if ($this.next().hasClass('show')) {
                $this.next().removeClass('show');
                $this.next().slideUp(350);
            } else {
                $this.parent().parent().find('li .accordion-inner').removeClass('show');
                $this.parent().parent().find('li .accordion-inner').slideUp(350);
                $this.toggleClass('chev');
                $this.next().toggleClass('show');
                $this.next().slideToggle(350);
            }
        });
    });
</script>
<?php get_footer(); ?>