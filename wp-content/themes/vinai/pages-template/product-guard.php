<?php
// Template name: Product Guard
get_header();
$title_guard = get_field('product_guard_title', 'option');
$sub_guard = get_field('product_guard_sub', 'option');
$content_guard = get_field('product_guard_content', 'option');

$tab_guard = get_field('product_guard_tab', 'option');

$section_guard_title = get_field('g_section_guard_title', 'option');
$section_guard_sub = get_field('g_section_guard_sub', 'option');
$section_guard_image = get_field('g_section_guard_image', 'option');

$title_main = get_field('g_section_features_title_main', 'option');
$main_list = get_field('g_section_features_main_list', 'option');
$title_advanced = get_field('g_section_features_title_advanced', 'option');
$advanced_list = get_field('g_section_features_advanced_list', 'option');

$why_title = get_field('g_section_why_title', 'option');
$why_list = get_field('g_section_why_list', 'option');

?>
<div class="products product-driver product-guard">
	<div class="banner_page section_01">
        <div class="tw-container">
            <div class="tw-content">
                <div class="_text w-100">
					<span class="sub-heading" data-aos="fade-up"><?php echo $sub_guard; ?></span>
					<h2 class="tw-title" data-aos="fade-up"><?php echo $title_guard; ?></h2>
					<p data-aos="fade-up"><?php echo $content_guard; ?></p>
				</div>
            </div>
        </div>
		<div class="shape-one"></div>
		<div class="shape-two"></div>
    </div>
	<div class="tab-seminar tab_products">
		<div class="tw-container">
			<div class="tw-content pc">
				<div class="category_link d-flex">
					<?php if(!empty($tab_guard)) : ?>
						<?php if( have_rows('product_guard_tab', 'option') ): ?>
						<?php $i = 1; ?>
						<?php while( have_rows('product_guard_tab', 'option') ): the_row(); 
							$title = get_sub_field('text');
							?>
								<?php 
									if($i === 1) {
										?>
											<a href="#tab-<?php echo $i; ?>" class="is--active"><?php echo $title; ?></a>
										<?php
									}
									else {
										?>
											<a href="#tab-<?php echo $i; ?>"><?php echo $title; ?></a>
										<?php
									}
								?>
							<?php $i++; ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php endif; ?>
                </div>
			</div>
			<div class="tw-content mb">
				<div class="category_link">
					<div class="swiper swiper-container swiper-cate-link">
						<div class="swiper-wrapper">
								<?php if( have_rows('product_guard_tab', 'option') ): ?>
								<?php $i = 1; ?>
								<?php while( have_rows('product_guard_tab', 'option') ): the_row(); 
									$title = get_sub_field('text');
									?>
										<div class="swiper-slide">
											<?php 
												if($i === 1) {
													?>
														<a href="#tab-<?php echo $i; ?>" class="is--active"><?php echo $title; ?></a>
													<?php
												}
												else {
													?>
														<a href="#tab-<?php echo $i; ?>"><?php echo $title; ?></a>
													<?php
												}
											?>
										</div>
									<?php $i++; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section_02">
		<div class="tw-container">
            <div class="tw-content d-flex align-items-center justify-content-center flex-direction-colum">
                <div class="_left w-100">
					<p class="text_heading" data-aos="fade-up"><?php echo $section_guard_title; ?></p>
					<p class="text_sub" data-aos="fade-up"><?php echo $section_guard_sub; ?></p>
				</div>
				<div class="_right w-100">
					<img data-aos="fade-up" class="border-radius-10" src="<?php echo $section_guard_image; ?>" />
				</div>
            </div>
        </div>
	</div>
	<div class="section_03 page-section" id="tab-1">
		<div class="tw-container">
            <div class="tw-content d-flex justify-content-center flex-direction-colum">
                <div class="_left w-100" data-aos="fade-up">
					<h3><?php echo $title_main; ?></h3>
					<ul>
						<?php 
							foreach($main_list as $titles) {
								echo '<li class="">
										<h5>'.$titles['title'].'</h5>
										<span>'.$titles['content'].'</span>
									</li>';
							}
						?>
					</ul>
				</div>
				<div class="_right w-100" data-aos="fade-up">
					<h3><?php echo $title_advanced; ?></h3>
					<ul>
						<?php 
							foreach($advanced_list as $title) {
								echo '<li class="">
										<h5>'.$title['title'].'</h5>
										<span>'.$title['content'].'</span>
									</li>';
							}
						?>
					</ul>
				</div>
            </div>
        </div>
	</div>
	<div class="section_05 page-section" id="tab-2">
		<div class="tw-container">
            <div class="tw-content d-flex justify-content-center flex-direction-colum">
				<h2><?php echo $why_title; ?></h2>
				<div class="list-why-us w-100">
					<ul class="">
					<?php if(!empty($why_list)) : ?>
						<?php 
							foreach($why_list as $lists) {
								?>
									<li class="d-flex" data-aos="fade-up">
										<i class="fas fa-check"></i>
										<span><?php echo $lists['text']; ?></span>
									</li>
								<?php
							}
						?>
					<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>	
	</div>
</div>
<script>
    jQuery(document).ready(function($){
		var swiper_cate_link = new Swiper(".swiper-cate-link", {
			slidesPerView: 2,
			spaceBetween: 30,
      });

		// Tab
		$(".tab_products .category_link a").click(function () {
			$(".tab_products .category_link a").removeClass("is--active");
			$(this).addClass("is--active");   
		});
    })
</script>
<?php get_footer(); ?>