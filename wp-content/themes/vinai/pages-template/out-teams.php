<?php
    //Template Name: Our team
    get_header();
    $out_team_title = get_field('out_team_title', 'option');
    $out_team_description = get_field('out_team_description', 'option');
	
	$img_video = get_field('image_video_team', 'option');
	$video = get_field('video_our_team', 'option');
	
    $director_name = get_field('director_name', 'option');
    $director_location = get_field('director_location', 'option');
    $director_experience = get_field('director_experience', 'option');
    $director_story = get_field('director_story', 'option');
    $director_avatar = get_field('diretor_avatar', 'option');
	
	$title_leadership = get_field('about_employee_title', 'option');
	$employee_list = get_field('employee_list', 'option');
	
	$title_testimonial = get_field('testimonial_title', 'option');
	$testimonial_list = get_field('testimonial_list', 'option');
	
    $taxonomy = 'teams-category';
    $terms_teams_category = get_terms([
        'taxonomy'   => $taxonomy,
        'hide_empty' => false,
    ]);

    $our_team_banner = get_field('our_team_banner', 'option');
?>
<div class="out-teams">
    <section class="h-section1" id="h-section1">
		<div class="banner_site">
			<div class="tw-container">
				<div class="tw-content text-align-center ">
					<h1 data-aos="fade-up" data-aos-delay="300"><?php echo $out_team_title; ?></h1>
					<p data-aos="fade-up" data-aos-delay="300"><?php echo $out_team_description; ?></p>
				</div>
			</div>
			<div class="shape-one"></div>
			<div class="shape-two"></div>
		</div>
	</section>
	
	<div class="section-video">
		<div class="tw-container">
            <div class="tw-content">
				<div class="card-video" data-aos="fade-up" data-aos-delay="300">
					<iframe src="<?php echo $video; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>	
	</div>

    <div class="section_02">
        <div class="tw-container">
            <div class="tw-content">
                <div class="profile d-flex justify-content-between flex-direction-colum" id="<?php _e('key-members', 'corex'); ?>">
                    <div class="_avatar w-100">
                        <h2 class="tw-title" data-aos="fade-up" data-aos-delay="300"><?php echo $director_location; ?></h2>
                    </div>
                    <div class="_skill_story w-100">
                        <div class="__skill_inner d-flex flex-direction-colum">
							<div class="__skill_inner_main d-flex w-100">
								<p data-aos="fade-up" data-aos-delay="300" class="__skill" id="desktop"><?php echo $director_experience; ?></p>
								<h4 data-aos="fade-up" data-aos-delay="300" id="desktop"><?php echo $director_name; ?></h4>
							</div>
							<div class="__skill_avatar" data-aos="fade-up" data-aos-delay="300">
								<img src="<?php echo $director_avatar; ?>" />
							</div>
							<h4 id="mobile"><?php echo $director_name; ?></h4>
                            <p class="__skill" id="mobile"><?php echo $director_experience; ?></p>
						</div>
                        <div class="__story" data-aos="fade-up" data-aos-delay="300">
                            <?php echo $director_story; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="section_03" id="<?php _e('leadership', 'corex'); ?>">
		<div class="tw-container">
            <div class="tw-content">
				<h2 class="tw-title" data-aos="fade-up" data-aos-delay="300"><?php echo $title_leadership; ?></h2>
				<div class="list-member-leader">
					<div class="swiper swiper-leadership">
					  <div class="swiper-wrapper">
						<?php
							foreach($employee_list as $employees) {
								?>
									<div class="swiper-slide">
										<div class="list-member-inner" data-aos="fade-up" data-aos-delay="300">
											<img src="<?php echo $employees['employee_avatar']; ?>" />
											<h4><?php echo $employees['employee_name']; ?></h4>
											<p><?php echo $employees['employee_location']; ?></p>
										</div>
									</div>
								<?php
							}
						?>
					  </div>
					 
					</div>
				</div>
			</div>
		</div>	
	</div>
	
	<div class="section_04" id="<?php _e('testimonial', 'corex'); ?>">
		<div class="tw-container">
            <div class="tw-content d-flex justify-content-between flex-direction-colum">
				<div class="title_left"> 
					<h2 class="tw-title" data-aos="fade-up" data-aos-delay="300"><?php echo $title_testimonial; ?></h2>
				</div>
				<div class="content_right w-100">
					<div class="list-testimonial">
						<div class="swiper swiper-testimonial">
							<div class="swiper-wrapper">
								<?php foreach($testimonial_list as $testimonials) : ?>
									<div class="swiper-slide">
										<div class="list-member-inner" data-aos="fade-up" data-aos-delay="300">
											<p class="_content"><?php echo $testimonials['text_testimonial']; ?></p>
											<div class="_avatar d-flex align-items-center">
												<img src="<?php echo $testimonials['avatar_testimonial']; ?>" />
												<div class="__text">
													<h4><?php echo $testimonials['name_testimonial']; ?></h4>
													<p><?php echo $testimonials['job_testimonial']; ?></p>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ; ?>
							</div>
							<div class="swiper-pagination-testimonial"></div>
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function($){
		var swiper_leadership = new Swiper(".swiper-leadership", {
			slidesPerView: 4,
			spaceBetween: 30,
			loop: true,
			autoplay: true,
			pagination: {
				el: ".swiper-pagination-leadership",
				clickable: true,
			},
			breakpoints: {
            1024: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
      });
	  
	  var swiper_testimonial = new Swiper(".swiper-testimonial", {
			slidesPerView: 1,
			spaceBetween: 10,
			pagination: {
				el: ".swiper-pagination-testimonial",
				clickable: true,
			},
			breakpoints: {
            1024: {
                slidesPerView: 1,
            },
            768: {
                slidesPerView: 1,
            },
            480: {
                slidesPerView: 1,
            }
        }
      });
    });
</script>
<?php
    get_footer();
?>