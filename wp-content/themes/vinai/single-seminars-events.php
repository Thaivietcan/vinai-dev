<?php 
    get_header( );
    if(have_posts(  )) : while (have_posts(  )): the_post(  );
    $postID = get_the_ID();
    $thumbnailEvent = get_the_post_thumbnail_url($postID, 'fullsize') ? get_the_post_thumbnail_url($postID, 'fullsize') : NOT_IMAGE;
    $currentCat = get_the_category($postID);
    $title = get_the_title($postID);
    $location = get_field('event_address', $postID);
    $date = get_field('event_date', $postID);
    $date = date('d/m/Y', strtotime($date));
    $author = get_field('event_talker', $postID);
    $register = get_field('register_link', $postID);
    $content = get_the_content($postID);
    $timeNow = time();
    $eventID = array($postID);
    $postsEventAll = new WP_Query([
        'post_type'  => 'post',
        'cat' => $currentCat[0]->term_id,
        'posts_per_page' => -1,
        'post_status' => 'publish',
    ]);
    if($postsEventAll->have_posts(  )){
        while ($postsEventAll->have_posts(  )) {
            $postsEventAll->the_post(  );
            $_date = get_field('event_date', $_postID);
            $_time = strtotime(date($_date));
            $__postID = get_the_ID(  );
            if($timeNow > $_time){
                array_push($eventID, $__postID);
            }
        }
        wp_reset_postdata();
    }

    $postsEvent = new WP_Query([
        'post_type'  => 'post',
        'cat' => $currentCat[0]->term_id,
        'posts_per_page' => 6,
        'post__not_in' => $eventID,
        'post_status' => 'publish',
    ]);
?>
<div class="news event-detail">
    <div class="banner_page section_01" style="background-image: url('<?php echo $thumbnailEvent; ?>')">
        <div class="tw-container">
            <div class="tw-content">

            </div>
        </div>
    </div>
    <div class="title_single">
        <div class="tw-container">
            <div class="tw-content">
                <div class="--content">
                    <h1><?php echo $title; ?></h1>
                    <div class="_infomation">
                        <p class="--location"><span><img src="<?php echo THEME_ASSETS .'/images/common/location.png'; ?>" alt="location"></span><span><?php echo $location; ?></span></p>
                        <p class="--time"><span><img src="<?php echo THEME_ASSETS .'/images/common/calendar.png'; ?>" alt="time"></span><span><?php echo $date; ?></span></p>
                        <p class="--time"><span><img src="<?php echo THEME_ASSETS .'/images/common/people.png'; ?>" alt="location"></span><span><?php echo $author; ?></span></p>
                    </div>
                    <div class="_control">
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink($postID); ?>"><?php _e('Share Link', 'corex'); ?></a>
                        <a href="<?php echo $register ? $register : 'javascript:void(0)' ?>" <?php echo $register ? 'target="_blank"' : null ?> ><span><?php _e('Register', 'corex'); ?></span><img src="<?php echo THEME_ASSETS .'/images/common/right.png'; ?>" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section_02">
        <div class="tw-container">
            <div class="tw-content">
                <div class="the_content">
                    <div class="_detail">
                        <?php
                            if($content){
                                the_content();
                            }else{
                                echo '<p>'.__('Updating', 'corex').'...</p>';
                            }
                        ?>
                    </div>
                    <h2 class="tw-title"><?php _e('Upcoming Events', 'corex') ?></h2>
                    <div class="_list_news">
                        <?php
                        if($postsEvent->have_posts(  )): while ($postsEvent->have_posts(  )): $postsEvent->the_post(  );
                            $_postID = get_the_ID(  );
                            $_featuredImage = get_the_post_thumbnail_url($_postID) ?  get_the_post_thumbnail_url($_postID) : NOT_IMAGE;
                            $_title = get_the_title($_postID);
                            $_permalink = get_the_permalink($_postID);
                            $_location = get_field('event_address', $_postID);
                            $_date = get_field('event_date', $_postID);
                            $_date = date('d/m/Y', strtotime($_date));
                            $_author = get_field('event_talker', $_postID);
                        ?>
                            <div class="__item event_item">
                                <a href="<?php echo $_permalink; ?>" class="thumbnail">
                                    <div class="--bg"
                                        style="background-image: url('<?php echo $_featuredImage; ?>')">
                                    </div>
                                </a>
                                <div class="text">
                                    <a href="<?php echo $_permalink; ?>" class="--title">
                                        <h3><?php echo $_title; ?></h3>
                                    </a>
                                    <p class="--time --location"><span><img src="<?php echo THEME_ASSETS .'/images/common/location.png'; ?>" alt="location"></span><span> <?php echo $_location; ?></span></p>
                                    <p class="--time"><span><img src="<?php echo THEME_ASSETS .'/images/common/calendar.png'; ?>" alt="time"></span><span> <?php echo $_date; ?></span></p>
                                    <p class="--time"><span><img src="<?php echo THEME_ASSETS .'/images/common/people.png'; ?>" alt="location"></span><span> <?php echo $_author; ?></span></p>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata(  ); endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; wp_reset_postdata(  ); endif; ?>
<?php get_footer( ); ?>