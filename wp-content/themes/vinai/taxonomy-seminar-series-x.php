<?php
get_header();

$seminar_title = get_field('seminar_content_title', 'option');
$seminar_description = get_field('seminar_content_description', 'option');

$_currentTax = get_queried_object();
$_taxonomy = $_currentTax->taxonomy;
$_slug = $_currentTax->slug;
$_id = $_currentTax->term_id;
$_name = $_currentTax->name;

$upcoming_speaker_id = [];

$taxonomy = 'seminar-series';
$terms = get_terms( array(
    'taxonomy' => $taxonomy,
    'hide_empty' => false,
	'orderby'       => 'id', 
    'order'         => 'ASC',
));


$seminar_post = new WP_Query([
    'post_type'      => 'seminar',
    'paged'          => 1,
    'posts_per_page' => 8,
    'post_status'    => 'publish',
]);
?>
<div class="seminar-page">
    <div class="banner_page section_01">
        <div class="tw-container">
            <div class="tw-content d-flex justify-content-between flex-direction-colum">
                <div class="title_seminar">
					<h1 class="tw-title"><?php echo $seminar_title; ?></h1>
				</div>
				<div class="description_seminar w-100">
					<?php echo $seminar_description; ?>
				</div>
            </div>
        </div>
		<div class="shape-one"></div>
		<div class="shape-two"></div>
    </div>
	<div class="tab-seminar">
		<div class="tw-container">
			<div class="tw-content">
				<div class="category_link d-flex">
                    <a href="<?php bloginfo('url');_e('/seminar-series/research-seminar/', 'corex'); ?>" class="is--active"><?php _e('Seminar series', 'corex'); ?></a>
                    <a href="<?php bloginfo('url');_e('/workshop', 'corex'); ?>"><?php _e('Workshop', 'corex'); ?></a>
                </div>
			</div>
		</div>
	</div>
    <div class="page-content">
        <div class="section_02 upcoming_speakers">
            <div class="tw-container">
                <div class="tw-content">
                    <h2 class="tw-title">Upcoming Speakers</h2>
                    <div class="grid-speakers">
                        <?php
                            $upcoming_speaker_posts = fn_get_posts('seminar', 1, -1, [
                                'meta_query'      => array(
                                    array(
                                        'key'     => 'upcoming_speakers',
                                        'value'   => 'yes',
                                        'compare' => 'LIKE'
                                    )
                                )
                            ]);
                            if($upcoming_speaker_posts->have_posts(  ))
                            {
                                while ($upcoming_speaker_posts->have_posts(  )) {
                                    $upcoming_speaker_posts->the_post(  );
                                    $upcoming_speaker_post_id = get_the_ID();
                                    $upcoming_speaker_post_avatar = get_field('avatar', $upcoming_speaker_post_id) ?: NOT_AVATAR;
                                    $upcoming_speaker_post_name = get_field('name_speakers', $upcoming_speaker_post_id);
                                    $upcoming_speaker_post_work_place = get_field('work_place', $upcoming_speaker_post_id);
                                    $upcoming_speaker_post_title = get_the_title($upcoming_speaker_post_id);
                                    $dateObject = new DateTime(get_field('time_takes_place', $upcoming_speaker_post_id));
                                    $timeStamp = $dateObject->getTimestamp();
                                    $upcoming_speaker_post_timing = get_field('time_takes_place', $upcoming_speaker_post_id) ? date('l, M d Y - h:i a', $timeStamp).' (GMT + 7)' : null;
                                    $upcoming_speaker_post_permalink = get_the_permalink($upcoming_speaker_post_id);
                                    array_push($upcoming_speaker_id, $upcoming_speaker_post_id);
                                    ?>
										<div class="speaker-item">
                                            <div class="avatar">
                                                <a href="<?php echo $upcoming_speaker_post_permalink; ?>"><img src="<?php echo $upcoming_speaker_post_avatar; ?>" alt="<?php echo $upcoming_speaker_post_title; ?>"></a>
                                            </div>
                                            <div class="info">
                                                <a href="<?php echo $upcoming_speaker_post_permalink; ?>"><h3 class="name"><?php echo $upcoming_speaker_post_name; ?></h3></a>
                                                <p class="work_place"><?php echo $upcoming_speaker_post_work_place; ?></p>
                                                <p class="title-post">
												<i class="fas fa-microphone"></i>
													<?php 
														if (strlen($upcoming_speaker_post_title) > 60) {
															echo substr($upcoming_speaker_post_title, 0, 60) . '...';
														} else {
															echo $upcoming_speaker_post_title;
														} 
													?>
												</p>
                                                <p class="time_takes_place"><i class="fas fa-clock"></i> <?php echo $upcoming_speaker_post_timing; ?></p>
                                            </div>
                                        </div>
									<?php
                                } wp_reset_postdata();
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_03 previous_speakers">
            <div class="tw-container">
                <div class="tw-content">
                    <h2 class="tw-title">Previous speakers</h2>
					<div class="all-feild-select d-flex ">
						<div class="field-item --select">
							<div class="_input">
								<select name="research_seminar" id="research_seminar">
									<option value="0"><?php _e('All research fields', 'corex'); ?></option>
									<?php if($terms){
										foreach ($terms as $key => $cat) {
											echo '<option value="'.$cat->slug.'">'.$cat->name.'</option>';
										}
									} ?>
								</select>
							</div>
						</div>
					</div>
                    


                    <div class="preloader_member pubs"> 
                        <div class="loading_animation">
                            <?php include_once THEME_DIR . '/assets/images/preloader.svg' ?>
                        </div>
                        <div class="the_content" id="loading_seminar_ajax">
                            <div class="grid-speakers">
                                <?php
                                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                                    $seminar_post = fn_get_posts('seminar', $paged, 8, [
                                        $taxonomy      => $_slug,
                                        'post__not_in' => $upcoming_speaker_id,
                                    ]);
                                    if($seminar_post->have_posts()){
                                        while ($seminar_post->have_posts()) {
                                            $seminar_post->the_post();
                                            $post_id = get_the_ID();
                                            $post_avatar = get_field('avatar', $post_id) ?: NOT_AVATAR;
                                            $post_name = get_field('name_speakers', $post_id);
                                            $post_work_place = get_field('work_place', $post_id);
                                            $post_title = get_the_title($post_id);
                                            $dateObject = new DateTime(get_field('time_takes_place', $post_id));
                                            $timeStamp = $dateObject->getTimestamp();
                                            $post_timing = get_field('time_takes_place', $post_id) ? date('l, M d Y - h:i a', $timeStamp).' (GMT + 7)' : null;
                                            $post_permalink = get_the_permalink($post_id);
                                            ?>
												<div class="speaker-item">
                                                    <div class="avatar">
                                                        <a href="<?php echo $post_permalink; ?>"><img src="<?php echo $post_avatar; ?>" alt="<?php echo $post_title; ?>"></a>
                                                    </div>
                                                    <div class="info">
                                                        <a href="<?php echo $post_permalink; ?>"><h3 class="name"><?php echo $post_name; ?></h3></a>
                                                        <p class="work_place">
															<?php 
																if (strlen($post_work_place) > 50) {
																	echo substr($post_work_place, 0, 50) . '...';
																} else {
																	echo $post_work_place;
																} 
															?>
														</p>
                                                        <p class="title-post"><i class="fas fa-microphone"></i>
															<?php 
																if (strlen($post_title) > 90) {
																	echo substr($post_title, 0, 90) . '...';
																} else {
																	echo $post_title;
																} 
															?>
														</p>
                                                        <p class="time_takes_place"><i class="fas fa-clock"></i> <?php echo $post_timing; ?></p>
                                                    </div>
                                                </div>
											<?php
                                        }wp_reset_postdata();
                                    }
                                ?>
                            </div>
                            <div class="pagination-common js_panigator text-align-center">
                                <?php wp_navigation_paged_ajax($seminar_post); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form>
    <?php wp_nonce_field( 'ajax_loading_seminar', 'post_seminar_nonce' ); ?>
</form>
<script>
jQuery(document).ready(function($){

    const technicalSeminar = $('#loading_seminar_ajax');
	const researchSeminar = $('#research_seminar');
    const nonceValue = $('#post_seminar_nonce').val();
	
	researchSeminar.on('change', function(){
        $('.loading_animation').addClass('is--active');
        setTimeout(() => {
            const _researchArea = $(this).val();
            searchBlogsFunc(_researchArea);
        }, 0);
    });
	
	function searchBlogsFunc(researchSeminar=0){
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: obj.AJAX_URL,
            data: {
                action: "ajax_search_load_seminar",
                researchSeminar: researchSeminar,
                nonce: nonceValue,
            },
            beforeSend: () => {
                $('.loading_animation').addClass('is--active');
            },
            complete: () => {
                setTimeout(() => {
                    $('.loading_animation').removeClass('is--active');
                }, 500);
            },
            success: (response) => {
                setTimeout(() => {
                    technicalSeminar.empty();
                    technicalSeminar.append($(response.data));
                }, 300);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
            }
        });
    };
	
	technicalSeminar.on('click', '.js_panigator .paginate_links a', function(e){
        e.preventDefault();
        const _researchArea = researchSeminar.val();
        var hrefThis = $(this).attr('href');
        var page = hrefThis.match(/\/\d+\//)[0];
        page = page.match(/\d+/)[0];
        if(!page) page = 1;
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: obj.AJAX_URL,
            data: {
                action: "ajax_navigation_load_seminar",
                researchSeminar: _researchArea,
                paged: page,
                nonce: nonceValue,
            },
            beforeSend: () => {
                $('.loading_animation').addClass('is--active');
            },
            complete: () => {
                setTimeout(() => {
                    $('.loading_animation').removeClass('is--active');
                }, 500);
            },
            success: (response) => {
                setTimeout(() => {
                    technicalSeminar.empty();
                    technicalSeminar.append($(response.data));
                }, 300);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
            }
        });
    });
	
	
	

})
</script>
<?php get_footer() ?>

