<?php
$id = get_the_id();
$terms = get_the_terms( $id, 'category' );
$name = $terms[0]->name;

    get_header( );
    if(have_posts(  )) : 
        while (have_posts(  )): 
            the_post(  ); 
            $postId = get_the_ID(  );
            $thumbnail = get_the_post_thumbnail_url($postId, 'fullsize') ? get_the_post_thumbnail_url($postId, 'fullsize') : NOT_IMAGE;
            $title = get_the_title( $postId );
            $time = get_the_time( 'd/m/Y', $postId );
            $content = get_the_content( $postId );
			$category = get_the_terms( $postId, 'research-area' );
			$category_post = get_the_category($postId);
			$authors = get_field('authors_post', $postId);

?>
<div class="news_detail detail_single">
	<div class="section-1">
		<div class="tw-container">
			<div class="tw-content">
				<p class="location">
					<span><i class="fas fa-clock"></i>
					<?php echo $time; ?></span>
					<span><i class="fas fa-folder-open"></i>
					<?php 
						if($category) {
							echo $category[0]->name;
						}
						else {
							echo $category_post[0]->name;
						}
						
					?></span>
				</p>
				<h2 class="tw-title"><?php the_title(); ?></h2>
			</div>
		</div>
	</div>
	
	<div class="thumbnail">
		<img src="<?php echo $thumbnail; ?>" alt="">
	</div>
	
	<div class="section-2">
		<div class="tw-container">
			<div class="tw-content">
				<div class="careers_detail_contents">
					<div class="the_content">
						<div class="careers_detail_inner d-flex justify-content-between flex-direction-colum">
							<div class="__left w-100" id="desktop">
								<div class="__left_item __left_item_info w-100">
									<ul>
										<li class="__item_time d-flex "><img src="<?php echo THEME_ASSETS . '/images/oclock.svg' ?>" /> <?php the_time( 'i' ); ?> minutes</li>
										<li class="__item_user d-flex"><img src="<?php echo THEME_ASSETS . '/images/user-icon.svg' ?>" /> <p><?php echo $authors; ?></p></li>
									</ul>
								</div>
								
								<div class="__left_item __left_item_share w-100">
									<span>Share</span>
									<ul class="__list-social">
										<li class="__item_time">
											<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="btn_face popup_windown" rel="nofollow noopener"> 
												<i class="fab fa-facebook-f"></i>
											</a>
										</li>
										<li class="__item_time">
											<a href="https://twitter.com/share?url=<?php the_permalink(); ?>" class="btn_skype popup_windown" rel="nofollow noopener"> <i class="fab fa-twitter"></i></a>
										</li>
										<li class="__item_time">
											<a href="mailto:<?php the_permalink(); ?>"> <i class="fas fa-envelope-open"></i></a>
										</li>
									</ul>
								</div>
							</div>
							
							<div class="__right w-100">
								<div class="the_article">
									<?php
										if($content){
											the_content();
										}else{
											echo '<p>'.__('Updating', 'corex').'...</p>';
										}
									?>
								</div>
								<?php 
									if($name == "VinAI in the news" || $name == "Achievements blog") {
										?>
											<a href="<?php bloginfo('url');_e('/news', 'corex'); ?>" class="btn btn-view-detail btn-view-detail-home">
												<img src="<?php echo THEME_ASSETS . '/images/icon-left.svg' ?>" alt="">
												<span>Back to News</span>
											</a>
										<?php
									}
									else {
										?>
											<a href="<?php bloginfo('url');_e('/technical-blog', 'corex'); ?>" class="btn btn-view-detail btn-view-detail-home">
												<img src="<?php echo THEME_ASSETS . '/images/icon-left.svg' ?>" alt="">
												<span>Back to Research</span>
											</a>
										<?php
									}
								?>
								
							</div>
                            <div class="__left w-100" id="mobile">
								<div class="__left_item __left_item_info w-100">
									<ul>
										<li class="__item_time d-flex "><img src="<?php echo THEME_ASSETS . '/images/oclock.svg' ?>" /> <?php the_time( 'i' ); ?> minutes</li>
										<li class="__item_user d-flex"><img src="<?php echo THEME_ASSETS . '/images/oclock.svg' ?>" /> <p><?php echo $authors; ?></p></li>
									</ul>
								</div>
								
								<div class="__left_item __left_item_share w-100">
									<span>Share</span>
									<ul class="__list-social">
										<li class="__item_time">
											<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="btn_face popup_windown" rel="nofollow noopener"> 
												<i class="fab fa-facebook-f"></i>
											</a>
										</li>
										<li class="__item_time">
											<a href="https://twitter.com/share?url=<?php the_permalink(); ?>" class="btn_skype popup_windown" rel="nofollow noopener"> <i class="fab fa-twitter"></i></a>
										</li>
										<li class="__item_time">
											<a href="mailto:<?php the_permalink(); ?>"> <i class="fas fa-envelope-open"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <div class="section-3 news">
		<div class="tw-container">
			<div class="tw-content">
				<div class="_right">
					<h2><?php _e('Related post', 'corex'); ?></h2>
					<div class="_related_list list-publications-global">
						<div class="swiper swiper-container swiper-blog">
						<div class="swiper-wrapper">
						<?php
							$category = get_the_category($postId);
							$explore_more = ($category[0]->slug == 'products-solutions' || $category[0]->slug == 'customer-cases') ? (get_bloginfo('url').'/'.__('applied-ai', 'corex')) : HOME_URL . 'news';
							$postCat = new WP_Query([
								'post_type'       => 'post',
								'category_name'   => $category[0]->slug,
								'posts_per_page'  => 3,
								'post__not_in'    => array($postId),
								'post_status'     => 'publish'
							]);
						?>
						<?php
							if($postCat->have_posts(  )){
								while ($postCat->have_posts(  )) {
									$postCat->the_post(  );
									$_postId = get_the_ID(  );
									$_thumbnail = get_the_post_thumbnail_url( $_postId, 'fullsize' ) ? get_the_post_thumbnail_url( $_postId, 'fullsize' ) : NOT_IMAGE;
									$_title = get_the_title( $_postId );
									$_time = get_the_time( 'd/m/Y', $_postId );
									$_permalink = get_the_permalink($_postId);
									$_theExcerpt = get_the_excerpt($_postId);
									$category = get_the_terms( $_postId, 'research-area' );
									$category_post = get_the_category($_postId);
									?>
										<div class="swiper-slide">
										<div class="__item">
											<a href="<?php echo $_permalink ?>" class="thumbnail">
												<div class="--bg"
													style="background-image: url(<?php echo $_thumbnail ?>)">
												</div>
											</a>
											<div class="text">
												<span class="category sub-heading">
												<?php 
													if($category) {
														echo $category[0]->name;
													}
													else {
														echo $category_post[0]->name;
													}
													
												?>
												</span>
												<a href="<?php echo $_permalink ?>" class="--title">
													<h4><?php echo $_title; ?></h4>
												</a>
												<p class="desc">
													<?php 
														if (strlen($_theExcerpt) > 100) {
															echo substr($_theExcerpt, 0, 100) . '(...)';
														} else {
															echo $_theExcerpt;
														}
													?>
												</p>
												
												<div class="--meta d-flex align-items-center justify-content-between">
													<a href="<?php echo $_permalink ?>" class="btn btn-view-detail">
														<span><?php _e('View details', 'corex'); ?></span>
														<img src="<?php echo THEME_ASSETS . '/images/icon-right.svg' ?>" alt="">
													</a>
													<p class="--time"><?php echo $_time; ?></p>
												</div>
											</div>
										</div>
										</div>
									<?php
								}
							}
						?>
						</div>
						<div class="swiper-pagination-blog text-align-center" id="mobile"></div>
						</div>
					</div>

						
				</div>
			</div>
		</div>
	</div>
	
</div>
<?php endwhile; wp_reset_postdata(); endif; ?>
<script>
    jQuery(document).ready(function($){
		var swiper_blog = new Swiper(".swiper-blog", {
			slidesPerView: 3,
			spaceBetween: 30,
			loop: true,
			pagination: {
				el: ".swiper-pagination-blog",
				clickable: true,
			},
			breakpoints: {
            1024: {
                slidesPerView: 3,
				spaceBetween: 10,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
      });
	 		
		
		// Popup window 
		$('.popup_windown').click(function (event) {
			event.preventDefault();
			window.open($(this).attr("href"), "popupWindow", "width=600,height=600,scrollbars=yes");
		});
		
    })
</script>
<?php get_footer();