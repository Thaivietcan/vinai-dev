<style>
    .site-footer{
        opacity: 0;
        visibility: hidden;
        height: 0;
        overflow: hidden;
    }
</style>
<?php
    get_header()
?>
<section class="h-section1 404-page" id="h-section1">
	<div class="banner_site" style="background: linear-gradient( 
270deg, #82ffff 0.04%, rgb(242 248 255) 99.83% );"></div>
	<div class="content_banner_site">
		<div class="tw-container">
			<div class="tw-content">
				<h1 style="color:var(--color-link)"><?php _e('404', 'corex') ?></h1>
				<h2><?php _e('Opps! Page not found', 'corex') ?>.</h2>
				<a href="<?php echo get_bloginfo('url'); ?>" class="btn btn-view-detail btn-view-detail-home">
					<span><?php _e('Go back to Homepage', 'corex') ?></span>
					<img src="/wp-content/uploads/2021/11/icon-right.svg" alt="">
				</a>
			</div>
		</div>
	</div>
	 <div class="shape-one"></div>
	 <div class="shape-two"></div>
</section>

<?php
    get_footer();